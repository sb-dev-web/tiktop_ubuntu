const http = require('http');
// const https = require('https');
// const fs = require('fs'); 

require('dotenv').config();

var express = require("express");
var mongooes = require("mongoose");
var bodyParser = require("body-parser");
const util = require("./src/util/utility");
const cors = require("cors");
// const requestIp = require('request-ip');
const config = require("./src/config/config");
const Utility = require('./src/Helpers/Utility');
const commonRoute = require("./src/routes/common");



require('./src/crons/Crons');

//DB connection
// mongooes.connect(config.dbConfig.uri,{useNewUrlParser: true,useUnifiedTopology: true,replicaSet: 'rs'}).then(()=>{
//     console.log("db connected "+config.dbConfig.debug_mode);
// });

// console.log("config",config.dbConfig.uri);
// mongooes
//     .connect(config.dbConfig.uri, {
//        // replicaSet: "rs",
//         useNewUrlParser: true,
//         // useUnifiedTopology: true,
//         // retryWrites:false
//         //{ replicaSet: 'rs' }
//     })
//     .then(() => {
//         console.log("db connected " + config.dbConfig.debug_mode);
//     });
// mongooes.connection.on("error", (err) => {
//     console.log("db connection error:", err.message);
// });
// mongooes.connection.once("open", (_) => {
//     console.log("Database connected:");
// });


const app = express();

var globalVerifyFp = "verify_kffcu0at_oivt734B_sem4_4ObK_9TDD_D7NPr1bwmgr3";

const port = process.env.PORT;
// const port = '4000'

app.use(cors());
app.use(bodyParser.json());
// app.use(requestIp.mw());
app.use(util.getRequest);
const setResponseTime = (req, res, next) => {
    req.start_time = Utility.getTime();

    next();
}

app.use(setResponseTime);

const setVerifyFp = (req, res, next) => {
    if (req.url.match(/getTiktokSignedRequestDetails/i)) {
        let inputVerifyFP = req.body.cookies ? req.body.cookies.s_v_web_id : "";
        if (inputVerifyFP && inputVerifyFP !== 'verify_kclpmhmy_v581FRSe_H4D7_41eH_9beC_BA44TgG5HaaA') {
            globalVerifyFp = inputVerifyFP;
        }
        req.globalVerifyFp = globalVerifyFp;
    }
    next();
}
app.use(setVerifyFp);

app.post("/test", function (req, res) {
    var salt = CryptoJS.lib.WordArray.random(128 / 8);
    var password = CryptoJS.MD5("123456");
    var hash_password = CryptoJS.SHA256(password);
    console.log(
        "salt " + salt,
        "password " + password,
        "has pass   " + hash_password
    );
});
var commonController = require("./src/controllers/common");

//app.use('/register',commonController.register);

// app.use("/user", userRoute);
// app.use("/package", subscriptionRoute);
// app.use("/subscription", iossubscriptionRoute);
app.use("/", commonRoute);

app.get("/update_follower_tiktop_userid", async(req,res) => {
    let UserSubscriptionModel = require('./src/models/user_subscription');

    let response = {
        updated: 0,
        message: 'No update',
    }

    try {

        let objUserSubscription = await UserSubscriptionModel.find({tiktop_userid: "", type: "Follower"}).populate('user_id');
        if(objUserSubscription.length) {
            objUserSubscription.forEach(obj => {
                if(!obj.tiktop_userid && obj.user_id && obj.user_id.tiktop_userid) {
                    obj.tiktop_userid = obj.user_id.tiktop_userid;
                    obj.save();
                    response.updated++;
                }
            });
            response.message = 'success';
        }
    } catch (err) {
        response.message = err.stack;
    }

    res.json(response);
});


/**
 * SSL certificates related work
 */
// var options = {
//     key: fs.readFileSync(config.appConfig.sslCertificatePrivateKeyPath),
//     cert: fs.readFileSync(config.appConfig.sslCertificateCertFilePath),
//     strictSSL: true,
//     ca: fs.readFileSync(config.appConfig.sslCertificateCAFilePath)
// };

// var server = https.createServer(options, app);

/**
 * Normal HTTP related work
 */
// const Signer = require("tiktok-signature");
// //  global.globalSigner = new Signer(
// //     "Mozilla/5.0 (iPhone; CPU iPhone OS 13_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/83.0.4147.71 Mobile/15E148 Safari/606.1"
// // );
//  global.globalSigner = new Signer(
//     "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36"
// );

//global.globalSigner.init();

let objSigner = require("@truecarry/tiktok-signature");


   global.globalSigner  = new objSigner("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36");
   global.globalSigner.init();

   global.userCookie = {
        "sessionid_ss": "6844cebee70c1c7904d536d08bab85a9",
        "odin_tt": "37c272e488bb4751d49440e595680b77fd0d156bdf08ee8ba7a14aded73a70e290739393361e676bda30e325b683ba6dffda54482455ccb253421d35e0e671ef",
        "uid_tt": "c715975be0ed7d13da556533123d9c55ae79405fcffb26059255f3e056d14b96",
        "tt_webid_v2": "6889935201600177670",
        "MONITOR_WEB_ID": "4cfb4fde-24c6-434a-a305-c71e55040ea9",
        "sid_guard": "6844cebee70c1c7904d536d08bab85a9%7C1604188081%7C5184000%7CWed%2C+30-Dec-2020+23%3A48%3A01+GMT",
        "sid_tt": "6844cebee70c1c7904d536d08bab85a9",
        "uid_tt_ss": "c715975be0ed7d13da556533123d9c55ae79405fcffb26059255f3e056d14b96",
        "tt_webid": "6889935201600177670",
        "passport_auth_status": "22021837ae13d9758f9d8c0e47ed130a%2C",
        "ttwid": "1%7CkQcd6v8KLbhM-K2LPOtWtuOn-rWqwRhpr2sSHs7IHSU%7C1604188052%7Cef94a39fedaf9f65994249b8d7ded0a9e5701e89124b46c930b044c2d6245df1",
        "passport_csrf_token": "f5a8c894108357c1e2cbdf6f97399e74",
        "sessionid": "6844cebee70c1c7904d536d08bab85a9",
        "store-idc": "maliva",
        "store-country-code": "us"
    };


var server = http.createServer(app);

server.listen(port, () => {
    console.log("port listen ", port);
});
app.get('/autoFollow', function(req, res) {
  console.log("dfdf");
  commonController.autoFollow(req,res);
});

app.get('/autoLike', function(req, res) {
  console.log("dfdf");
  commonController.autoLike(req,res);
});

app.get('/block',function(req,res){
    console.log("dfdf");
  commonController.wrongBlockedUsers(req,res);
})

app.get('/checkProfile',function(req,res){
    console.log("dfdf");
  commonController.checkProfile(req,res);
})

