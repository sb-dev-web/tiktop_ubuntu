var express = require("express");
//User Controller
const commonController = require("../controllers/common");
const auth= require("../middleware/auth");
const app = express();
var router = express.Router();

router.post("/register", commonController.register);
router.post("/app_version", commonController.appVersion);
router.post("/general_info",commonController.generalInfo);
//router.post("/authentication",commonController.auth);
router.post("/follow-package",auth.checkAuthentication,commonController.getFollwPackage);
router.post("/like-package",auth.checkAuthentication,commonController.getLikePackage);
router.post("/getTiktokSignedRequestDetails",commonController.getTiktokSignedRequestDetails);
router.post("/auto-like", commonController.autoLike);
router.get("/check-profile",commonController.checkProfile);
module.exports = router;
