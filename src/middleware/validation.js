const utility = require('../util/utility');


const userRegister =async (req,res,next)=>{
    let requireObj = {
        username: 'required',//|username|unique:users,username',
       // password: "required",
    };
    const validator = await utility.checkValidation(req.body, requireObj);
    const isValidate = await validator.check();
    if (!isValidate) {
        let errorMessage = await utility.ValidationMessage(
            validator.errors
        );
        let outputObj = {
            status: 0,
            message: errorMessage
        };

        return utility.sendResponse(req, res, 200, outputObj);
    }
    next();
}
const followProfile = async(req,res,next)=>{
    let requireObj = {
        package_id: 'required',//|username|unique:users,username',
        
    };
    const validator = await utility.checkValidation(req.body, requireObj);
    const isValidate = await validator.check();
    if (!isValidate) {
        let errorMessage = await utility.ValidationMessage(
            validator.errors
        );
        let outputObj = {
            status: 0,
            message: errorMessage
        };

        return utility.sendResponse(req, res, 200, outputObj);
    }
    next();

}

const likeOder = async(req,res,next)=>{
    let requireObj = {
        package_id: 'required',//|username|unique:users,username',
        video_url: "required",
        thumb_url:"required",
        post_id:"required",
        like_count:"required",
        //title:"required"

    };
    const validator = await utility.checkValidation(req.body, requireObj);
    const isValidate = await validator.check();
    if (!isValidate) {
        let errorMessage = await utility.ValidationMessage(
            validator.errors
        );
        let outputObj = {
            status: 0,
            message: errorMessage
        };

        return utility.sendResponse(req, res, 200, outputObj);
    }
    next(); 
}

const promoteUser = async(req,res,next)=>{
    let requireObj = {
       // limit: 'required',//|username|unique:users,username',
       // page: "required",
    };
    const validator = await utility.checkValidation(req.body, requireObj);
    const isValidate = await validator.check();
    if (!isValidate) {
        let errorMessage = await utility.ValidationMessage(
            validator.errors
        );
        let outputObj = {
            status: 0,
            message: errorMessage
        };

        return utility.sendResponse(req, res, 200, outputObj);
    }
    next();

}

const profileaction = async(req,res,next)=>{
    let requireObj = {
        promoted_id: 'required',//|username|unique:users,username',
        type: "required",
    };
    const validator = await utility.checkValidation(req.body, requireObj);
    const isValidate = await validator.check();
    if (!isValidate) {
        let errorMessage = await utility.ValidationMessage(
            validator.errors
        );
        let outputObj = {
            status: 0,
            message: errorMessage
        };

        return utility.sendResponse(req, res, 200, outputObj);
    }
    next();

}
module.exports={
    userRegister,
    followProfile,
    promoteUser,
    profileaction,
    likeOder
    
}