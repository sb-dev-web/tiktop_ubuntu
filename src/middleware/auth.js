const {
    message_obj,
    mailtext_obj,
    api_error_code,
} = require("../lang/message");
const devicedetailModel = require("../models/devicedetail");
const userModel = require("../models/users");
const config = require("../config/config");
const jwt = require("jsonwebtoken");
const util = require("../util/utility");
const mongoose = require("mongoose");
const checkHashtaAuthentication = async (req, res, next) =>{
    const hashtagdevicedetailModel = require("../models/hashtagdevicedetail");
    token = req.headers.oauth;
    if (!token) {
        responseObj = {
            status: 403,
            message: message_obj.auth_token_missing,
            message_dev: "token not found",
            error_code: api_error_code.auth_fail,
        };
        return util.sendResponse(req, res, 403, responseObj);
    } else {
        const isToken = await  hashtagdevicedetailModel.findOne({"oauth":token},{'oauth': true}, {returnOriginal: true});
        if(isToken){
            req.headers.auth_user_id = tokenValidation.id;
            req.headers.coin = tokenValidation.coin;
            req.headers.username= tokenValidation.username
            next();
        }
        else{
            responseObj = {
                status: 403,
                message: message_obj.auth_token_missing,
                message_dev: "token not found",
                error_code: api_error_code.auth_fail,
            };
            return util.sendResponse(req, res, 403, responseObj);
        }

        
        
    }
    
    
}
const checkAuthentication = async (req, res, next) => {
    token = req.headers.oauth;
    if (!token) {
        responseObj = {
            status: 403,
            message: message_obj.auth_token_missing,
            message_dev: "token not found",
            error_code: api_error_code.auth_fail,
        };
        return util.sendResponse(req, res, 403, responseObj);
    } else {
        //verify token is correct or not
        let jwtPayload = jwt.verify(
            token,
            config.appConfig.secretKey,
            function (err, decoded) {
                if (err) {
                    responseObj = {
                        status: 403,
                        message: message_obj.auth_verification_fail,
                        message_dev: err.message,
                        error_code: api_error_code.auth_fail,
                    };
                    return util.sendResponse(req, res, 403, responseObj);
                }
                return decoded;
            }
        );

        //check user id
        //console.log(jwtPayload);
        if (jwtPayload) {
            let tokenValidation = await isTokenValid(jwtPayload);
            if (tokenValidation.status == 0) {
                let responseObj = {
                    status: 403,
                    message: tokenValidation.message,
                    error_code: api_error_code.auth_fail,
                };
                return util.sendResponse(req, res, 403, responseObj);
            } else if (tokenValidation.status == 1) {
                //set userid in req.header
                     //   console.log("token",tokenValidation);
                req.headers.auth_user_id = tokenValidation.id;
                req.headers.coin = tokenValidation.coin;
                req.headers.username= tokenValidation.username
                req.headers.tiktop_userid= tokenValidation.tiktop_userid
                
            }
            next();
        }
    }
};

const isTokenValid = async (payload) => {
    //console.log(payload);
    let userId = mongoose.Types.ObjectId(payload.user_id);
    let userDeviceId = payload.user_device_id;

   // console.log("user_id",userId, "device ", userDeviceId);
    var userDetail;
    let deviceDetail = await userModel
        .aggregate([
            {
                $match: {
                    $and: [
                        {
                            _id: userId,
                        },
                    ],
                },
            },
            {
                $lookup: {
                    from: "device_details",
                    localField: "_id",
                    foreignField: "user_id",
                    as: "deviceDetail",
                },
            },
            { $unwind: "$deviceDetail" },
            {
                $match: {
                    $and: [
                        {
                            "deviceDetail.device_id": userDeviceId,
                        },
                    ],
                },
            },
        ])
        .exec();

        
  //  console.log("deviceDetail",deviceDetail);
    if (deviceDetail.length > 0) {
        
        //check expire date
        
        userDetail = await userModel.findOne({
            _id: mongoose.Types.ObjectId(deviceDetail[0]._id),
            status : {$ne:"DELETED"}    
                ,
        });

        

        if (userDetail) {
            //  console.log(userDetail,"account type",userDetail.type,userDetail.name);
            return {
                status: 1,
                message: "success",
                username:userDetail.username,
                coin: userDetail.coin,
                id: userDetail._id,
                nick_name:userDetail.nick_name,
                uniqueId:userDetail.uniqueId,
                avatar_thumb:userDetail.avatar_thumb,
                avatar_larger:userDetail.avatar_larger,
                oauth:deviceDetail[0].deviceDetail.oauth,
                tiktop_userid:userDetail.tiktop_userid,
            };
        } else {
            return {
                status: 0,
                message: message_obj.auth_not_fount,
                error_code: api_error_code.server_error,
            };
        }
    } else {
        return {
            status: 0,
            message: message_obj.auth_not_fount,
            error_code: api_error_code.auth_fail,
        };
    }

  
};

const checkDriverInput = async (req, res, next) => {
   
    let jwtPayload = jwt.verify(token, process.env.secretKey, function (
        err,
        decoded
    ) {
        if (err) {
            console.log(err.message);
            console.log(err);
        }
        return decoded;
        console.log("decode", decoded);
    });
    
    return;
    // next();
};

module.exports = {
    checkDriverInput,
    checkAuthentication,
    checkHashtaAuthentication,
    isTokenValid
};
