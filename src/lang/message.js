var message_obj ={
    force_update:'We listened to your suggestions and made some exciting improvements. Please enjoy our new version.',
    new_version_available:'A new version of the app is available, click Update to proceed or Later to cancel',
    downtime:'This service is not available as the application is currently undergoing maintenance. Please wait or retry later.',
    app_uptodate:"Your application is uptodate",
    server_error:"An unexpected error has occured. To solve the issue, contact the person responsible for your Server.",
    tiktok_profile_not_found:"Tiktok profile not found with entered username",
    device_not_update:"User device is not registered ",
    user_insert_fail:"User saving failed",
    device_insert_fail:"Device detail not saved",
    follower_package_missing:"Sorry! No more follower packages are available",
    like_package_missing:"Sorry! No any like package available",
    auth_token_missing:"Auth token missing ",
    auth_not_fount:"Auth not found in db",
    package_not_found:"Package Not found",
    not_saficient_coin:"Your don't have enough coin to proceed.",
    subscription_not_save:"Unable to save your purchase, Please try again later.",
    transaction_log_entry_fail:"Transaction log entry failed",
    videourl_require:"Video url did not found",
    promoted_media_not_found:"No media available at the moment, please try after sometime.",
    promoted_media_target_complete:"Promoted profile/video already complete the target, please try with other profile/video",
    subscription_update_fail:"Subscription details not updated.",
    already_like:"You have already liked this video.",
    already_follow:"You are already following this user.",
    already_skip:"You already skipped this user ",
    like_successfully:"You have successfully like this video",
    skip_successfully:"You have successfully skipped",
    follow_successfully:"You have successfully followed this profile",
    like_order_not_exist:"You don't have any like orders.",
    auth_verification_fail:"Your session has been logged out, Please login again to continue.",
    alreadyviewadd:"Add is already viewed by this user.",
    coinAddforviewadd:"Coin awarded successfully",
    
    

    
};

var api_error_code={
    server_error:501,
    exception_code:502,
    auth_fail:401,
    app_update_require:302,
    app_new_version_exist:304,
    downtime:305,
    list_item_not_found:210, //IMP ERROR_CODE
    list_item_not_created:211,
    tiktok_profile_not_found:212,
    device_not_update:213,
    user_insert_fail:214,
    device_insert_fail:215,
    follower_package_missing:216,
    like_package_missing:217,
    package_not_found:218,
    not_saficient_coin:219,
    subscription_not_save:220,
    transaction_log_entry_fail:221,
    videourl_require:222,
    promoted_media_not_found:223,
    promoted_media_target_complete:224,
    subscription_update_fail:225,
    already_like:226,
    already_view:226,
    already_follow:227,
    like_order_not_exist:228

    
    

   
};

var mailtext_obj ={
    welcome_mail:"Welcome to using restapi "

}

module.exports={
    message_obj,
    mailtext_obj,
    api_error_code
}

//message.email_require ="A unique valid email require";
