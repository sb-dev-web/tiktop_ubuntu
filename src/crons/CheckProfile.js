const utility = require("../util/utility");
const UtilHelpers = require("../Helpers/Utility");

let CommonController = require('../controllers/common');

module.exports.cron = async () => {
	try {
        await CommonController.checkProfile();
	} catch (err) {
		let fileName = "ProfileCheck_" + UtilHelpers.getGMTDate();
		fileName += ".txt";
		UtilHelpers.logData(err.stack, fileName);
	}
};
