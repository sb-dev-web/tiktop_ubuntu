const utility = require("../util/utility");
const UtilHelpers = require("../Helpers/Utility");

module.exports.cron = async () => {
	let moment = require("moment");
	let mongoose = require("mongoose");

	//load modals
	let UserModel = require("../models/users");

	//load libraries
	const s3Actions = require("../libraries/S3ImageUpload");

	try {
		let previousSevenDaysDate = moment().subtract(7, "days").utc().toISOString();

		let objUsers = await UserModel.find({
			$and: [
				{
					$and: [
						{
							$or: [
								{
									is_image_updated: {
										$exists: false
									}
								},
								{
									is_image_updated: 0,
								}
							]
						},
						{
							$or: [
								{
									is_image_updation_failed: {
										$exists: false
									}
								},
								{
									is_image_updation_failed: 0
								},
							]
						}
					],
				},
				{
					created_on: { $gte: previousSevenDaysDate },
				}
			]
		}).sort({ _id: 1 }).limit(10);
		let i = 0;
		if (objUsers.length) {
			for (i; i < objUsers.length; i++) {
				let objUser = objUsers[i];

				let newUpdatedThumbURl = "";
				let newUpdatedLargeURl = "";
				let isImageUpdated = 0;
				if (objUser.avatar_thumb) {
					let isImageExists = await utility.imageExists(objUser.avatar_thumb);
					if (isImageExists.status) {
						let fileType = s3Actions.findFileTypeFromURL(objUser.avatar_thumb);
						let fileName = "tiktop-media/followers/thumb/" + objUser._id + "." + fileType;
						let response = await s3Actions.uploadImageOnS3(objUser.avatar_thumb, fileName);
						if (response && response.Location) {
							newUpdatedLargeURl = response.Location;
							isImageUpdated = 1;
						}
					}
				}
				if (objUser.avatar_larger) {
					let isImageExists = await utility.imageExists(objUser.avatar_larger);
					if (isImageExists.status) {
						let fileType = s3Actions.findFileTypeFromURL(objUser.avatar_larger);
						let fileName = "tiktop-media/followers/" + objUser._id + "." + fileType;
						let response = await s3Actions.uploadImageOnS3(objUser.avatar_larger, fileName);
						if (response && response.Location) {
							newUpdatedThumbURl = response.Location;
							isImageUpdated = 1;
						}
					}
				}

				let updatedUserData = {};
				if(isImageUpdated) {
					if(newUpdatedLargeURl) {
						updatedUserData = {
							...updatedUserData,
							avatar_larger: newUpdatedLargeURl,
							is_image_updated: 1,
							tiktok_avatar_larger: objUser.avatar_larger,
						}
					}
					if(newUpdatedThumbURl) {
						updatedUserData = {
							...updatedUserData,
							avatar_thumb: newUpdatedThumbURl,
							is_image_updated: 1,
							tiktok_avatar_thumb: objUser.avatar_thumb,
						}
					}
				} else {
					updatedUserData = {
						is_image_updation_failed: 1
					}
				}

				updatedUserData['image_processed_on'] = new Date();

				await UserModel.updateOne(
					{ _id: mongoose.Types.ObjectId(objUser._id) },
					updatedUserData
				);
			}
		}
	} catch (err) {
		let fileName = "followers_cron_error_" + UtilHelpers.getGMTDate();
		fileName += ".txt";
		UtilHelpers.logData(err.stack, fileName);
	}
};
