const CronJob = require("cron").CronJob;

//helpers
const Utility = require('../Helpers/Utility')

//const followerCron = require('./FolllowersImages');
//const autoLikeCron = require('./AutoLikeCron');
const profileCheckCron= require("./CheckProfile");

// var cronJobForProcessFollowerSubscriptionUserImages = new CronJob(
// 	"*/3 * * * *",
// 	async () => {
// 		try {
// 			await followerCron.cron();
// 		} catch (err) {
// 			console.log("err from cron", err.stack)
// 			let fileName = "followers_cron_error_" + Utility.getGMTDate();
// 			fileName += ".txt";
// 			Utility.logData(err.stack, fileName);
// 		}
// 	},
// 	null,
// 	true,
// 	"Asia/Kolkata"
// );
// //cronJobForProcessFollowerSubscriptionUserImages.start();

// var autoLikeCronProcess = new CronJob(
// 	"*/59 * * * *",
// 	async () => {
// 		try {
// 			await autoLikeCron.cron();
// 		} catch (err) {
// 			console.log("err from cron", err.stack)
// 			let fileName = "auto_like_cron_error_" + Utility.getGMTDate();
// 			fileName += ".txt";
// 			Utility.logData(err.stack, fileName);
// 		}
// 	},
// 	null,
// 	true,
// 	"Asia/Kolkata"
// );
// //autoLikeCronProcess.start();

var profileCheckCronProcess = new CronJob(
	"*/20 * * * *",
	async () => {
		try {
			await profileCheckCron.cron();
		} catch (err) {
			console.log("err from cron", err.stack)
			let fileName = "checkProfile" + Utility.getGMTDate();
			fileName += ".txt";
			Utility.logData(err.stack, fileName);
		}
	},
	null,
	true,
	"Asia/Kolkata"
);
profileCheckCronProcess.start();
