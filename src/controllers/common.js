const { message_obj, api_error_code } = require("../lang/message");
const utility = require("../util/utility");
const userModel = require("../models/users");

//config file
const config = require("../config/config");
const mongoose = require("mongoose");

const findurlArrForAction = async (req) => {
    console.log(req);
    var urlArrType = req.body.action;
    var params = req.body.parameters;
    let url = "";
    let method = "";
    var deviceId = "";
    var verifyFp = "";
    let cookies = req.body.cookies;
    if (
        req.body.hasOwnProperty("cookies") &&
        cookies.hasOwnProperty("tt_webid_v2")
    ) {
        deviceId = cookies.tt_webid_v2;
    }

    verifyFp = cookies.s_v_web_id;

    if (deviceId == "undefined" || deviceId == "") {
        deviceId = params.device_id;
    }
    if (verifyFp == "undefined") {
        verifyFp = params.verify_fp;
    }

    if (urlArrType == "follow-user") {
        url = `https://m.tiktok.com/api/commit/follow/user/?device_id=${deviceId}&type=1&user_id=${params.user_id}&from=18&channel_id=0&from_pre=0&aid=1988&app_language=en&device_platform=web_pc&current_region=DE&fromWeb=1&app_name=tiktok_web&cookie_enabled=true&screen_width=1440&screen_height=900&browser_language=en-US&browser_online=true&brand=Apple`;
        httpMethod = "post";
    } else if (urlArrType == "like-media") {
        url = `https://m.tiktok.com/api/commit/item/digg/?device_id=${deviceId}&aid=1988&aweme_id=${params.media_id}&type=1&device_platform=web_pc&app_language=en&channel_id=0&app_name=tiktok_web&cookie_enabled=true&screen_width=1440&screen_height=900&browser_language=en-US&browser_platform=MacIntel&page_referer=https:%2F%2Fwww.tiktok.com`;
        httpMethod = "post";
    } else if (urlArrType == "video-list") {
        url = `https://m.tiktok.com/api/item_list/?count=${params.count}&id=${params.user_id}&type=1&secUid=${params.sec_id}&maxCursor=${params.max_cursor}$&minCursor=${params.min_cursor}&sourceType=8&appId=1233&region=US&language=en`;
        httpMethod = "get";
    } else if (urlArrType == "account-details") {
        url = `https://m.tiktok.com/passport/web/account/info/?account_sdk_source=web&aid=1459&language=en&is_sso=false&host=`;
        httpMethod = "get";
    } else if (urlArrType == "user-details-by-unique-id") {
        url = `https://m.tiktok.com/api/user/detail/?uniqueId=${params.unique_id}&aid=1988&app_name=tiktok_web&device_platform=web&referer=&screen_width=1440&screen_height=900&browser_language=en-US&browser_platform=MacIntel&browser_name=Mozilla&browser_online=true&ac=4g&timezone_name=Asia%2FCalcutta&priority_region=&appId=1233&region=DE&appType=m&isAndroid=false&isMobile=false&isIOS=false&OS=mac&did=${deviceId}&language=en`;
        httpMethod = "get";
    } else if (urlArrType == "user-details-by-user-id") {
        url = `https://m.tiktok.com/api/user/detail/?userId=${params.user_id}&aid=1988&app_name=tiktok_web&device_platform=web&referer=&screen_width=1440&screen_height=900&browser_language=en-US&browser_platform=MacIntel&browser_name=Mozilla&browser_online=true&ac=4g&timezone_name=Asia%2FCalcutta&priority_region=&appId=1233&region=DE&appType=m&isAndroid=false&isMobile=false&isIOS=false&OS=mac&did=${deviceId}&language=en`;
        httpMethod = "get";
    }

    if (
        verifyFp &&
        verifyFp != "undefined" &&
        verifyFp === "verify_kclpmhmy_v581FRSe_H4D7_41eH_9beC_BA44TgG5HaaA"
    ) {
        url = url + `&verifyFp=` + req.globalVerifyFp;
    } else if (!verifyFp && req.globalVerifyFp) {
        url = url + `&verifyFp=` + req.globalVerifyFp;
    }

    return { url, httpMethod };

    //let urlArr_FollowUserNew = "https://m.tiktok.com/api/commit/follow/user/?aid=1988&app_name=tiktok_web&device_platform=web_pc&referer=https:%2F%2Fwww.tiktok.com%2F@garyquartus&user_agent=Mozilla%2F5.0+(Macintosh%3B+Intel+Mac+OS+X+10_15_1)+AppleWebKit%2F537.36+(KHTML,+like+Gecko)+Chrome%2F84.0.4147.135+Safari%2F537.36&cookie_enabled=true&screen_width=1440&screen_height=900&browser_language=en-US&browser_platform=MacIntel&browser_name=Mozilla&browser_version=5.0+(Macintosh%3B+Intel+Mac+OS+X+10_15_1)+AppleWebKit%2F537.36+(KHTML,+like+Gecko)+Chrome%2F84.0.4147.135+Safari%2F537.36&browser_online=true&ac=4g&timezone_name=Asia%2FCalcutta&page_referer=https:%2F%2Fwww.tiktok.com%2F@garyquartus92%3Flang%3Den&priority_region=&appId=1233&region=DE&appType=m&isAndroid=false&isMobile=false&isIOS=false&OS=mac&did=6858229425696966145&device_id=$DEVICEID$&type=1&user_id=$USERID$&from=19&channel_id=3&from_pre=0&app_language=en&current_region=DE&fromWeb=1"
};
const setCookiesPars = (cookies) => {
    var strCookie = "";
    // cookies.map((cookie,index )=>{
    //     //console.log('cook',JSON.stringify(cookie));
    //strCookie = strCookie.concat( `${Object.keys(cookie)}=${Object.values(cookie)}`,';')
    // })
    // for (key in cookies) {
    //     strCookie = strCookie.concat(`${key}=${cookies[key]}`, ";");
    // }
    for (key in cookies) {
        if (key == "sessionid" || key == "tt_csrf_token") {
            strCookie = strCookie.concat(`${key}=${cookies[key]}`, ";");
        }
    }
    // if(!strCookie.includes("tt-csrf-token"))
    // {
    //     let token = "CfcsLDR0osHYQSvjYEfIyRxv"
    //       strCookie = strCookie.concat(`tt_csrf_token=${token};`);
    //  }

    return strCookie;
};
const getHeaders = (cookies, user_agent) => {
    if (
        typeof cookies.tt_csrf_token == "undefined" ||
        (!cookies && !cookies.tt_csrf_token)
    ) {
        cookies.tt_csrf_token = "CfcsLDR0osHYQSvjYEfIyRxv";
    }
    let tt_csrf_token = cookies.tt_csrf_token;
    cookies = setCookiesPars(cookies);

    headers = [
        {
            name: "Accept",
            value: "application/json",
        },
        {
            name: "authority",
            value: "m.tiktok.com",
        },
        {
            name: "content-length",
            value: "0",
        },
        {
            name: "user-agent",
            value: user_agent,
        },
        {
            name: "content-type",
            value: "application/x-www-form-urlArrencoded",
        },
        {
            name: "origin",
            value: "https://www.tiktok.com",
        },
        {
            name: "referer",
            value: "https://www.tiktok.com/foryou?lang=en",
        },
        {
            name: "sec-fetch-site",
            value: "same-site",
        },
        {
            name: "sec-fetch-mode",
            value: "cors",
        },
        {
            name: "accept-language",
            value: "en-US,en;q=0.9",
        },
        {
            name: "cookie",
            value: cookies,
        },
        {
            name: "tt-csrf-token",
            value: tt_csrf_token,
        },
    ];
    return headers;
};
const getTiktokSignedRequestDetails = async (req, res) => {
    try {
        let geturlArr = await findurlArrForAction(req);

        let appVersion = +req.headers.app_version;

        let userAgent = req.body.user_agent;
        userAgent =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36";

        let headers = getHeaders(req.body.cookies, userAgent);

        if (req.body.action === "user-details-by-unique-id") {
            headers = headers.filter((header) => header.name != "cookie");
        }

        if (geturlArr.url) {
            let ttPlainURL = geturlArr.url;

            let responseObj = {};

            if (!appVersion || appVersion < 1.5) {
                let url = config.appConfig.ttSignatureCreateURL;

                let data = {
                    data: {
                        userAgent,
                        ttUrl: geturlArr.url,
                        cookies: req.body.cookies,
                    },
                    type: "post",
                };

                let response = await utility.sendRequest(url, data);
                if (!response.status) {
                    responseObj = {
                        status: 0,
                        message: "Something went wrong.",
                        message_dev: response.message,
                        shouldUseLocalSignature: true,
                        ttPlainURL,
                    };
                } else {
                    responseObj = {
                        status: 1,
                        signature: response.signature,
                        user_agent: userAgent,

                        headers: headers,
                        method: geturlArr.httpMethod,
                        signedurlArr: response.signedUrl,
                        shouldUseLocalSignature: true,
                        ttPlainURL,
                    };
                }
            } else {
                responseObj = {
                    status: 1,
                    signature: "",
                    user_agent: userAgent,

                    headers: headers,
                    method: geturlArr.httpMethod,
                    signedurlArr: "",
                    shouldUseLocalSignature: true,
                    ttPlainURL,
                };
            }

            utility.sendResponse(req, res, 200, responseObj);
        } else {
            let responseObj = {
                status: 0,
                message: "Action urlArr is not valid",
                message_dev: "Action urlArr is not valid",
            };
            utility.sendResponse(req, res, 200, responseObj);
        }
    } catch (error) {
        let responseObj = {
            status: 0,
            message: message_obj.server_error,
            message_dev: error.message,
        };
        utility.sendResponse(req, res, 200, responseObj);
    }
};

const appVersion = async (req, res) => {
    const appVersionModel = require("../models/app_version");
    try {
        let requireObj = {
            os: "required",
            app_version: "required",
        };
        const validator = await utility.checkValidation(req.body, requireObj);
        const isValidate = await validator.check();
        //   console.log("dfd",isValidate);
        if (!isValidate) {
            let errorMessage = await utility.ValidationMessage(
                validator.errors
            );
            let outputObj = {
                status: 0,
                message: errorMessage,
            };

            return utility.sendResponse(req, res, 200, outputObj);
        }
        let osType = req.body.os;
        let uniqueId = req.body.unique_id;
        let applicationVersion = req.body.app_version;

        var message = "";
        var error_code = "";
        let where = {
            os_type: osType,
        };

        let versionDetail = await appVersionModel.findOne(where);

        if (versionDetail) {
            let ttUrlToLoad = versionDetail.tt_url_to_load;
            let signaturePageUserAgent =
                versionDetail.signature_page_user_agent;
            let initialAdShowCount = versionDetail.initial_ad_show_count;
            let repeatAdShowCount = versionDetail.repeat_ad_show_count;

            if (uniqueId) {
                ttUrlToLoad = "https://www.tiktok.com/@" + uniqueId;
            }

            var app_mode = versionDetail.app_mode;
            var databaseAppVersion = versionDetail.app_version;
            var update_required = versionDetail.update_require;
            var mandatory = versionDetail.mandatory;
            var user_agent = versionDetail.user_agent;
            if (applicationVersion > databaseAppVersion) {
                app_mode = "HASH";
            }
            if (applicationVersion >= databaseAppVersion) {
                update_required = false;
                mandatory = false;
            }
            if (versionDetail.mandatory) {
                message = message_obj.force_update;
                error_code = api_error_code.app_update_require;
            } else {
                message = message_obj.new_version_available;
                error_code = api_error_code.app_new_version_exist;
            }
            let outputObj = {
                status: 1,
                update_message: message,
                update_required: update_required,
                mandatory: mandatory,
                app_mode: app_mode,
                user_agent: user_agent,
                app_version: databaseAppVersion,
                app_url: versionDetail.app_link ? versionDetail.app_link : "",
                tt_url_to_load: ttUrlToLoad,
                signature_page_user_agent: signaturePageUserAgent,
                initial_ad_show_count: initialAdShowCount,
                repeat_ad_show_count: repeatAdShowCount,
                interestial_ad_unit_id: versionDetail.interestial_ad_unit_id,
                reward_ad_unit_id: versionDetail.reward_ad_unit_id,
            };
            utility.sendResponse(req, res, 200, outputObj);
        } else {
            let outputObj = {
                status: 0,
                message: message_obj.app_uptodate,
                message_dev: "app not registered",
            };
            utility.sendResponse(req, res, 200, outputObj);
        }
    } catch (error) {
        console.log("ere", error);
        let outputObj = {
            status: 0,
            message: message_obj.server_error,
            message_dev: error.message,
        };
        utility.sendResponse(req, res, 200, outputObj);
    }
};

const auth = async (req, res) => {};

const getFollwPackage = async (req, res) => {
    const _ = require("loadsh");
    const packageModel = require("../models/package");
    let where = {
        // deleted_on: "",
        type: "Follower",
    };
    let packages = await packageModel.find(where, "", { lean: true }).select({
        _id: 1,
        credits: 1,
        count: 1,
        type: 1,
    });

    // packages = _.orderBy(packages, [''],['asc']);
    packages = _.sortBy(packages, [
        function (o) {
            return +o.credits;
        },
    ]);

    if (packages.length > 0) {
        let responseObj = {
            status: 1,
            package: packages,
        };
        utility.sendResponse(req, res, 200, responseObj);
    } else {
        let responseObj = {
            status: 0,
            message: message_obj.follower_package_missing,
            error_code: api_error_code.follower_package_missing,
        };
        utility.sendResponse(req, res, 200, responseObj);
    }
};
const getLikePackage = async (req, res) => {
    const packageModel = require("../models/package");
    const _ = require("loadsh");
    let where = {
        //deleted_on: "",
        type: "Like",
    };
    let packages = await packageModel.find(where, "", { lean: true }).select({
        _id: 1,
        credits: 1,
        count: 1,
        type: 1,
    });
    packages = _.sortBy(packages, [
        function (o) {
            return +o.credits;
        },
    ]);

    if (packages.length > 0) {
        let responseObj = {
            status: 1,
            package: packages,
        };
        utility.sendResponse(req, res, 200, responseObj);
    } else {
        let responseObj = {
            status: 0,
            message: message_obj.follower_package_missing,
            error_code: api_error_code.follower_package_missing,
        };
        utility.sendResponse(req, res, 200, responseObj);
    }
};
const register = async (req, res) => {
    let requireObj = {
        // email: 'required|email',
        password: "required",
        email: "required|email|unique:Users,email",
    };
    const validator = await utility.checkValidation(req.body, requireObj);

    const error = await validator.check();

    if (!error) {
        res.status(422).send(validator.errors);
    }
    res.status(422).send({ stauts: 1, message: true });
};
const generalInfo = async (req, res) => {
    try {
        const packageModel = require("../models/package");
        var followers = [];
        var likes = [];

        var packagesDetail = {
            follower: followers,
            likes: likes,
        };
        //get all package
        let where = {
            deleted_on: "",
        };
        let packages = await packageModel
            .find(where, "", { lean: true })
            .select({
                _id: 1,
                credits: 1,
                count: 1,
                type: 1,
            });
        if (packages.length > 0) {
            packages.forEach((element) => {
                if (element.type == "Follower") {
                    followers.push(element);
                } else {
                    likes.push(element);
                }
            });

            packagesDetail = {
                follower: followers,
                likes: likes,
            };
        }

        let outputObj = {
            status: 1,
            packages: packagesDetail,
        };
        utility.sendResponse(req, res, 200, outputObj);
    } catch (error) {
        let outputObj = {
            status: 0,
            message_dev: error.message,
            message: message_obj.server_error,
        };
        utility.sendResponse(req, res, 200, outputObj);
    }
};

const autoLike = async (req = "", res = "") => {
    let AppVersionModel = require("../models/app_version");
    let UserModel = require("../models/users");
    let UserDeviceModel = require("../models/devicedetail");
    let UserActionsModel = require("../models/user_actions");
    let UserSubscriptionModel = require("../models/user_subscription");
    let AutoLikeLogModel = require("../models/auto_like_log");

    let ActionController = require("../controllers/action");
    const actionModel = require("../models/user_actions");

    // let UtilHelper = require("../Helpers/Utility");

    let objSigner = require("@truecarry/tiktok-signature");
    // let HttpProxyAgent = require('http-proxy-agent');

    let signer = "";

    // let responseProxyHTTPAgent = await getProxyHTTPAgent();
    // console.log(responseProxyHTTPAgent);
    // if(!responseProxyHTTPAgent) {
    //   console.log("responseProxyHTTPAgent", responseProxyHTTPAgent);
    //   return false;
    // }

    try {
        let userAggregateOptions = [
            {
                $match: {
                    auto_like_reused: {
                        $exists: false,
                    },
                    is_blocked: { $exists: false },
                },
            },
            {
                $lookup: {
                    from: "device_details",
                    localField: "_id",
                    foreignField: "user_id",
                    as: "user_devices",
                },
            },
            {
                $unwind: "$user_devices",
            },
            {
                $match: {
                    $and: [
                        {
                            "user_devices.user_cookie": { $exists: true },
                            "user_devices.user_cookie": { $ne: null },
                        },
                        {
                            "user_devices.user_cookie.tt_webid_v2": {
                                $exists: true,
                            },
                        },
                    ],
                },
            },
            {
                $group: {
                    _id: "$_id",
                    tiktop_userid: { $first: "$tiktop_userid" },
                    device_data: {
                        $push: {
                            cookie: "$user_devices.user_cookie",
                        },
                    },
                },
            },
            {
                $project: {
                    _id: 1,
                    tiktop_userid: 1,
                    device_data: {
                        $slice: ["$device_data", 0, 1],
                    },
                },
            },
            {
                $project: {
                    _id: 1,
                    tiktop_userid: 1,
                    cookie: {
                        $arrayElemAt: ["$device_data.cookie", 0],
                    },
                },
            },
            {
                $sort: { updated_on: -1 },
            },
            {
                $limit: 10,
            },
        ];

        /**
     * Sample response of objUser
      [
        {
          _id: "asdasd",
          tiktop_userid: "asdasd",
          cookie: {
            "Asdasd": "asdasd"
          },
        }
      ]
     */

        let errorStatusCodes = [2209, 2053];
        let objUsers = await UserModel.aggregate(userAggregateOptions);
        console.log("objUsers Count", objUsers.length);
        if (objUsers && objUsers.length) {
            let objAppVersion = await AppVersionModel.findOne({
                os_type: "Ios",
            });
            let deviceSign = "";
            if (objAppVersion) {
                signer = new objSigner(objAppVersion.user_agent);
                deviceSign = await signer.init();
            }

            let objUserSubscriptions = "";
            for (
                let iteratorUser = 0;
                iteratorUser < objUsers.length;
                iteratorUser++
            ) {
                let objUser = objUsers[iteratorUser];
                console.log("user id ", objUser._id);
                let data = {
                    body: {
                        provided_media: [],
                    },
                };
                data["headers"] = {
                    auth_user_id: objUser._id,
                };
                objUserSubscriptions = await ActionController.getPromotedUser(
                    data,
                    "",
                    "",
                    1
                );
                console.log(
                    "objUserSubscriptions.promoted_media.length",
                    objUserSubscriptions.promoted_media.length
                );
                if (
                    objUserSubscriptions &&
                    objUserSubscriptions.status &&
                    objUserSubscriptions.promoted_media.length
                ) {
                    for (
                        let i = 0;
                        i < objUserSubscriptions.promoted_media.length;
                        i++
                    ) {
                        await new Promise((resolve) =>
                            setTimeout(resolve, 2000)
                        );

                        let objUserSubscription =
                            objUserSubscriptions.promoted_media[i];

                        let count = objUserSubscription.count;
                        let completed = objUserSubscription.completed;
                        let remaining = count - completed;

                        let data = {
                            body: {
                                action: "like-media",
                                cookies: objUser.cookie,
                                parameters: {
                                    media_id: objUserSubscription.tiktop_postid,
                                },
                            },
                        };
                        let url = await findurlArrForAction(data);
                        if (deviceSign && url) {
                            //deviceSign = await signer.init();
                            let signature = await signer.sign(url.url);
                            //await signer.close();

                            let tiktokURL =
                                url.url + "&_signature=" + signature;

                            let headers = getHeaders(
                                objUser.cookie,
                                objAppVersion.user_agent
                            );

                            let formattedHeaders = {};
                            headers.forEach((header) => {
                                formattedHeaders[header.name + ""] =
                                    header.value;
                            });

                            let errorMsg = "";
                            let response = "";
                            var unique_identifier =
                                objUserSubscription.tiktop_userid +
                                "-" +
                                objUserSubscription.tiktop_postid;

                            let actionObj = {
                                promoted_id: objUserSubscription._id,
                                type: "Like",
                                action_by: objUser._id,
                                action_to: objUserSubscription.user_id,
                                entity: objUserSubscription.entity,
                                tiktop_postid:
                                    objUserSubscription.tiktop_postid,
                                tiktop_userid:
                                    objUserSubscription.tiktop_userid,
                                action_type: "auto",
                                unique_identifier: unique_identifier,
                                created_on: utility.getGmtTime(),
                                updated_on: utility.getGmtTime(),
                                tiktok_url: tiktokURL,
                                headers: formattedHeaders,
                            };
                            //console.log("action Obj",actionObj);

                            try {
                                // let agent = new HttpProxyAgent(responseProxyHTTPAgent);
                                response = await utility.sendRequest(
                                    tiktokURL,
                                    {
                                        type: "post",
                                        headers: formattedHeaders,
                                        otherData: {
                                            // mode: "cors",
                                            body: null,
                                            // agent
                                        },
                                    }
                                );
                                //console.log("tikto")

                                if (response.is_digg === 0) {
                                    var insertAction = await UserActionsModel.create(
                                        actionObj
                                    );

                                    actionObj.is_success = 1;

                                    let updateStr = {
                                        $inc: { completed: 1 },
                                        updated_on: utility.getGmtTime(),
                                    };

                                    await UserSubscriptionModel.findOneAndUpdate(
                                        { _id: objUserSubscription._id },
                                        updateStr,
                                        {
                                            new: true,
                                            rawResult: true,
                                        }
                                    );

                                    await UserModel.updateOne(
                                        { _id: objUser._id },
                                        {
                                            $inc: { auto_like_reused: 1 },
                                        }
                                    );
                                    //work related to move user action table data to order actions table
                                    let objOrderActionResponse = await utility.addActionToOrderActions(
                                        objUsers[iteratorUser],
                                        objUserSubscription,
                                        "Like"
                                    );
                                    console.log(
                                        "response ",
                                        objOrderActionResponse
                                    );
                                    // console.log("add order action log",objOrderActionResponse);
                                    insertAction = JSON.parse(
                                        JSON.stringify(insertAction)
                                    );

                                    console.log(
                                        "action model id",
                                        insertAction._id
                                    );

                                    var dd = await actionModel.findByIdAndUpdate(
                                        insertAction._id,
                                        {
                                            is_moved_to_order_actions:
                                                objOrderActionResponse.status,
                                            is_moved_to_order_action_error:
                                                objOrderActionResponse.message,
                                        }
                                    );
                                } else if (
                                    errorStatusCodes.includes(
                                        response.status_code
                                    )
                                ) {
                                    await UserSubscriptionModel.findOneAndUpdate(
                                        { _id: objUserSubscription._id },
                                        {
                                            is_video_deleted: true,
                                            is_completed: true,
                                        }
                                    );
                                }

                                actionObj.tiktop_response = response;

                                await new AutoLikeLogModel(actionObj).save();

                                if (response.status_code === 8) {
                                    await UserModel.updateOne(
                                        { _id: objUser._id },
                                        { auto_like_reused: 50 }
                                    );
                                    break;
                                }
                                if (response.status_code === 9) {
                                    //Message: Oops, your account has been temporarily suspended.
                                    await blockPromoted(objUser.tiktop_userid);
                                    await UserModel.updateOne(
                                        { _id: objUser._id },
                                        { is_blocked: true }
                                    );
                                    break;
                                }

                                if (response.status_code === 2150) {
                                    //Message: You're tapping too fast. Take a break!
                                    break;
                                }

                                if (response.status_code === 24) {
                                    //This account was banned due to multiple Community Guidelines violations
                                    await UserModel.updateOne(
                                        { _id: objUserSubscription.user_id },
                                        { is_blocked: true }
                                    );

                                    await blockPromoted(
                                        objUserSubscription.user_id
                                    );

                                    break;
                                }
                            } catch (error) {
                                errorMsg = error.message;

                                actionObj.tiktop_response = errorMsg;

                                await new AutoLikeLogModel(actionObj).save();
                            }
                        }
                    }

                    // return res.json({
                    //   tiktokURL,
                    //   signature,
                    //   response,
                    //   errorMsg,
                    // });
                }
            }
            if (signer) {
                await signer.close();
            }
            if (res) {
                return res.json("Asdasd");
            }
            return true;
        } else {
            if (signer) {
                await signer.close();
            }
            if (res) {
                return res.json("Asdasd1");
            }
            return true;
        }
    } catch (error) {
        console.log(error);
        if (signer) {
            await signer.close();
        }
        let outputObj = {
            status: 0,
            message_dev: error.message,
            message: message_obj.server_error,
        };
        if (res) {
            return utility.sendResponse(req, res, 200, outputObj);
        }
        return outputObj;
    }
};

const getProxyHTTPAgent = async (_) => {
    let fetch = require("node-fetch");

    let url =
        "https://api.proxyflow.io/v1/proxy/random?token=f9236dd764a99b75f91bdec0&ssl=true&protocol=http&country=US";
    let response = await fetch(url);
    if (response.status === 200) {
        response = await response.json();
        return response.url;
    }
    return "";
};

const blockPromoted = async (tiktop_userid) => {
    const promotedModel = require("../models/user_subscription");
    var where = {
        tiktop_userid: tiktop_userid,
        count: { $gt: $completed },
    };
    var updateObj = {
        is_blocked: true,
    };
    var updatePromoted = promotedModel.findOneAndUpdate(where, updateObj, {
        new: true,
    });
    if (updatePromoted._id) {
        return true;
    } else {
        return false;
    }
};

const checkProfile = async (req, res) => {
    try {
        console.log("here in function");
        const appVersionModel = require("../models/app_version");
        // let objSigner = require("@truecarry/tiktok-signature");
        // let HttpProxyAgent = require('http-proxy-agent');

        let signer = "";
        let objAppVersion = await appVersionModel.findOne({
            os_type: "Ios",
        });
        let deviceSign = "";
        // if (objAppVersion) {
        //     signer = new objSigner(objAppVersion.signature_page_user_agent);
        //     deviceSign = await signer.init();
        // }

        var getUserDetail = await getUser();
        var userAgent = objAppVersion.signature_page_user_agent;
        // console.log("user agent", userAgent);
        if (getUserDetail.length > 0) {
            console.log("found", getUserDetail.length);
            for (let index = 0; index < getUserDetail.length; index++) {
                //Check with unique id
                var objUser = getUserDetail[index];
                var tiktop_userid = objUser.tiktop_userid;
                // var response = await checkProfileWithuniqueId(
                //     objUser,
                //     userAgent
                // );

                // if (response.status == 2) {
                //     console.log(response);
                //     console.log("sign fail");
                //     await new Promise((resolve) => setTimeout(resolve, 2000));
                //     continue;
                // }

                //if success full update user detail
                // if (response.status == 1) {
                //     //update user cookie
                //     await updateUser(
                //         tiktop_userid,
                //         response.user_info,
                //         response.user_cookie,
                //         response.is_private_account,
                //         true
                //     );
                // } else {
                console.log("try with userid " + getUserDetail[index]._id);
                //check with tiktop_userid
                var response = await checkProfileWithUserId(
                    getUserDetail[index],
                    userAgent
                );
                if (response.status == -1) {
                    console.log(response);
                    console.log("Undefine");
                    await new Promise((resolve) => setTimeout(resolve, 2000));
                    continue;
                } else if (response.status == 2) {
                    console.log(response);
                    console.log("sign fail");
                    await new Promise((resolve) => setTimeout(resolve, 2000));
                    continue;
                } else if (response.status == 1) {
                    // console.log("found");
                    await updateUser(
                        tiktop_userid,
                        response.user_info,
                        response.user_cookie,
                        response.is_private_account,
                        true
                    );
                } else {
                    //mark is_block true and user subscription
                    await blockUser(getUserDetail[index]._id);
                }
                // }
                await new Promise((resolve) => setTimeout(resolve, 2000));
            }
            var output = {
                status: 1,
                message: "succes",
            };

            utility.sendResponse(req, res, 200, output);
        } else {
            console.log("user not found");
            utility.sendResponse(req, res, 200, { message: "user not found" });
        }
    } catch (error) {
        console.log(error);
    }
};

const getUser = async () => {
    var today = new Date();
    var lastweek = new Date(today.getTime() - 7 * 24 * 60 * 60 * 1000);
    var users = await userModel.aggregate([
        {
            $match: {
                //_id: { $in: ["5f945c0dd8d11f76322f3b4b","5f9b4b6bd9a4b11b9dffd713","5fb4ee53747f6934fd3f87fb"] },
                // is_blocked:true,
                //_id:mongoose.Types.ObjectId('5f81ebca8f1f7a56b73d8fbc'),
                $or: [
                    {
                        is_profile_update: { $exists: false },
                    },
                    {
                        $and: [
                            {
                                is_profile_update: { $exists: true },
                            },
                            { profile_updated_on: { $lt: lastweek } },
                        ],
                    },
                ],
                //user_cookie: { $exists: true },
            },
        },
        {
            $lookup: {
                from: "user_subscriptions",
                localField: "_id",
                foreignField: "user_id",
                as: "user_subscriptions",
            },
        },
        {
            $unwind: "$user_subscriptions",
        },
        {
            $match: {
                $and: [
                    // { "user_subscriptions.type": "Follower" },
                    { "user_subscriptions.is_completed": false },

                    //temp comment
                    {
                        $or: [
                            {
                                "user_subscriptions.is_blocked": {
                                    $exists: false,
                                },
                            },
                            {
                                $and: [
                                    {
                                        "user_subscriptions.is_blocked": {
                                            $exists: true,
                                        },
                                    },
                                    { "user_subscriptions.is_blocked": false },
                                ],
                            },
                        ],
                        $or: [
                            {
                                "user_subscriptions.temp_hold": {
                                    $exists: false,
                                },
                            },
                            {
                                $and: [
                                    {
                                        "user_subscriptions.temp_hold": {
                                            $exists: true,
                                        },
                                    },
                                    { "user_subscriptions.temp_hold": false },
                                ],
                            },
                        ],
                    },
                ],
            },
        },
        {
            $group: {
                _id: "$_id",
                uniqueId: { $first: "$uniqueId" },
                tiktop_userid: { $first: "$tiktop_userid" },
                username: { $first: "$username:" },
                nick_name: { $first: "$nick_name" },
                user_cookie: { $first: "$user_cookie" },
                user_info: { $first: "$user_info" },
                is_blocked: { $first: "$is_blocked" },
                user_subscriptions: {
                    $push: {
                        _id: "$user_subscriptions._id",
                        entity: "$user_subscriptions.entity",
                        is_blocked: "$user_subscriptions.is_blocked",
                        type: "$user_subscriptions.type",
                    },
                },
            },
        },

        {
            $limit: 50,
        },
    ]);
    return users;
};

const checkProfileWithuniqueId = async (objUser, userAgent) => {
    var tiktop_userid = objUser.tiktop_userid;
    console.log("tiktok user id", tiktop_userid);
    console.log(objUser);

    //check user_cookie
    if (
        !objUser.hasOwnProperty("user_cookie") ||
        objUser.user_cookie === null
    ) {
        console.log("cookie not found");
        objUser.user_cookie = global.userCookie;
        // console.log("typeof ", typeof objUser.user_cookie);
        // console.log("user cookie ", objUser.user_cookie);
    }
    var deviceId = "";
    //console.log(objUser.user_cookie);

    if (
        objUser.user_cookie &&
        objUser.user_cookie.hasOwnProperty("tt_webid_v2")
    ) {
        var deviceId = objUser.user_cookie.tt_webid_v2;
    } else {
    }

    let data = {
        body: {
            action: "user-details-by-unique-id",
            cookies: objUser.user_cookie,
            parameters: {
                unique_id: objUser.uniqueId,
                device_id: deviceId,
            },
        },
    };

    // console.log(data);

    let url = await findurlArrForAction(data);
    if (url && url.url) {
        // let signResponse = await utility.sendRequest(url, signdata);
        var generateSignedUrl = await createSignUrl(
            url.url,
            objUser.user_cookie,
            userAgent
        );
        //console.log(generateSignedUrl);
        if (generateSignedUrl.status == 0) {
            return {
                status: 2,
                message: "Signed URL not generate",
                message_dev: generateSignedUrl,
            };
        } else if (generateSignedUrl.status == 1) {
            // let headers = getHeaders(objUser.user_cookie, userAgent);
            let signedUrl = generateSignedUrl.signedUrl;
            //console.log(signedUrl);
            let headers = getHeaders(objUser.user_cookie, userAgent);

            let formattedHeaders = {};
            headers.forEach((header) => {
                formattedHeaders[header.name + ""] = header.value;
            });

            let signedUrlData = {
                headers: formattedHeaders,
                type: "get",
            };

            var UserDetails = await utility.sendRequest(
                signedUrl,
                signedUrlData
            );

            //console.log("user code with uniqueid", UserDetails.statusCode);

            //  10222 Private account
            //  10221 Block account
            if (UserDetails && typeof UserDetails.statusCode === "undefined") {
                console.log(UserDetails);
                return {
                    status: -1,
                    message: "undefined",
                    message_dev: UserDetails,
                };
            } else if (
                (UserDetails && UserDetails.statusCode == 0) ||
                (UserDetails && UserDetails.statusCode == 10222)
            ) {
                //update global cookie
                global.userCookie = objUser.user_cookie;

                var response = { status: 1 };
                response.is_private_account = false;
                if (UserDetails.hasOwnProperty("userInfo")) {
                    response.user_info = UserDetails.userInfo;
                }
                if (UserDetails.hasOwnProperty("user_cookie")) {
                    response.user_cookie = UserDetails.user_cookie;
                }
                if (UserDetails && UserDetails.statusCode == 10222) {
                    await makeAccountPrivate(tiktop_userid);
                    response.is_private_account = true;
                }

                return response;
            } else {
                return {
                    status: 0,
                    message: "NOT-FOUND",
                    message_dev: UserDetails,
                };
            }
        } else {
            return {
                status: 0,
                message: "Signed URL not generate",
                message_dev: generateSignedUrl,
            };
        }
    } else {
        return {
            status: 2,
            message: "URL NOT GENERATE",
            message_dev: UserDetails,
        };
    }
};

const checkProfileWithUserId = async (objUser, userAgent) => {
    var tiktop_userid = objUser.tiktop_userid;
    if (
        !objUser.hasOwnProperty("user_cookie") ||
        objUser.user_cookie === null ||
        typeof objUser.user_cookie === "undefined"
    ) {
        objUser.user_cookie = global.user_cookie;
    }

    let data = {
        body: {
            action: "user-details-by-user-id",
            cookies: objUser.user_cookie,
            parameters: {
                //unique_id: objUser.uniqueId,
                user_id: tiktop_userid,
                is_user_id: true,
            },
        },
    };

    let url = await findurlArrForAction(data);
    if (url && url.url) {
        var generateSignedUrl = await createSignUrl(
            url.url,
            objUser.user_cookie,
            userAgent
        );
        if (generateSignedUrl.status == 0) {
            return {
                status: 2,
                message: "Signed URL not generate",
                message_dev: generateSignedUrl,
            };
        } else if (generateSignedUrl.status == 1) {
            // let headers = getHeaders(objUser.user_cookie, userAgent);
            let headers = getHeaders(objUser.user_cookie, userAgent);

            let formattedHeaders = {};
            headers.forEach((header) => {
                formattedHeaders[header.name + ""] = header.value;
            });

            let signedUrl = generateSignedUrl.signedUrl;

            let signedUrlData = {
                headers: formattedHeaders,
                type: "get",
            };

            var UserDetails = await utility.sendRequest(
                signedUrl,
                signedUrlData
            );

            if (UserDetails && typeof UserDetails.statusCode === "undefined") {
                console.log(UserDetails);
                return {
                    status: -1,
                    message: "undefined",
                    message_dev: UserDetails,
                };
            } else if (
                (UserDetails && UserDetails.statusCode == 0) ||
                (UserDetails && UserDetails.statusCode == 10222)
            ) {
                if (UserDetails.hasOwnProperty("user_cookie")) {
                    response.user_cookie = UserDetails.user_cookie;
                }
                var response = { status: 1 };
                response.is_private_account = false;
                if (UserDetails.hasOwnProperty("userInfo")) {
                    response.user_info = UserDetails.userInfo;
                }
                if (UserDetails.hasOwnProperty("user_cookie")) {
                    response.user_cookie = UserDetails.user_cookie;
                }
                if (UserDetails && UserDetails.statusCode == 10222) {
                    await makeAccountPrivate(tiktop_userid);
                    response.is_private_account = true;
                }

                return response;
            } else {
                return {
                    status: 0,
                    message: "NOT-FOUND",
                    message_dev: UserDetails,
                };
            }
        } else {
            return {
                status: 0,
                message: "Signed-URL-NOT-GENERATE",
                message_dev: generateSignedUrl,
            };
        }
    } else {
        return {
            status: 0,
            message: "URL-NOT-GENERATE",
            message_dev: UserDetails,
        };
    }
};

const updateUser = async (
    tiktop_userid,
    user_info,
    user_cookie,
    is_private_account,
    isUniqueidUpdate = false
) => {
    try {
        var updateObj = {
            uniqueId: user_info.user.uniqueId,
            user_info: user_info,
            is_profile_update: true,
            is_blocked: false,
            profile_updated_on: utility.getGmtTime(),
            is_private_account: is_private_account,
        };

        if (
            user_cookie &&
            user_cookie.hasOwnProperty("sessionid") &&
            user_cookie.hasOwnProperty("tt_csrf_token")
        ) {
            updateObj.user_cookie = user_cookie;
        }

        // console.log("tiktop_userid==>" + tiktop_userid);

        //return ;
        updateUserData = await userModel.findOneAndUpdate(
            {
                tiktop_userid: tiktop_userid,
            },
            updateObj,
            {
                new: true,
            }
        );

        if (updateUserData._id) {
            if (isUniqueidUpdate) {
                let UserSubscriptionModel = require("../models/user_subscription");
                var updateUserOrder = await UserSubscriptionModel.findOneAndUpdate(
                    { tiktop_userid: tiktop_userid, is_completed: false },
                    { entity: user_info.user.uniqueId, is_blocked: false },
                    { new: true }
                );
                if (updateUserOrder && updateUserOrder._id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } catch (error) {
        console.log(error);
    }
};

const blockUser = async (user_id) => {
    try {
        console.log("block user ", user_id);
        // return
        let UserSubscriptionModel = require("../models/user_subscription");
        var updateObj = {
            is_blocked: true,
            is_profile_update: true,
            profile_updated_on: utility.getGmtTime(),
        };
        var updateUser = await userModel.findOneAndUpdate(
            {
                _id: mongoose.Types.ObjectId(user_id),
            },
            updateObj,
            {
                new: true,
            }
        );
        console.log("block user update", updateUser);
        //update user subscription
        var updateObj = {
            is_blocked: true,
        };
        var updateUserOrder = await UserSubscriptionModel.findOneAndUpdate(
            {
                user_id: user_id,
                is_completed: false,
            },
            updateObj,
            { new: true }
        );
        console.log("block order update", updateUserOrder);
        if (updateUserOrder && updateUserOrder._id) {
            return true;
        }
    } catch (error) {
        console.log(error);
        return false;
    }
};

const createSignUrl = async (url, cookies, userAgent) => {
    try {
        // const Signer = require("tiktok-signature");

        let ttUrl = url;
        // let userAgent = userAgent;

        let failedVerificationFields = [];
        if (!ttUrl) {
            failedVerificationFields.push("ttUrl");
        }
        if (!userAgent) {
            failedVerificationFields.push("userAgent");
        }

        if (failedVerificationFields.length) {
            return {
                status: 0,
                message: "tt signature input validation failed",
                fields: failedVerificationFields,
            };
        } else {
            // const signer = new Signer(userAgent); // Create new signer

            try {
                // await signer.init(cookies); // Create page with. Returns promise
                // const signature = await signer.sign(ttUrl); // Get sign for your url. Returns promise
                // const token = await signer.getVerifyFp(); // Get verification token for your url. Returns promise
                //global signer carabot

                // await global.globalSigner.init(cookies);
                // const signature = await global.globalSigner.sign(ttUrl); // Get sign for your url. Returns promise
                // const token = await global.globalSigner.getVerifyFp();

                //global signer truecarry
                let signature = await global.globalSigner.sign(ttUrl);

                //await signer.close(); // Close browser. Returns promise
                //ttUrl += "&_signature="+signature;
                //ttUrl += "&verifyFp=" + token + "&_signature=" + signature;
                ttUrl += "&_signature=" + signature;
                return {
                    status: 1,
                    signature,
                    // token,
                    signedUrl: ttUrl,
                };
            } catch (err) {
                // await signer.close();
                return {
                    status: 0,
                    message: err.stack,
                };
            }
        }
    } catch (err) {
        return {
            status: 0,
            message: err.stack,
        };
    }
};

const wrongBlockedUsers = async (req, res) => {
    try {
        var blockedUser = await userModel
            .find({ is_profile_update: { $exists: true }, is_blocked: true })
            .select({ _id: 1 });
        if (blockedUser) {
            console.log("here");
            utility.sendResponse(req, res, 200, blockedUser);
        }
    } catch (error) {
        console.log(error);
    }
};

const makeAccountPrivate = async (tiktok_userId) => {
    try {
        await userModel.findOneAndUpdate(
            {
                tiktop_userid: tiktok_userId,
            },
            {
                is_private_account: true,
            }
        );
    } catch (error) {}
};

module.exports = {
    register,
    appVersion,
    generalInfo,
    getFollwPackage,
    getLikePackage,
    getTiktokSignedRequestDetails,
    autoLike,
    checkProfile,
    wrongBlockedUsers,
};
