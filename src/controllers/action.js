const mongoose = require("mongoose");
const utility = require("../util/utility");
const {
  message_obj,
  mailtext_obj,
  api_error_code
} = require("../lang/message");
const config = require("../config/config");
const userModel = require("../models/users");
const userViewAddModel = require("../models/userViewAdds");

require("dotenv").config();

/*new api for add coin on view add by user */
const AddCoinsViaAds = async (req,res) =>{
  try
  {
  
   
    let addcoin=   config.appConfig.addvertisementCoin;
    let updateStr = {
      updated_on: utility.getGmtTime(),
      $inc: { coin: addcoin }
    };
    let userId = mongoose.Types.ObjectId(req.headers.auth_user_id);
    let updateCoin = await userModel
      .findByIdAndUpdate(userId, updateStr, {
        new: true,
        rawResult: true
      });
      if(updateCoin){

     
        
        let outputObj = {
          status: 1,
          message: message_obj.coinAddforviewadd,
          coin: updateCoin.value.coin,
      };
        utility.sendResponse(req, res, 200, outputObj);
      }
          
   

  }catch( error){
    let outputObj = {
      status: 0,
      message: error.message,
      error_code: api_error_code.exception_code
    };
    utility.sendResponse(req, res, 200, outputObj);
  }
}

const getPromotedUser_ = async (req, res, next, sendJSONResponse = 0) => {
  try {
    
    userSubscriptionModel = require("../models/user_subscription");
    users = require("../models/users");
    var provided_media = req.body.provided_media;
  
    let userId = req.headers.auth_user_id;
    var arrPostIds = [];
    let arrUserIds = [""];
    let where = {
      user_id: { $ne: mongoose.Types.ObjectId(userId) },
      is_blocked: { $exists: false },
      $expr: { $gt: ["$count", "$completed"] },
    };
    
    if(sendJSONResponse) {
      where['type'] = "Like";
     // where["count"] = { $gte: 100 };
    }

    if (typeof provided_media !== "undefined") {
      if (provided_media.length > 0) {
      
        provided_media = provided_media.map(row=>mongoose.Types.ObjectId(row));

        let providedMediaIds = await userSubscriptionModel.find({_id: {$in:provided_media }});
        
        providedMediaIds.forEach(row => {
          if(row.type === 'Like') {
            arrPostIds.push(row.tiktop_postid)
          } else {
            arrUserIds.push(row.tiktop_userid)
          }
        })
//console.log("rightby",typeof provided_media[0]);
      // where._id = {$nin:provided_media }
      }else{
        provided_media  = [];
      }
    }

    let checkActionOfUser = await getActionUserMedia(userId);
    var arrPramot = [];
    
    for(var k =0 ; k < checkActionOfUser.length; k++){
      if(checkActionOfUser[k].promoted_id && checkActionOfUser[k].promoted_id.post_id) {
        arrPostIds.push(checkActionOfUser[k].promoted_id.post_id); 
      }
      if(checkActionOfUser[k].promoted_id && checkActionOfUser[k].promoted_id.tiktop_userid) {
        arrUserIds.push(checkActionOfUser[k].promoted_id.tiktop_userid); 
      }
    }

      if(arrPostIds.length){
        where.post_id = {$nin:arrPostIds }
      }

     // console.log("checkActionOfUsernisha",provided_media);
     
      // if(arrUserIds.length){
      //   where.tiktop_userid = {$nin: arrUserIds }
      // }
  
  
    var mediaData = await getPromotedMediaFromDB(where, arrUserIds);
    if (mediaData.length > 0) {
      let outputObj = {
        status: 1,
        promoted_media: mediaData
      };
      if(!sendJSONResponse) {
        utility.sendResponse(req, res, 200, outputObj);
      } else {
        return outputObj;
      }
    } else {
      let outputObj = {
        status: 0,
        message_dev: message_obj.promoted_media_not_found,
        error_code: api_error_code.promoted_media_not_found,
        message:message_obj.promoted_media_not_found
      };
      if(!sendJSONResponse) {
        utility.sendResponse(req, res, 200, outputObj);
      } else {
        return outputObj;
      }
    }
  } catch (e) {
    let outputObj = {
      status: 0,
      message: message_obj.server_error,
      message_dev: e.message,
      api_error_code: api_error_code.exception_code
    };
    if(!sendJSONResponse) {
      utility.sendResponse(req, res, 200, outputObj);
    } else {
      return outputObj;
    }
  }
};
const getActionUserMedia = async (user_id) =>{

  
  const actionModel = require("../models/user_actions");
  let where = {
    
    action_by: mongoose.Types.ObjectId(user_id)
  };

  let userAction = await actionModel.find(where).populate('promoted_id');
  if (userAction) {
    return userAction;
  } else {
    return false;
  }
}
const combineArrayObject = async (arr1, arr2) => {
  var array3 = [];
  var index;

  for (index = 0; index < arr1.length; index++) {
    array3.push(arr1[index]);
    if (index == arr1.length - 1) {
      for (var index2 = 0; index2 < arr2.length; index2++) {
        array3.push(arr2[index2]);
      }
    }
  }
  return array3;
};

const profileAction = async (req, res) => {
  try {
    
    const actionModel = require("../models/user_actions");
    var promoted_id = req.body.promoted_id;
    var type = req.body.type;

    var userId = req.headers.auth_user_id;
    //get promoted details
    const session = await mongoose.startSession();
   // session.startTransaction();
    //check if already take action
    let isAlreadyTakeAction = await checkActionLog(promoted_id, userId);

    let appVersion = req.headers.app_version;
	if (appVersion && appVersion < process.env.SEND_ERROR_TO_APP_VERSION) {
		let outputObj = {
			status: false,
			show_message_to_user: true,
			message: process.env.APP_VERSION_ERROR_MESSAGE,
		};
		return utility.sendResponse(req, res, 200, outputObj);
	}

    if (isAlreadyTakeAction) {
      
      var message;
      if (type == "Like") {
        message = message_obj.already_like;
        var error_code = api_error_code.already_like;
      } else if (type == "Follower") {
        message = message_obj.already_follow;
        var error_code = api_error_code.already_follow;
      }else if (type == "Skip") {
        message = message_obj.already_skip;
        var error_code = api_error_code.already_skip;
      }
      let outputObj = {
        status: 0,
        message: message,
        error_code: error_code
      };
     return  utility.sendResponse(req, res, 200, outputObj);
      
    }
    let promotedDetail = await getPromotedMedia(promoted_id);
   
    if (promotedDetail) {
     
    //  let type = promotedDetail.type;
      let count = promotedDetail.count;
      let completed = promotedDetail.completed;
      if (completed < count) {
        //insert into action collection
        let actionObj = [
          {
            promoted_id: promoted_id,
            type: type,
            action_by: req.headers.auth_user_id,
            action_to: promotedDetail.user_id,
            entity: promotedDetail.entity,
            created_on: utility.getGmtTime(),
            updated_on: utility.getGmtTime()
          }
        ];

        let insertAction = await actionModel.create(actionObj, {
          session: session
        });

        if (!insertAction.length) {
          console.log("error");
        }

        //update user subscrption
        let updateSubscription = await updateSubscriptionDetail(
          promoted_id,
          type,
          session
        );
        // console.log("updateSubscription", updateSubscription);

        if (updateSubscription.status == 1) {
          //entry in transaction log
          if (type == "Like") {
            var credits = config.appConfig.likeCoin;
          } else if (type == "Skip"){
            var credits = config.appConfig.skipCoin;
          }
          else {
            var credits = config.appConfig.followCoin;
          }
        
          let trasactionLogObj = [
            {
              credits: credits,
              action: type,
              transaction_type: "Earn",
              user_id: userId,
              created_on: utility.getGmtTime(),
              updated_on: utility.getGmtTime()
            }
          ];
          let transactionLog = await saveTrasactionLog(
            trasactionLogObj,
            session
          );

        

          if (transactionLog.status == 1) {
            //update user coin
            let updateStr = {
              updated_on: utility.getGmtTime(),
              $inc: { coin: credits }
            };
            let userId = mongoose.Types.ObjectId(req.headers.auth_user_id);
            let updateCoin = await userModel
              .findByIdAndUpdate(userId, updateStr, {
                new: true,
                rawResult: true
              })
              .session(session);
            
            if (updateCoin.lastErrorObject.n) {
             // await session.commitTransaction();
              session.endSession();
              if (type == "Like") {
                var message = message_obj.like_successfully;
              } else if(type == "Skip") {
                var message = message_obj.skip_successfully;
              }else{
                var message = message_obj.follow_successfully;
              }
              let outputObj = {
                status: 1,
                coin: updateCoin.value.coin,
                message: message
              };
              
            return   utility.sendResponse(req, res, 200, outputObj);
            } else {
             // await session.abortTransaction();
              session.endSession();
              let outputObj = {
                status: 0,
                message: message_obj.server_error,
                message_dev: updateSubscription.message,
                error_code: api_error_code.subscription_update_fail
              };
             return  utility.sendResponse(req, res, 200, outputObj);
            }
          } else {
            
            let outputObj = {
              status: 0,
              message: transactionLog.message,
              error_code: transactionLog.error_code
            };
         return  utility.sendResponse(req, res, 200, outputObj);
          }
        } else {
          //await session.abortTransaction();
          // session.endSession();
          let outputObj = {
            status: 0,
            message: message_obj.server_error,
            message_dev: updateSubscription.message,
            error_code: api_error_code.subscription_update_fail
          };
      return   utility.sendResponse(req, res, 200, outputObj);
        }
      } else {
        
        let outputObj = {
          status: 0,
          message: message_obj.promoted_media_target_complete,
          error_code: api_error_code.promoted_media_target_complete
        };
     return  utility.sendResponse(req, res, 200, outputObj);
      }
    } else {
     
      let outputObj = {
        
        status: 0,
        message: message_obj.promoted_media_not_found,
        message_dev: message_obj.promoted_media_not_found,
        error_code: api_error_code.promoted_media_not_found
      };
    return   utility.sendResponse(req, res, 200, outputObj);
    }
  } catch (error) {
    let outputObj = {
      status: 0,
      message: error.message,
      error_code: api_error_code.exception_code
    };
    utility.sendResponse(req, res, 200, outputObj);
  }
};

const getPromotedMedia = async promotedId => {
  try {
    const userSubscriptionModel = require("../models/user_subscription");
    let promotedMedia = await userSubscriptionModel.findById(
      mongoose.Types.ObjectId(promotedId)
    );

    if (promotedMedia) {
      return promotedMedia;
    } else {
      return false;
    }
  } catch (error) {
    return false;
  }
};

const checkActionLog = async (promoted_id, user_id) => {
  const actionModel = require("../models/user_actions");
  let where = {
    promoted_id: mongoose.Types.ObjectId(promoted_id),
    action_by: mongoose.Types.ObjectId(user_id)
  };

  let userAction = await actionModel.findOne(where);

  if (userAction) {
    return true;
  } else {
    return false;
  }
};

const updateSubscriptionDetail = async (promotedId, type, session) => {
  try {
    const userSubscriptionModel = require("../models/user_subscription");
    promotedId = mongoose.Types.ObjectId(promotedId);
    let updateStr = {
      $inc: { completed: (type == "Skip" ? 0 : 1) },
      updated_on: utility.getGmtTime()
    };

    let updateComplete = await userSubscriptionModel
      .findOneAndUpdate({ _id: promotedId }, updateStr, {
        new: true,
        rawResult: true
      })
      .session(session);

    if (updateComplete.lastErrorObject.updatedExisting) {
      return {
        status: 1,
        message: "success"
      };
    } else {
     // await session.abortTransaction();
      session.endSession();
      return {
        status: 0,
        message: message_obj.subscription_update_fail
      };
    }
  } catch (error) {
   // await session.abortTransaction();
    session.endSession();
    return {
      status: 0,
      message: error.message
    };
  }
};

const saveTrasactionLog = async (transactionLogObj, session) => {
  try {
    const transactionModel = require("../models/transaction_logs");
    let saveTrsaction = await transactionModel.create(transactionLogObj, {
      session: session
    });

    if (saveTrsaction) {
      return {
        status: 1,
        message: "success"
      };
    } else {
      //await session.abortTransaction();
      session.endSession();
      return {
        status: 0,
        message: message_obj.transaction_log_entry_fail,
        error_code: api_error_code.transaction_log_entry_fail
      };
    }
  } catch (error) {
   // await session.abortTransaction();
    session.endSession();
    return {
      status: 0,
      message: error.message,
      error_code: api_error_code.exception_code
    };
  }
};

const getPromotedMediaFromDB = async (where, arrUserIds) => {
  let randomNumber = parseInt(Math.random()*10);
  let randomSort = randomNumber <= 5 ? 1 : -1;

  let sort = { created_on: randomSort };
  if (where.type) {
		sort = { _id: 1, count: -1, completed: -1 };
  }

	userSubscriptionModel = require("../models/user_subscription");

	let data = await userSubscriptionModel.aggregate([
		{
			$match: {
				$and: [where],
				$or: [
					{
						is_video_deleted: { $exists: false },
					},
					{
						$and: [
              { 
                is_video_deleted: { $exists: true } 
              }, 
              { 
                is_video_deleted: false 
              }
            ],
					},
				],
			},
		},
		{
			$lookup: {
				from: "users",
				localField: "user_id",
				foreignField: "_id",
				as: "users",
			},
		},
		{
			$unwind: "$users",
		},
		{
			$group: {
				_id: "$_id",
				username: { $first: "$users.username" },
				avatar_thumb: { $first: "$users.avatar_thumb" },
				avatar_larger: { $first: "$users.avatar_larger" },
				nick_name: { $first: "$users.nick_name" },
				uniqueId: { $first: "$users.uniqueId" },
				media_data: {
					$push: {
						pramote_id: "$_id",
						type: "$type",
						user_id: "$user_id",
						package_id: "$package_id",
						completed: "$completed",
						entity: "$entity",
						created_on: "$created_on",
						count: "$count",
						thumb_url: "$thumb_url",
						cover_url: "$cover_url",
						tiktop_userid: "$tiktop_userid",
						tiktop_postid: "$tiktop_postid",
					},
				},
			},
		},
		{
			$unwind: "$media_data",
		},
		{
			$sort: { "media_data.created_on": randomSort },
		},
		{
			$group: {
				_id: {
					$concat: [
						{ $convert: { input: "$media_data.tiktop_userid", to: "string" } },
						"-",
						{ $convert: { input: "$media_data.tiktop_postid", to: "string" } },
					],
				},
				row_id: { $first: "$_id" },
				username: { $first: "$username" },
				avatar_thumb: { $first: "$avatar_thumb" },
				avatar_larger: { $first: "$avatar_larger" },
				nick_name: { $first: "$nick_name" },
				uniqueId: { $first: "$uniqueId" },
				status: { $first: "$status" },
				tiktop_postid: { $first: "$tiktop_postid" },
				tiktop_userid: { $first: "$tiktop_userid" },
				media_data: {
					$push: {
						pramote_id: "$media_data._id",
						type: "$media_data.type",
						user_id: "$media_data.user_id",
						package_id: "$media_data.package_id",
						completed: "$media_data.completed",
						entity: "$media_data.entity",
						count: "$media_data.count",
						thumb_url: "$media_data.thumb_url",
						cover_url: "$media_data.cover_url",
						tiktop_userid: "$media_data.tiktop_userid",
						tiktop_postid: "$media_data.tiktop_postid",
						created_on: "$media_data.created_on",
					},
				},
			},
		},
		{
			$match: {
				$and: [
					{
						status: { $ne: "DELETED" },
					},
				],
			},
		},
		{
			$project: {
				_id: "$row_id",
				username: 1,
				avatar_thumb: 1,
				avatar_larger: 1,
				nick_name: 1,
				uniqueId: 1,
				tiktop_userid: 1,
				tiktop_postid: 1,
				media_data: {
					$slice: ["$media_data", 0, 1],
				},
			},
		},
		{
			$project: {
				_id: 1,
				username: 1,
				avatar_thumb: 1,
				avatar_larger: 1,
				nick_name: 1,
				uniqueId: 1,
				tiktop_userid: 1,
				tiktop_postid: 1,
				pramote_id: {
					$arrayElemAt: ["$media_data._id", 0],
				},
				type: {
					$arrayElemAt: ["$media_data.type", 0],
				},
				user_id: {
					$arrayElemAt: ["$media_data.user_id", 0],
				},
				package_id: {
					$arrayElemAt: ["$media_data.package_id", 0],
				},
				completed: {
					$arrayElemAt: ["$media_data.completed", 0],
				},
				entity: {
					$arrayElemAt: ["$media_data.entity", 0],
				},
				count: {
					$arrayElemAt: ["$media_data.count", 0],
				},
				thumb_url: {
					$arrayElemAt: ["$media_data.thumb_url", 0],
				},
				cover_url: {
					$arrayElemAt: ["$media_data.cover_url", 0],
				},
				tiktop_userid: {
					$arrayElemAt: ["$media_data.tiktop_userid", 0],
				},
				tiktop_postid: {
					$arrayElemAt: ["$media_data.tiktop_postid", 0],
				},
				created_on: {
					$arrayElemAt: ["$media_data.created_on", 0],
				},
			},
		},
		{
			$group: {
				_id: "$type",
				media_data: {
					$push: {
						row_id: "$_id",
						username: "$username",
						type: "$type",
						completed: "$completed",
						entity: "$entity",
						count: "$count",
						uniqueId: "$uniqueId",
						avatar_thumb: "$avatar_thumb",
						avatar_larger: "$avatar_larger",
						nick_name: "$nick_name",
						tiktop_userid: "$tiktop_userid",
						tiktop_postid: "$tiktop_postid",
						pramote_id: "$pramote_id",
						user_id: "$user_id",
						package_id: "$package_id",
						thumb_url: "$thumb_url",
						cover_url: "$cover_url",
						tiktop_userid: "$tiktop_userid",
						tiktop_postid: "$tiktop_postid",
						created_on: "$created_on",
					},
				},
			},
		},
		{
			$project: {
				_id: 1,
				media_data: {
					$cond: {
						if: {
							$and: [
								{
									$eq: ["$_id", "Follower"],
								},
							],
						},
						then: {
							$filter: {
								input: "$media_data",
								as: "item",
								cond: { $not: { $in: ["$$item.tiktop_userid", arrUserIds] } },
							},
						},
						else: "$media_data",
					},
				},
			},
		},
		{
			$unwind: "$media_data",
		},
		{
			$project: {
				_id: "$media_data.row_id",
				username: "$media_data.username",
				type: "$media_data.type",
				completed: "$media_data.completed",
				entity: "$media_data.entity",
				count: "$media_data.count",
				uniqueId: "$media_data.uniqueId",
				avatar_thumb: "$media_data.avatar_thumb",
				avatar_larger: "$media_data.avatar_larger",
				nick_name: "$media_data.nick_name",
				tiktop_userid: "$media_data.tiktop_userid",
				tiktop_postid: "$media_data.tiktop_postid",
				pramote_id: "$media_data.pramote_id",
				user_id: "$media_data.user_id",
				package_id: "$media_data.package_id",
				thumb_url: "$media_data.thumb_url",
				cover_url: "$media_data.cover_url",
				tiktop_userid: "$media_data.tiktop_userid",
				tiktop_postid: "$media_data.tiktop_postid",
				created_on: "$media_data.created_on",
			},
		},
		{
			$sort: sort,
		},
		{
			$limit: where.type ? 50 : config.appConfig.promoted_media_limit,
		},
	]);

	return data;
};

const getPromotedUser = async (req, res, next, sendJSONResponse = 0) => {
    try {
        var provided_media = req.body.provided_media;
        let userId = req.headers.auth_user_id;

        //user action media
        var userActionMedia = await getUserActionMedia(userId);

        userActionMedia = userActionMedia.map(function (e) {
            return e.unique_identifier;
        });

        //get new promoted media
        var promotedMedia = await getNewPromotedMedia(
            userId,
            userActionMedia,
            provided_media
        );

        if (promotedMedia) {
            promotedMedia = JSON.parse(JSON.stringify(promotedMedia));

            for (let index = 0; index < promotedMedia.length; index++) {
                let promotedUser = promotedMedia.user_id;
                //get user detail
                let userDetail = await getUsetDetail(
                    promotedMedia[index].user_id
                );

                if (userDetail) {
                    (promotedMedia[index].uniqueId = userDetail.uniqueId),
                        (promotedMedia[index].tiktop_userid =
                            userDetail.tiktop_userid),
                        (promotedMedia[index].user_id = userDetail._id);
                    (promotedMedia[index].username = userDetail.username),
                        (promotedMedia[index].avatar_thumb =
                            userDetail.avatar_thumb),
                        (promotedMedia[index].avatar_larger =
                            userDetail.avatar_larger),
                        (promotedMedia[index].nick_name = userDetail.nick_name);

                    //promotedMediaArr.push(mediaObj);
                }
            }
            let outputObj = {
                status: 1,
                promoted_media: promotedMedia,
            };
            return outputObj;
           // utility.sendResponse(req, res, 200, outputObj);
        } else {
            let outputObj = {
                status: 0,
                message_dev: message_obj.promoted_media_not_found,
                error_code: api_error_code.promoted_media_not_found,
                message: message_obj.promoted_media_not_found,
            };

            utility.sendResponse(req, res, 200, outputObj);
        }
    } catch (error) {
        let outputObj = {
            status: 0,
            message: error.message,
            error_code: api_error_code.exception_code,
            error: error.stack,
        };
        utility.sendResponse(req, res, 200, outputObj);
    }
};

const getUserActionMedia = async (userId) => {
    try {
        const actionModel = require("../models/user_actions");
        let where = {
            action_by: mongoose.Types.ObjectId(userId),
        };
        //console.log("where");
        //console.log(where);

        let userAction = await actionModel
            .find(where)
            .select({ unique_identifier: 1, _id: -1 });
        if (userAction) {
            // console.log(userAction);
            return userAction;
        } else {
            return false;
        }
    } catch (error) {
        console.log(error);
        return false;
    }
};

const getNewPromotedMedia = async (userId, actionMedia, providedMedia) => {
    try {
        userSubscriptionModel = require("../models/user_subscription");
        var where = {
            type:"Like",
            is_completed: false,
            $or: [
                {
                    is_video_deleted: { $exists: false },
                },
                {
                    $and: [
                        { is_video_deleted: { $exists: true } },
                        { is_video_deleted: false },
                    ],
                },
            ],

            //_id: { $nin: providedMedia },

            unique_identifier: { $nin: actionMedia },
            user_id: { $ne: userId },
        };
        if (providedMedia) {
            where._id = { $nin: providedMedia };
        }

        var media = await userSubscriptionModel
            .find(where)
            .select({
                _id: 1,
                type: 1,
                completed: 1,
                cover_url: 1,
                entity: 1,
                count: 1,
                package_id: 1,
                video_url: 1,
                thumb_url: 1,
                post_id: 1,
                user_id: 1,
                created_on: 1,
                package_id: 1,
                tiktop_postid: 1,
            })
            .sort({ created_on: 1 })
            .limit(20);

        if (media.length > 0) {
            return media;
        } else {
            return false;
        }
    } catch (error) {

        console.log(error);
        return false;
    }
};

const getUsetDetail = async (userId) => {
    var userDetail = await userModel.findById(userId).select({
        username: 1,
        uniqueId: 1,
        avatar_thumb: 1,
        avatar_larger: 1,
        nick_name: 1,
        tiktop_userid: 1,
    });
    if (userDetail) {
        return userDetail;
    }
};

module.exports = {
  getPromotedUser,
  profileAction,
  AddCoinsViaAds
};
