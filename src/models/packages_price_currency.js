const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModelConstants = require('./BaseModel').constants;

const PackagesPriceCurrency = new Schema (
    {
        package: {type: Schema.Types.String, required: true},
        currency: {type: Schema.Types.String, required: true},
        price: {type: Schema.Types.String, required: true},
        device_id: {type: Schema.Types.String, required: true},
    },{
        collection: 'packages_price_currency',
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        }
    }
);

Object.assign(PackagesPriceCurrency.statics, BaseModelConstants);

module.exports = mongoose.model('PackagesPriceCurrency', PackagesPriceCurrency);