const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModelConstants = require('./BaseModel').constants;

const HashtagSubscriptionTransactionDetail = new Schema (
    {
        device_id: {type: String, required:true},
        device_type: {type: String, required: true},
        parent_transaction_id: {type: String, required: true},
        subscription_token: {type: String, required: true},
        response_status: {type: String, required: false, default: ""},
        error_message: {type: String, required: false, default: ""},
        auto_renewing: {type: Number, required: false, default: 0},
        is_in_billing_retry: {type: Number, required: false, default: 0},
        latest_receipt: {type: String, required: false},

    },{
        collection: 'Hashtagsubscription_transaction_detail',
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        }
    }
);

Object.assign(HashtagSubscriptionTransactionDetail.statics, BaseModelConstants);

module.exports = mongoose.model('HashtagSubscriptionTransactionDetail', HashtagSubscriptionTransactionDetail);