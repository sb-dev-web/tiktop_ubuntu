const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userModel = require("./users");
const userActionSchema = new Schema(
    {
        // name: { type: String, required: true },
        promoted_id: { type: mongoose.ObjectId, required: true, ref: "user_subscriptions" },
        action_by: { type: mongoose.ObjectId, required: true },
        type: { type: String, required: false },
        action_to:{
            type:mongoose.ObjectId
        },
        entity :{
            type:String
        },
        created_on: {
            type: Date,
            default: Date.now,
        },
        updated_on: {
            type: Date,
        },
        deleted_on: {
            type: Date,
        },
        tiktop_postid: {
            type: String,
        },
        tiktop_userid: {
            type: String,
        },
        action_type: {
            type: String,
            default: "manual",
        },
        unique_identifier: {
            type: String,
        },
        is_completed: {
            type: Boolean,
            default: false
        },
        is_video_deleted: {
            type: Boolean,
            default: false
        },
        is_moved_to_order_actions: {
            type: Boolean,
            default: false
        },
        is_moved_to_order_action_error: {
            type: String,
            default: ""
        },
    },
    {
        versionKey: false,
    },

    {
        collection: "user_actions",
    }
);

module.exports = mongoose.model("user_actions", userActionSchema);
