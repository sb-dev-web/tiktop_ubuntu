const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const transactionSchema = new Schema(
    {
        credits: { type: String, required: true },
        package_id: {
            type: mongoose.ObjectId,
            //require: true,
        },
        user_id:{
            type:mongoose.ObjectId
        },
        transaction_id: {
            type: String,
        },
        action: {
            type: String,//Like,Follow,Purchase 
        },
        transaction_type: {
            type: String,//Wallet, Purchase,Earn  
            required: true,
        },
        created_on: {
            type: Date,
            default: Date.now,
        },
        updated_on: {
            type: Date,
        },
        deleted_on: {
            type: Date,
        },
    },
    {
        versionKey: false,
    },

    {
        collection: "transaction_logs",
    }
);

module.exports = mongoose.model("transaction_logs", transactionSchema);
