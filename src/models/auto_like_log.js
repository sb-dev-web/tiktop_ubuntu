const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userModel = require("./users");
const userActionLogSchema = new Schema(
    {
        // name: { type: String, required: true },
        promoted_id: { type: mongoose.ObjectId, required: true, ref: "user_subscriptions" },
        action_by: { type: mongoose.ObjectId, required: true },
        type: { type: String, required: false },
        action_to:{
            type:mongoose.ObjectId
        },
        entity :{
            type:String
        },
        created_on: {
            type: Date,
            default: Date.now,
        },
        updated_on: {
            type: Date,
        },
        deleted_on: {
            type: Date,
        },
        tiktop_postid: {
            type: String,
        },
        tiktop_userid: {
            type: String,
        },
        action_type: {
            type: String,
            default: "manual",
        },
        headers:{
            type:Object
        },
        tiktok_url:{
            type:String
        },
        tiktop_response: {
            type: Object,
        },
        is_success: {
            type: Number,
            default: 0
        }
    },
    {
        versionKey: false,
    },

    {
        collection: "user_actions_log",
    }
);

module.exports = mongoose.model("user_actions_log", userActionLogSchema);
