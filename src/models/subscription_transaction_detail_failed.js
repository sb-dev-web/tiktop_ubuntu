const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModelConstants = require('./BaseModel').constants;

const SubscriptionTransactionDetailFailed = new Schema (
    {
        user_id: {type: mongoose.Types.ObjectId, required: true, ref: 'Users'},
        device_type: {type: String, required: true},
        subscription_token: {type: String, required: true},
        response_status: {type: String, required: false, default: ''},
        error_message: {type: String, required: false, default: ''},
    },{
        versionKey: false,
        collection: 'subscription_transaction_detail_failed',
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        }
    }
);

Object.assign(SubscriptionTransactionDetailFailed.statics, BaseModelConstants);

module.exports = mongoose.model('SubscriptionTransactionDetailFailed', SubscriptionTransactionDetailFailed);