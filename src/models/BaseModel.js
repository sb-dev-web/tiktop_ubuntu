const constants = Object.freeze({
    IOS_DEVICE_TYPE: 1,
    ANDROID_DEVICE_TYPE: 2,

    IS_PREMIUM: 1,
    NOT_IS_PREMIUM: 0,

    IS_SUPER_PREMIUM: 1,
    NOT_IS_SUPER_PREMIUM: 0,
});

module.exports.constants = constants;