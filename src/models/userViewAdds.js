const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userViewAdds = new Schema(
    {
        //  email: { type: String, required: true },
        // salt: { type: String, required: true },
        // password: { type: String, required: true },
        user_id: { type: mongoose.ObjectId, required: true },
        add_id: {
            type: String,
            required: true
        },
       
        created_on: {
            type: Date,
            default: Date.now,
        },
        updated_on: {
            type: Date,
        },
        uniqueId:{
            type:String,  
            default: ""
        },
        tiktop_userid:{
            type:String,
            default:""
        }
    },
    {
        versionKey: false,
    },
    {
        collection: "userViewAdds",
    }
);

module.exports = mongoose.model("userViewAdds", userViewAdds);
