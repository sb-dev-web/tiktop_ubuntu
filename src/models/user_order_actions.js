const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userOrderActionSchema = new Schema(
    {
        user_id: {
            type: mongoose.ObjectId,
            require: true,
            ref: "Users",
        },
        tiktop_userid: {
            type: String,
        },
        like_actions: {
            type: Array,
            default: []
        },
        follow_actions: {
            type: Array,
            default: []
        },
        unique_actions: {
            type: Array,
            default: []
        },
        skip_like_actions: {
            type: Array,
            default: []
        },
        skip_follow_actions: {
            type: Array,
            default: []
        },
        skip_unique_actions: {
            type: Array,
            default: []
        },
        like_promoted_ids: {
            type: Array,
            default: []
        },
        follow_promoted_ids: {
            type: Array,
            default: []
        },
        skip_like_promoted_ids: {
            type: Array,
            default: []
        },
        skip_follow_promoted_ids: {
            type: Array,
            default: []
        },
        like_count: { type: Number, default: 0 },
        follow_count: { type: Number, default: 0 },
        skip_like_count: { type: Number, default: 0 },
        skip_follow_count: { type: Number, default: 0 },
    },
    {
        versionKey: false,
        timestamps: {
            createdAt: "created_on",
            updatedAt: "updated_on",
        },
        collection: "user_order_actions",
    },
);

module.exports = mongoose.model("user_order_actions", userOrderActionSchema);
