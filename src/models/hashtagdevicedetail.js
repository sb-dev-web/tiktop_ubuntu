const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const deviceDetail = new Schema ({
    device_name: { type: String, required: true },
    device_type: { type: String, required: true },
    os_type: { type: String, required: true },
    os_version:{
        type:String
    },
    app_version:{
        type:Number
    },
    app_mode:{
        type:String
    },
    app_environment:{
        type:String
    },
    user_id:{
        type:mongoose.ObjectId
    },
    oauth:{
        type:String,
    },
    device_id:{
        type:String
    },
    device_token:{
        type:String
    },
    prefered_language:{
        type:String,
        default:""

    },
    localization_language:{
        type:String,
        default:""
    },
    app_language:{
        type:String,
        default:""
    },
    created_on:{
        type:Date
    },
    updated_on:{
        type:Date
    },
   coin : {
       type:Number,
       default:0
   }
},{
    versionKey: false,
},
{
    collection: "Hashtagdevice_detail"
});
module.exports = mongoose.model('Hashtagdevice_detail', deviceDetail);
