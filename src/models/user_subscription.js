const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const userModel = require("./users");
const userSubcriptionSchema = new Schema(
    {
       // name: { type: String, required: true },
        credits: { type: Number, required: true },
        count: { type: Number, required: true },
        type: { type: String, required: false },
        completed:{
                type:Number,
                required:true
        },
        user_id:{
            type:mongoose.ObjectId,
            require:true,
            ref:'Users'
        },
        entity:{
            type:String,
            default:""
        
        }, 
        post_id:{
            type:String,
            default:""
        },
        thumb_url:{
            type:String
        },
        cover_url:{
            type:String,
            default:""
        },
        video_url:{
            type:String
        },
        like_count:{
            type:Number
        },
        video_title:{
            type:String,
            default:""
        },

        package_id:{
            type:mongoose.ObjectId,
            require:true,
        },
        created_on: {
            type: Date,
            default: Date.now
        },
        updated_on: {
            type: Date
        },
        deleted_on: {
            type: Date
        },
        tiktop_postid:{
            type:String,
            default:""
        },
        tiktop_userid:{
            type:String,
            default:""
        },
        is_video_deleted:{
            type: Boolean,
            default:false
        },
        is_blocked:{
            type:Boolean
        }

        
    },
    {
        versionKey: false
    },

    {
        collection: "user_subscriptions"
    }
);

module.exports = mongoose.model("user_subscriptions", userSubcriptionSchema);
