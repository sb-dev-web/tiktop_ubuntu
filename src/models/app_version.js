const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const appVersion = new Schema ({
    name: { type: String, required: true },
    app_version: { type: String, required: true },
    app_mode: { type: String, required: true },
    is_require: { type: Boolean, required: false },
    update_require:{type:Boolean, required:false},
    mandatory:{type:Boolean, required:false},
    os_type: { type: String, required: false, default: 1 },
    user_agent :{ type:String , required:false, default:""},
    tt_url_to_load: {type: String, required: false, default: "https://www.tiktok.com/foryou?lang=en"},
    signature_page_user_agent: {type: String, required: false, default: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36"},
    initial_ad_show_count: {type: Number, required: false, default: 15},
    repeat_ad_show_count: {type: Number, required: false, default: 20},
    interestial_ad_unit_id: {type: String, required: false, default: ""},
    reward_ad_unit_id: {type: String, required: false, default: ""},
    app_link: {
        type:String
    }
// },{
//     versionKey: false,
//     timestamps: { 
//         createdAt: 'created_on', 
//         updatedAt: 'updated_on', 
//     }, 
// },
},
{
    collection: "app_version"
});


module.exports = mongoose.model('app_version', appVersion);
