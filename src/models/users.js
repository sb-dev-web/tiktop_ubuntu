const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Users = new Schema(
    {
        //  email: { type: String, required: true },
        // salt: { type: String, required: true },
        // password: { type: String, required: true },
        username: { type: String },
        //  is_active: { type: String, required: false, default: 1 },
        coin: {
            type: Number,
        },
        status: {
            type: String,
            default:"ACTIVE"
        },
        nick_name :{
            type:String,
            required:true
        },
        avatar_thumb:{
            type:String
        },
        avatar_larger:{
            type:String
        },
        tiktok_avatar_thumb:{
            type:String
        },
        tiktok_avatar_larger:{
            type:String
        },
        created_on: {
            type: Date,
            default: Date.now,
        },
        updated_on: {
            type: Date,
        },
        uniqueId:{
            type:String,  
            default: ""
        },
        tiktop_userid:{
            type:String,
            default:""
        },
        auto_like:{
            type: Number,
            default: 0
        },
        auto_like_reused:{
            type:Number
        },
        is_image_updated:{
            type: Number,
            default: 0
        },
        is_profile_update:{
            type:Boolean
        },
        is_image_updation_failed:{
            type: Number,
            default: 0
        },
        image_processed_on:{
            type: Date,
            default: new Date()
        },
        is_blocked:{
            type:Boolean
        },
        profile_updated_on:{
            type:Date
        },
        is_private_account:{
            type:Boolean
        }
    },
    {
        versionKey: false,
    },
    {
        collection: "users",
    },
);

module.exports = mongoose.model("Users", Users);
