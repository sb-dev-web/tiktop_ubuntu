const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModelConstants = require('./BaseModel').constants;

const SubscriptionPackages = new Schema (
    {
        price: {type: String, required: true},
        product_id: {type: String, required: true},
        device_type: {type: String, required: true},
        label: {type: String, required: true},
    },{
        versionKey: false,
        collection: 'subscription_packages',
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        }
    }
);

Object.assign(SubscriptionPackages.statics, BaseModelConstants);

module.exports = mongoose.model('SubscriptionPackages', SubscriptionPackages);