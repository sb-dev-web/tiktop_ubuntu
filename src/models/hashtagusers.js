const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Users = new Schema(
    {
        //  email: { type: String, required: true },
        // salt: { type: String, required: true },
        // password: { type: String, required: true },
        username: { type: String },
        //  is_active: { type: String, required: false, default: 1 },
        coin: {
            type: Number,
        },
        status: {
            type: String,
            default:"ACTIVE"
        },
      
        avatar_thumb:{
            type:String
        },
        avatar_larger:{
            type:String
        },
        created_on: {
            type: Date,
            default: Date.now,
        },
        updated_on: {
            type: Date,
        },
       

    },
    {
        versionKey: false,
    },
    {
        collection: "hashtagusers",
    }
);

module.exports = mongoose.model("hashtagusers", Users);
