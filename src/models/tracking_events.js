const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModel = require('./BaseModel');

const TrackingEvents = new Schema ({
    name: { type: String, required: true },
    code: { type: String, required: true },
},{
    collection: 'tracking_events',
    timestamps: { 
        createdAt: 'created_at', 
        updatedAt: 'updated_at', 
    },
});

const constants = {...BaseModel};

Object.assign(TrackingEvents.statics, constants);

module.exports = mongoose.model('TrackingEvents', TrackingEvents);