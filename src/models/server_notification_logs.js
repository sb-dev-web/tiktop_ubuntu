const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModelConstants = require('./BaseModel').constants;

const ServerNotificationLogs = new Schema (
    {
        device_type: {type: Schema.Types.String, required: true},
        notification_type: {type: Schema.Types.String, required: true},
        product_id: {type: Schema.Types.String, required: true},
        original_transaction_id: {type: Schema.Types.String, required: false},
        transaction_id: {type: Schema.Types.String, required: false},
        purchase_date: {type: Schema.Types.Date, required: false},
        expires_date: {type: Schema.Types.Date, required: false},
        original_purchase_date: {type: Schema.Types.Date, required: false},
        is_trial_period: {type: Schema.Types.String, required: false},
        is_in_intro_offer_period: {type: Schema.Types.String, required: false},
        token: {type: Schema.Types.String, required: true},//purchase token
        response: {type: Schema.Types.String, required: true},//webhook response
        decoded_response: {type: Schema.Types.String, required: false},//webhook data decoded response
        google_receipt_response: {type: Schema.Types.String, required: false},//after token validation response
        apple_receipt_response: {type: Schema.Types.String, required: false},//after token validation response
        adjust_response: {type: Schema.Types.String, required: false},//after token event fire response
        receipt_process_response: {type: Schema.Types.String, required: false},//after token validation response
    },{
        collection: 'server_notification_logs',
        timestamps: {
            createdAt: 'created_at',
        }
    }
);

Object.assign(ServerNotificationLogs.statics, BaseModelConstants);

module.exports = mongoose.model('ServerNotificationLogs', ServerNotificationLogs);