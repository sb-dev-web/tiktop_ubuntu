const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const verifyFp = new Schema(
  {
    verify_fp: { type: String },
  },
  {
    collection: "verify_fp",
  }
);

module.exports = mongoose.model("verify_fp", verifyFp);
