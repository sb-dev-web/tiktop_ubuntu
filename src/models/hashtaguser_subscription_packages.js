const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModelConstants = require('./BaseModel').constants;

const ModelConstants = Object.freeze({
    IS_FREE_TRIAL: 1,
    NOT_FREE_TRIAL: 0,
    IS_START_AS_PAID_TRANSACTION: 1,
    IS_NOT_START_AS_PAID_TRANSACTION: 0,
    IS_RENEWAL: 1,
    IS_NOT_RENEWAL: 0,
    IS_TRIAL_CONVERT_TO_PAID: 1,
    IS_NOT_TRIAL_CONVERT_TO_PAID: 0,
    IS_EVENT_SENT: 1,
    NOT_EVENT_SENT: 0,
    
    IS_FIREBASE_EVENT_SENT: 1,
    NOT_FIREBASE_EVENT_SENT: 0,
    
    IS_NEW_ACTIVATION: 1,
    NOT_NEW_ACTIVATION: 0,


    IS_BILLING_RETRY_CONVERSION: 1,
    NOT_BILLING_RETRY_CONVERSION: 0,
    IS_BILLING_RETRY_RENEWAL: 1,
    NOT_BILLING_RETRY_RENEWAL: 0,

    IS_REACTIVATION: 1,
    NOT_REACTIVATION: 0,

});

const HashtagUserSubscriptionPackages = new Schema (
    {
        device_id: {type: String, required: true},
        //subscription_transaction_parent_id: {type: Schema.Types.ObjectId, required: true, ref: 'SubscriptionTransactionDetail'},
        subscription_package_id: {type: Schema.Types.ObjectId, required: true, ref: 'SubscriptionPackages'},
        transaction_id: {type: String, required: true},
       // end_date: {type: Date, required: true},
        description: {type: String, required: false},
        //deleted_at: {type: Date, required: false},
        //is_free_trial: {type: Number, required: false, default: 0},
        //currency: {type: String, required: false, default: ''},
        price: {type: String, required: false, default: '0'},
        //gps_adid: {type: String, required: false, default: ''},
        //idfa_id: {type: String, required: false, default: ''},
        //adid: {type: String, required: false, default: ''},
        //idfv: {type: String, required: false, default: ''},
        //adjust_event_id: {type: String, required: false, default: ''},
        //adjust_event_name: {type: String, required: false, default: ''},

        //is_event_sent: {type: Number, required: false, default: 0},
        //retry_count: {type: Number, required: false, default: 0},
        //is_firebase_event_sent: {type: Number, required: false, default: 0},
        //firebase_retry_count: {type: Number, required: false, default: 0},
        //is_renewal: {type: Number, required: false, default: 0},
       // is_trial_convert_to_paid: {type: Number, required: false, default: 0},
       // is_start_as_paid_transaction: {type: Number, required: false, default: 0},
      //  adjust_event_sent_date: {type: Date, required: false},
      //  firebase_event_sent_date: {type: Date, required: false},
      //  adjust_parameters: {type: String, required: false},
     //   adjust_response: {type: String, required: false},
      //  cancellation_date: {type: Date, required: false},
        purchase_date: {type: Date, required: false},

        subscription_package_name: {type: String, required: false},
        calling_from: {type: String, required: false},

        //is_new_activation: {type: Number, required: false, default: 0},

        //web_order_line_item_id: {type: String, required: false, default: 0},

        //is_processed: {type: Number, required: false, default: 0},
        //processed_at: {type: Date, required: false},

        //is_billing_retry_renewal: {type: Number, required: false, default: 0},
        //is_billing_retry_conversion: {type: Number, required: false, default: 0},

        //parent_transaction_id: {type: String, required: false, default: ''},

       // is_reactivation: {type: Number, required: false, default: 0},
    },{
        collection: 'hashtaguser_subscription_packages',
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        }
    }
);

const constants = {...BaseModelConstants, ...ModelConstants};

Object.assign(HashtagUserSubscriptionPackages.statics, constants);

module.exports = mongoose.model('HashtagUserSubscriptionPackages', HashtagUserSubscriptionPackages);