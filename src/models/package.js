const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const packages = new Schema(
    {
        name: { type: String, required: true },
        credits: { type: String, required: true },
        count: { type: String, required: true },
        type: { type: String, required: false },
        created_on: {
            type: Date,
            default: Date.now
        },
        updated_on: {
            type: Date
        },
        deleted_on: {
            type: Date
        }
    },
    {
        versionKey: false
    },

    {
        collection: "packages"
    }
);

module.exports = mongoose.model("packages", packages);
