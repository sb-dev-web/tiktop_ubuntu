const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModelConstants = require('./BaseModel').constants;

const SubscriptionTransactionDetail = new Schema (
    {
        user_id: {type: Schema.Types.ObjectId, required: true, ref: 'Users'},
        device_type: {type: String, required: true},
        parent_transaction_id: {type: String, required: true},
        subscription_token: {type: String, required: true},
        response_status: {type: String, required: false, default: ""},
        error_message: {type: String, required: false, default: ""},
        auto_renewing: {type: Number, required: false, default: 0},
        is_in_billing_retry: {type: Number, required: false, default: 0},
        latest_receipt: {type: String, required: false},

    },{
        collection: 'subscription_transaction_detail',
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        }
    }
);

Object.assign(SubscriptionTransactionDetail.statics, BaseModelConstants);

module.exports = mongoose.model('SubscriptionTransactionDetail', SubscriptionTransactionDetail);