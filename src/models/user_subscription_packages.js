const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BaseModelConstants = require('./BaseModel').constants;

const ModelConstants = Object.freeze({
    IS_FREE_TRIAL: 1,
    NOT_FREE_TRIAL: 0,
    IS_START_AS_PAID_TRANSACTION: 1,
    IS_NOT_START_AS_PAID_TRANSACTION: 0,
    IS_RENEWAL: 1,
    IS_NOT_RENEWAL: 0,
    IS_TRIAL_CONVERT_TO_PAID: 1,
    IS_NOT_TRIAL_CONVERT_TO_PAID: 0,
    IS_EVENT_SENT: 1,
    NOT_EVENT_SENT: 0,
    
    IS_FIREBASE_EVENT_SENT: 1,
    NOT_FIREBASE_EVENT_SENT: 0,
    
    IS_NEW_ACTIVATION: 1,
    NOT_NEW_ACTIVATION: 0,


    IS_BILLING_RETRY_CONVERSION: 1,
    NOT_BILLING_RETRY_CONVERSION: 0,
    IS_BILLING_RETRY_RENEWAL: 1,
    NOT_BILLING_RETRY_RENEWAL: 0,

    IS_REACTIVATION: 1,
    NOT_REACTIVATION: 0,

});

const UserSubscriptionPackages = new Schema (
    {
        user_id: {type: Schema.Types.ObjectId, required: true, ref: 'UserDevices'},
        //subscription_transaction_parent_id: {type: Schema.Types.ObjectId, required: true, ref: 'SubscriptionTransactionDetail'},
        subscription_package_id: {type: Schema.Types.ObjectId, required: true, ref: 'SubscriptionPackages'},
        transaction_id: {type: String, required: true},
        
        description: {type: String, required: false},
        price: {type: String, required: false, default: '0'},
        
        purchase_date: {type: Date, required: false},

        subscription_package_name: {type: String, required: false},
        calling_from: {type: String, required: false},

        
    },{
        collection: 'user_subscription_packages',
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at',
        }
    }
);

const constants = {...BaseModelConstants, ...ModelConstants};

Object.assign(UserSubscriptionPackages.statics, constants);

module.exports = mongoose.model('UserSubscriptionPackages', UserSubscriptionPackages);