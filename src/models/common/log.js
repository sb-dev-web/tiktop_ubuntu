const mongoes = require("mongoose");
//mongoes.set('debug',process.env.MONGODB_DEBUG_MODE );//

const logSchema = mongoes.Schema({
	headers: {
		type: Object,
	},
	params: {
		type: Object,
	},
	url: {
		type: Object,
	},
	start_time: {
		type: Number,
	},
	ip: {
		type: String,
	},
	end_time: {
		type: Number,
	},
	rtime: {
		type: Number,
	},
	status_code: {
		type: Number,
	},
	response: {
		type: Object,
	},
	created_on: {
		type: Date,
		default: Date.now,
	},
});

module.exports = mongoes.model("log", logSchema);
