//var SHA256 = require("crypto-js/sha256");
const CryptoJS = require("crypto-js");
var logModel = require("./../models/common/log");
var mongoose = require("mongoose");
const { Validator } = require("node-input-validator");

function _randomString(length = 16) {
    var result = "";
    var characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(
            Math.floor(Math.random() * charactersLength)
        );
    }

    //CryptoJS.lib.WordArray.random(128 / 8);
    return result;
}

function _hashPassword(salt, password) {
    let hashPassword = CryptoJS.SHA256(salt + password);
    return hashPassword;
}

function getTime() {
    return Math.round(new Date().getTime()); //milesecs
}
function getGmtTime(withoutMS = 0) {
    const date = new Date();
    const time = new Date(date.toUTCString());
    if (withoutMS) {
        return parseInt(time.getTime() / 1000);
    }
    return time.getTime();
}
function _generateToken(data) {
    return CryptoJS.SHA256(data);
}

async function getRequest(req, res, next) {
    var ip =
        req.headers["x-forwarded-for"] ||
        req.connection.remoteAddress ||
        req.socket.remoteAddress ||
        (req.connection.socket ? req.connection.socket.remoteAddress : null);
    var startTime = await getTime();
    // req.body.start_time= startTime;
    var endTime = "";
    var objLog = await new logModel({
        headers: req.headers,
        params: req.body ? req.body : req.query,
        url: req.originalUrl,
        start_time: startTime,
        end_time: endTime,
        ip: ip,
        rtime: "",
        status_code: "0",
        response: "",
    }).save();
    req.body.logId = objLog._id;
    req.body.start_time = startTime;
    req.body.ip = ip;

    //console.log("request start",req.body.start_time ," ",startTime);
    next();

    // return res.status(statusCode).json(data);
}

function sendResponse(req, res, statusCode, data) {
    if (typeof req.body.start_time === "undefined") {
        req.body.start_time = getTime();
    }
    var rtime = (getTime() - req.body.start_time) / 1000;
    if (isNaN(rtime)) {
        rtime = 0;
    }
    var updateObj = {
        response: data,
        end_time: getTime(),
        rtime: rtime,
        status_code: statusCode,
    };

    logModel
        .findOne({ _id: mongoose.Types.ObjectId(req.body.logId) })
        .updateOne(updateObj)
        .then((result) => {
            return res.status(statusCode).json(data);
            res.end();
        })
        .catch((e) => {
            console.log("eeror", e);
        });
}

function _generatesalt(count = 10) {
    return CryptoJS.lib.WordArray.random(128 / 8);
}

const getSignedUrl = async (path) => {
    try {
        const AWS = require("aws-sdk");
        const config = require("../config/config");
        const s3 = new AWS.S3(config.awsConfig);
        const url = s3.getSignedUrl("getObject", {
            Bucket: config.awsBucket,
            Key: path,
            Expires: config.awsSignedExpLimit,
        });
        /* promise.then(function(url) {
        console.log('The URL is', url);
        return url;
      }, function(err) { 
        return "https://pd-dev-global.s3-eu-west-1.amazonaws.com/app_images/driver-default.png";
       });*/

        if (url) {
            // return url;
            return { status: 1, url: url };
        } else {
            return { status: 0, url: config.driver_default_image_url };
        }
    } catch (e) {
        return {
            status: 0,
            message: e.message,
            url:
                "https://pd-dev-global.s3-eu-west-1.amazonaws.com/app_images/driver-default.png",
        };
    }
};

const s3ObjectCopy = async (s3ParamsObject, type) => {
    const config = require("../config/config");
    const s3 = new aws.S3(config.awsConfig);
    let docObj = {};
    try {
        var awsCopyObject = await s3.copyObject(s3ParamsObject).promise();
        var SignUrl = await getSignedUrl(s3ParamsObject.Key);

        if (awsCopyObject) {
            docObj.doc_type = type;
            docObj.doc_file = s3ParamsObject.Key;

            return {
                status: 1,
                message: "success",
                file_path: s3ParamsObject.Key,
                type: type,
                file_url: SignUrl,
            };
        } else {
            return {
                status: 0,
                message: message_obj.message_obj.s3_move_error,
            };
        }
    } catch (error) {
        return { status: 0, message: error.message };
    }
};

const isTokenValid = async (payload) => {
    // const mongoose = require('mongoose');
    var partnerModel = require("../models/partners/partner");
    var driverModel = require("../models/partners/driver");
    var loginModel = require("../models/common/login");

    let userId = mongoose.Types.ObjectId(payload.user_id);
    let userDeviceId = payload.user_device_id;
    var userDetail;
    let deviceDetail = await loginModel
        .aggregate([
            {
                $match: {
                    $and: [
                        {
                            _id: userId,
                        },
                    ],
                },
            },
            {
                $lookup: {
                    from: "devicedetail",
                    localField: "_id",
                    foreignField: "user_id",
                    as: "deviceDetail",
                },
            },
            { $unwind: "$deviceDetail" },
            {
                $match: {
                    $and: [
                        {
                            "deviceDetail.device_id": userDeviceId,
                        },
                    ],
                },
            },
        ])
        .exec();

    if (deviceDetail.length > 0) {
        //check expire date
        // console.log(deviceDetail);

        if (deviceDetail[0].type == "PARTNER") {
            userDetail = await partnerModel.findOne({
                _id: mongoose.Types.ObjectId(deviceDetail[0].user_id),
            });
        } else if (deviceDetail[0].type == "DRIVER") {
            userDetail = await driverModel.findOne({
                _id: mongoose.Types.ObjectId(deviceDetail[0].user_id),
            });
        }

        if (userDetail) {
            return {
                status: 1,
                message: "success",
                app_type: deviceDetail[0].deviceDetail.app_type,
                user_type: deviceDetail[0].type,
                device_type: deviceDetail[0].deviceDetail.device_type,
                device_type: deviceDetail[0].deviceDetail.name,
                user_id: userDetail._id, //User or partner id
                country_code: userDetail.phone_number.country_code,
                location_id: userDetail.location_id,
                phone: userDetail.phone_number,
                name: userDetail.name,
                email: userDetail.email,
                accountType: userDetail.type,
            };
        } else {
            return {
                status: 0,
                message: message_obj.auth_not_fount,
                error_code: api_error_code.server_error,
            };
        }
    } else {
        return {
            status: 0,
            message: message_obj.auth_not_fount,
            error_code: api_error_code.auth_fail,
        };
    }
};

const getUserDetail = async (req, res) => {
    const jwt = require("jsonwebtoken");
    const {
        message_obj,
        mailtext_obj,
        api_error_code,
    } = require("../lang/partner/message");
    token = req.headers.oauth;
    if (!token) {
        responseObj = {
            status: 401,
            message: message_obj.auth_token_missing,
            message_dev: "token not found",
            error_code: api_error_code.auth_fail,
        };
        return sendResponse(req, res, 401, responseObj);
    } else {
        //verify token is correct or not
        let jwtPayload = jwt.verify(token, process.env.secretKey, function (
            err,
            decoded
        ) {
            if (err) {
                responseObj = {
                    status: 401,
                    message: message_obj.auth_verification_fail,
                    message_dev: err.message,
                    error_code: api_error_code.auth_fail,
                };
                return sendResponse(req, res, 401, responseObj);
            }
            return decoded;
        });

        //check user id
        if (jwtPayload) {
            let tokenValidation = await isTokenValid(jwtPayload);
            if (tokenValidation.status == 0) {
                let responseObj = {
                    status: 401,
                    message: tokenValidation.message,
                    error_code: api_error_code.auth_fail,
                };
                return sendResponse(req, res, 200, responseObj);
            } else if (tokenValidation.status == 1) {
                //set userid in req.header

                req.headers.auth_app_type = tokenValidation.app_type; //
                req.headers.auth_user_type = tokenValidation.user_type;
                req.headers.auth_device_type = tokenValidation.device_type;
                req.headers.auth_user_id = tokenValidation.user_id;
                req.headers.auth_country = tokenValidation.country_code;
                req.headers.location_id = tokenValidation.location_id;
                return {
                    status: 1,
                    userDetail: tokenValidation,
                };
            }
        }
    }
};

const checkValidation = async (inputData, rules) => {
    var Validator = require("node-input-validator").Validator;
    var ValidatorExtend = require("node-input-validator").extend;
    ValidatorExtend("unique", async ({ value, args }) => {
        // default field is email in this example
        const field = args[1] || "email";

        let condition = {};

        condition[field] = value;

        // add ignore condition
        if (args[2]) {
            condition["_id"] = { $ne: mongoose.Types.ObjectId(args[2]) };
        }

        let emailExist = await mongoose
            .model(args[0])
            .findOne(condition)
            .select(field);

        // email already exists
        if (emailExist) {
            return false;
        }

        return true;
    });
    return new Validator(inputData, rules);

    return new Validator(
        {
            email: "required|email|unique:User,email",
        },
        inputData
    );
    //return new Validator(inputData, rule);
};

const ValidationMessage = async (validationErrors) => {
    var messages = [];
    for (let [key, value] of Object.entries(validationErrors)) {
        messages.push(value.message);
    }

    return messages.join(",");
};

const fetchData = async (url, user_agent, type) => {
    const fetch = require("node-fetch");
    return await fetch(url, {
        method: "get",
        // body:    JSON.stringify(body),
        headers: {
            "Content-Type": "application/json",
            "User-Agent": user_agent,
        },
        //'Content-Type': 'application/json'
    })
        .then((res) => res.json())
        .then((json) => {
            //console.log("Resukt==?>",json.userInfo);
            if (type == "CheckUser") {
                if (json.userInfo !== undefined) {
                    return json.userInfo;
                } else {
                    return false;
                }
            } else {
                return json;
            }
        });
};

const sendRequest = async (url, args) => {
    let fetch = require("node-fetch");

    var data, type;

    if (args.data) {
        data = args.data;
    } else {
        data = {};
    }

    type = args.type ? args.type : "get";

    var body = {
        method: type,
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
    };

    if (args.headers) {
        body.headers = { ...body.headers, ...args.headers };
    }

    if (args.otherData) {
        body = { ...body, ...args.otherData };
    }

    if (Object.keys(data).length == 0) {
        delete body.body;
    }

    if (Object.keys(body.headers).length == 0) {
        delete body.headers;
    }
   /* if (type == "GET") {
        var body = {
            method: type,
            body: null,
            headers: args.headers,
        };
    }*/
    console.log(url);
    console.log(body);
    const response = await fetch(url, body);
    

    const responseJSON = await response.json();
    //const responseJSON = await response.text();

    return responseJSON;
};

const imageExists = async (URL) => {
    let fetch = require("node-fetch");

    let response = await fetch(URL);

    return {
        status: response.status != 404 && response.status != 403,
        response,
    };
};

const moveOrderToOrderCompleted = async (promotionId) => {
    const mongoose = require("mongoose");
    const userSubscriptionModel = require("../models/user_subscription");
    const userOrderCompletedModel = require("../models/user_order_completed");

    promotionId = mongoose.Types.ObjectId(promotionId);
    let objUserSubscription = await userSubscriptionModel.findById(promotionId);

    if (
        objUserSubscription &&
        !objUserSubscription.is_moved_to_completed &&
        objUserSubscription.completed >= objUserSubscription.count
    ) {
        let updatedObjUserSubscription = { ...objUserSubscription.toObject() };
        updatedObjUserSubscription.reference_id =
            updatedObjUserSubscription._id;

        delete updatedObjUserSubscription._id;

        await new userOrderCompletedModel(updatedObjUserSubscription).save();

        objUserSubscription.is_completed = true;

        //At later stage we need to delete this data from user subscriptions table
        //For now marking as a moved.
        // objUserSubscription.is_moved_to_completed = true;
        await objUserSubscription.save();
    }
};

const addActionToOrderActions = async (objUser, objUserSubscription, type) => {
    const userOrderActionsModel = require("../models/user_order_actions");

    if (objUserSubscription && objUserSubscription._id) {
        if (objUser && objUser._id) {
            let objOrderAction = await userOrderActionsModel.findOne({
                user_id: objUser._id,
            });

            let isObjOrderAction = false;
            if (!objOrderAction) {
                objOrderAction = {
                    user_id: objUser._id,
                    tiktop_userid: objUser.tiktop_userid,

                    like_actions: [],
                    follow_actions: [],
                    unique_actions: [],

                    skip_like_actions: [],
                    skip_follow_actions: [],
                    skip_unique_actions: [],

                    like_promoted_ids: [],
                    follow_promoted_ids: [],
                    skip_like_promoted_ids: [],
                    skip_follow_promoted_ids: [],

                    like_count: 0,
                    follow_count: 0,
                    skip_like_count: 0,
                    skip_follow_count: 0,

                    created_on: new Date(Date.now()),
                    updated_on: new Date(Date.now()),
                };
            } else {
                isObjOrderAction = true;
            }

            let unique_identifier = objUserSubscription.tiktop_userid;
            if (objUserSubscription.type == "Like") {
                unique_identifier =
                    objUserSubscription.tiktop_userid +
                    "-" +
                    objUserSubscription.tiktop_postid;
            }

            if (type == "Like") {
                if (!objOrderAction.like_actions.includes(unique_identifier)) {
                    objOrderAction.like_actions.push(unique_identifier);
                    objOrderAction.unique_actions.push(unique_identifier);
                    objOrderAction.like_promoted_ids.push(
                        objUserSubscription._id
                    );
                }
            } else if (type == "Follower") {
                if (
                    !objOrderAction.follow_actions.includes(unique_identifier)
                ) {
                    objOrderAction.follow_actions.push(unique_identifier);
                    objOrderAction.unique_actions.push(unique_identifier);
                    objOrderAction.follow_promoted_ids.push(
                        objUserSubscription._id
                    );
                }
            } else if (type == "Skip") {
                if (objUserSubscription.type == "Like") {
                    if (
                        !objOrderAction.skip_like_actions.includes(
                            unique_identifier
                        )
                    ) {
                        objOrderAction.skip_like_actions.push(
                            unique_identifier
                        );
                        objOrderAction.skip_unique_actions.push(
                            unique_identifier
                        );
                        objOrderAction.skip_like_promoted_ids.push(
                            objUserSubscription._id
                        );
                    }
                } else {
                    if (
                        !objOrderAction.skip_follow_actions.includes(
                            unique_identifier
                        )
                    ) {
                        objOrderAction.skip_follow_actions.push(
                            unique_identifier
                        );
                        objOrderAction.skip_unique_actions.push(
                            unique_identifier
                        );
                        objOrderAction.skip_follow_promoted_ids.push(
                            objUserSubscription._id
                        );
                    }
                }
            }

            objOrderAction.like_count = objOrderAction.like_actions.length;
            objOrderAction.follow_count = objOrderAction.follow_actions.length;
            objOrderAction.skip_like_count =
                objOrderAction.skip_like_actions.length;
            objOrderAction.skip_follow_count =
                objOrderAction.skip_follow_actions.length;

            if (isObjOrderAction) {
                objOrderAction.updated_on = new Date(Date.now());
                await objOrderAction.save();
            } else {
                await new userOrderActionsModel(objOrderAction).save();
            }

            return {
                status: true,
                message: "success",
            };
        } else {
            return {
                status: false,
                message: "User not exists, ID: " + objUser._id,
            };
        }
    } else {
        return {
            status: false,
            message: "Subscription not exists, ID: " + objUserSubscription._id,
        };
    }
};

module.exports = {
    _randomString,
    _hashPassword,
    sendResponse,
    _generatesalt,
    _generateToken,
    getTime,
    getRequest,
    getSignedUrl,
    s3ObjectCopy,
    getUserDetail,
    isTokenValid,
    checkValidation,
    ValidationMessage,
    getGmtTime,
    fetchData,
    sendRequest,
    imageExists,
    addActionToOrderActions,
    moveOrderToOrderCompleted,
};
