var tpl = require('node-tpl');
const Log = require('../models/common/log');
const sha256 = require('sha256');
const mongoose = require('mongoose');
const md5 = require('md5');
const MomentJs = require('moment');
const fs = require('fs');
const fetch = require('node-fetch');

const ApiMessages = require("../lang/message");

tpl.setcwd(__dirname + "/../mail_templates");

var Validator = require('node-input-validator').Validator;
var ValidatorExtend = require('node-input-validator').extend;

class Utility {

    constructor() {
        this.STATUS_ERROR = false;
        this.STATUS_SUCCESS = true;

        this.SHOW_MESSAGE = true;
        this.NOT_SHOW_MESSAGE = false;
    }

    validateInputs(data, rules) {
        ValidatorExtend('unique', async ({ value, args }) => {
            // default field is email in this example
            const field = args[1] || 'email';

            let condition = {};

            condition[field] = value;

            // add ignore condition
            if (args[2]) {
                condition['_id'] = { $ne: mongoose.Types.ObjectId(args[2]) };
            }

            let emailExist = await mongoose.model(args[0]).findOne(condition).select(field);

            // email already exists
            if (emailExist) {
                return false;
            }

            return true;
        });

        return new Validator(data, rules);
    }

    getApiMessages(messageKey) {
        if (messageKey in ApiMessages) {
            return ApiMessages[messageKey];
        }
        return false;
    }

    formatDate(date, format = 'YYYY-MM-DD hh:mm:ss', offset = "+0:00") {
        return MomentJs(date).zone(offset).format(format);
    }

    formatUnixToGMTDate(date, format = 'YYYY-MM-DD hh:mm:ss') {
        return MomentJs.unix(date).utc().format(format);
    }

    formatValidationMessages(validationErrors) {
        var messages = [];
        for (let [key, value] of Object.entries(validationErrors)) {
            messages.push(value.message);
        }

        return messages.join(',');
    }

    setResponse() {
        const response = {
            'status': this.STATUS_ERROR,
            'developer_messages': 'all set',
            'show_message_to_user': this.SHOW_MESSAGE,
            'message': this.getApiMessages('something_went_wrong'),
        };
        return response;
    }

    getGMTDate() {
        return MomentJs.utc().format("YYYY-MM-DD");
    }

    getTime(fulltime = 0) {
        if (fulltime == 1) {
            return Math.round(new Date().getTime());
        }
        return Math.round(new Date().getTime() / 1000);
    }

    getMD5Hash(string) {
        const hash = md5(string);
        return hash;
    }

    getSHA256Hash(string) {
        const hash = sha256(string);
        return hash;
    }

    getPasswordEnc(password) {
        const hash = sha256(md5(password));
        return hash;
    }

    getHash(salt, password) {
        const hash = sha256(salt + password + "");
        return hash;
    }

    getGmtDateTime() {
        const date = new Date();
        return date.toUTCString();
    }

    generateHash() {
        const randomNumber = parseInt(Math.random() * 1000000000);
        const timestamp = this.getGmtTime();
        const salt = sha256(timestamp + randomNumber + "");

        return salt;
    }

    getGmtTime(withoutMS = 0) {
        const date = new Date();
        const time = new Date(date.toUTCString());
        if (withoutMS) {
            return parseInt(time.getTime() / 1000);
        }
        return time.getTime();
    }

    //set response for web
    sendResponseWeb(req, res, data, statusCode = 200) {
        var startTime = req.start_time;
        var endTime = this.getTime();
        var url = req.protocol + '://' + req.get('host') + req.originalUrl;

        var objLog = new Log({
            uri: url,
            headers: req.headers,
            params: req.body,
            start_time: startTime,
            end_time: endTime,
            rtime: endTime - startTime,
            status: req.status,
            response: data,
        });

        objLog.save();


        return res.status(statusCode).json(data);
    }

    sendResponse(req, res, data, statusCode = 200) {
        var startTime = req.start_time;
        var endTime = this.getTime();
        var url = req.protocol + '://' + req.get('host') + req.originalUrl;

        var objLog = new Log({
            uri: url,
            headers: req.headers,
            params: req.body,
            start_time: startTime,
            end_time: endTime,
            rtime: endTime - startTime,
            status: statusCode,
            response: data,
        });

        objLog.save();


        return res.status(statusCode).json(data);
    }

    loadMailTemplate(req, template, data) {
        const host = process.env.SITE_HTTP + req.headers.host;
        const logoImage = host + '/images/logo.png';

        Object.entries(data).forEach(([k, v]) => {
            tpl.assign(k, v);
        });

        const mailContent = tpl.fetch(template + ".tpl");

        const layoutData = {
            logo: logoImage,
            date: MomentJs().format('YYYY'),
            content: mailContent,
            company_name: process.env.SITE_NAME,
        }

        Object.entries(layoutData).forEach(([k, v]) => {
            tpl.assign(k, v);
        });

        const body = tpl.fetch("layout.tpl");
        return body;
    }

    logData(data, fileName = '') {
        if (!fileName) {
            fileName = this.getGMTDate();
            fileName += '.txt';
        }
        fs.appendFile(`logs/${fileName}`, `\n\n${new Date()}\n\n` + data, function (err) {
            if (err) throw err;
            console.log('Saved!');
        });
    }

    base64Decode(sting) {
        let buff = new Buffer(sting, 'base64');
        let text = buff.toString('ascii');

        return JSON.parse(text);
    }

    async sendRequest(url, args) {
        var data, type;

        if (args.data) {
            data = args.data;
        } else {
            data = {};
        }

        type = args.type ? args.type : 'get';

        var body = {
            method: type,
            body: data,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        if (args.headers) {
            body.headers = { ...body.headers, ...args.headers };
        }

        if (Object.keys(data).length == 0) {
            delete body.body;
        }

        if (Object.keys(body.headers).length == 0) {
            delete body.headers;
        }

        const response = await fetch(url, body);

        const responseJSON = await response.json();

        return responseJSON;
    }

    getCount(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    getstarttime() {
        return Math.round(new Date().getTime() / 1000);

    }

    getTime() {
        return Math.round(new Date().getTime() / 1000);
    }

    createURL(req, subURL) {
        subURL = subURL.replace(/^\//g, '');

        return req.protocol + '://' + req.get('host') + '/' + subURL;
    }

}

module.exports = new Utility();