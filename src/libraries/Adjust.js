// const firebase = require('firebase/app');
// require('@firebase/app');
// // require('@firebase/component');
// // require("firebase/analytics");

// require("@firebase/analytics");

// var firebaseConfig = {
//     apiKey: "AIzaSyAZw-sJvb6bAZtzcydDNPwRV_iCP_v_oDw",
//     authDomain: "net-869b1.firebaseapp.com",
//     databaseURL: "https://net-869b1.firebaseio.com",
//     projectId: "net-869b1",
//     storageBucket: "net-869b1.appspot.com",
//     messagingSenderId: "52790892615",
//     appId: "1:52790892615:web:9086af819b9bf98c373962",
//     measurementId: "G-D19L9S8LPL"
// };
// firebase.initializeApp(firebaseConfig);

// firebase.analytics().logEvent(
//     'test_content',
//     {
//         content_type: 'image',
//         content_id: 'P12453',
//         items: [{ name: 'Kittens' }]
//     }
// );


const Utility = require('../Helpers/Utility');

require('dotenv').config();

class Adjust {

    data = {};
    eventType = "";

    sendRequest = async () => {
        var data = {
            type: 'post',
            data: this.data,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        }

        const searchParams = Object.keys(data.data).map((key) => {
            return encodeURIComponent(key) + '=' + encodeURIComponent(data.data[key]);
        }).join('&');
        data.data = searchParams;

        return await Utility.sendRequest('https://s2s.adjust.com/event', data)
    }

    setData = (data) => {
        this.data = {
            "app_token": process.env.ADJUST_TOKEN,
            "s2s": 1,
            "environment": process.env.ADJUST_ENVIRONMENT,
        }

        this.data = { ...this.data, ...data };

        return this;
    }

    sendRequestFirebase = async () => {
        var firebaseConfig = {
            apiKey: "AIzaSyAZw-sJvb6bAZtzcydDNPwRV_iCP_v_oDw",
            authDomain: "net-869b1.firebaseapp.com",
            databaseURL: "https://net-869b1.firebaseio.com",
            projectId: "net-869b1",
            storageBucket: "net-869b1.appspot.com",
            messagingSenderId: "52790892615",
            appId: "1:52790892615:web:9086af819b9bf98c373962",
            measurementId: "G-D19L9S8LPL"
        };
        // firebase.initializeApp(firebaseConfig);

        // firebase.analytics().logEvent(
        //     'select_content',
        //     {
        //         content_type: 'image',
        //         content_id: 'P12453',
        //         items: [{ name: 'Kittens' }]
        //     }
        // );
    }

    setDataFirebase = (data) => {
        this.data = {
            // "event_token": eventToken,
            "environment": process.env.ADJUST_ENVIRONMENT,
            "created_at": Utility.getGmtDateTime(),
        }

        this.data = { ...this.data, ...data };

        return this;
    }

}

module.exports = new Adjust();