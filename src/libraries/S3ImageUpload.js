module.exports.uploadImageOnS3 = async (url, fileName) => {
    let aws = require("aws-sdk");
    let FileType = require("file-type");

    const config = require("../config/config");
    const utility = require("../util/utility");

	const s3 = new aws.S3({
		accessKeyId: config.appConfig.AWSAccessKey,
		secretAccessKey: config.appConfig.AWSSecretKey,
	});

    let response = await utility.imageExists(url);
    if(response) {
        let buf = await response.response.arrayBuffer();
        const buffer = Buffer.from(buf, "base64");
        let type = (await FileType.fromBuffer(buffer)).mime;
    
        let s3Response = await s3
            .upload({
                Bucket: "tiktop-media",
                Key: fileName,
                Body: buffer,
                ContentType: type,
            })
            .promise();
        return s3Response;
    }
    return false;
};

module.exports.findFileTypeFromURL = (URL) => {
	let type = "";
	if (URL.match(/jpeg/i)) {
		type = "jpeg";
	} else if (URL.match(/jpg/i)) {
		type = "jpg";
	} else if (URL.match(/png/i)) {
		type = "png";
	}

	return type;
};
