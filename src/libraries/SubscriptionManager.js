var _ = require("loadsh");
var mongoose = require("mongoose");
var iap1 = require("in-app-purchase");
//var Verifier = require('google-play-billing-validator');

const MOMENT = require("moment");

require("dotenv").config();

const Utility = require("../Helpers/Utility");
const UserModel = require("../models/users"); 
const UserDevicesModel = require("../models/devicedetail");
const SubscriptionPackageModel = require("../models/subscription_packages");
const UserSubscriptionPackageModel = require("../models/user_subscription_packages");
const HashtagUserSubscriptionPackageModel = require("../models/hashtaguser_subscription_packages");
const SubscriptionTransactionDetailModel = require("../models/subscription_transaction_detail");
const HashtagSubscriptionTransactionDetailModel = require("../models/hashtagsubscription_transaction_detail");
const SubscriptionTransactionDetailFailedModel = require("../models/subscription_transaction_detail_failed");
const HashtagSubscriptionTransactionDetailFailedModel = require("../models/hashtagsubscription_transaction_detail_failed");
const hashtagdeviceModel = require("../models/hashtagdevicedetail");

//const PackagesPriceCurrencyModel = require('../models/packages_price_currency');

//const UserSubscriptionPackageAndroidModel = require('../models/user_subscription_packages_android');
//const SubscriptionTransactionDetailAndroidModel = require('../models/subscription_transaction_detail_android');

const updateUserSubscription = async (userId, data) => {
  if (parseInt(userId) !== 0) {
    userId = mongoose.Types.ObjectId(userId);
    const where = {
      user_id: userId
    };
    return await UserDevicesModel.updateMany(where, data);
  }
  return false;
};

const updateUserDeviceSubscription = async (
  objUserDevice,
  userDeviceUpdateData
) => {
  var objUserDevice = await UserDevicesModel.findByIdAndUpdate(
    objUserDevice._id,
    userDeviceUpdateData
  );

  return objUserDevice;
};

const addParentSubscription = async (
  userId,
  deviceType,
  parentTransactionId,
  subscriptionToken,
  autoRenewing = 0,
  receiptResponse = {}
) => {
  var addParentSubscriptionData = {};
  addParentSubscriptionData["user_id"] = userId;
  addParentSubscriptionData["device_type"] = deviceType;
  addParentSubscriptionData["parent_transaction_id"] = parentTransactionId;
  addParentSubscriptionData["subscription_token"] = subscriptionToken;
  addParentSubscriptionData["auto_renewing"] = autoRenewing ? 1 : 0;
  addParentSubscriptionData["response_status"] = JSON.stringify(
    receiptResponse
  );

  const objSubscriptionTransactionDetail = await new SubscriptionTransactionDetailModel(
    addParentSubscriptionData
  ).save();
  return objSubscriptionTransactionDetail._id;
};
const addParentSubscriptionHashtag = async (
  deviceId,
  deviceType,
  parentTransactionId,
  subscriptionToken,
  autoRenewing = 0,
  receiptResponse = {}
) => {
  var addParentSubscriptionData = {};
  addParentSubscriptionData["device_id"] = deviceId;
  addParentSubscriptionData["device_type"] = deviceType;
  addParentSubscriptionData["parent_transaction_id"] = parentTransactionId;
  addParentSubscriptionData["subscription_token"] = subscriptionToken;
  addParentSubscriptionData["auto_renewing"] = autoRenewing ? 1 : 0;
  addParentSubscriptionData["response_status"] = JSON.stringify(
    receiptResponse
  );

  const objSubscriptionTransactionDetail = await new HashtagUserSubscriptionPackageModel(
    addParentSubscriptionData
  ).save();
  return objSubscriptionTransactionDetail._id;
};

const addParentSubscriptionAndroid = async (
  userId,
  deviceType,
  parentTransactionId,
  subscriptionToken,
  autoRenewing = 0,
  receiptResponse = {}
) => {
  var addParentSubscriptionData = {};
  addParentSubscriptionData["user_id"] = userId;
  addParentSubscriptionData["device_type"] = deviceType;
  addParentSubscriptionData["parent_transaction_id"] = parentTransactionId;
  addParentSubscriptionData["subscription_token"] = subscriptionToken;
  addParentSubscriptionData["auto_renewing"] = autoRenewing ? 1 : 0;
  addParentSubscriptionData["response_status"] = JSON.stringify(
    receiptResponse
  );

  const objSubscriptionTransactionDetail = await new SubscriptionTransactionDetailAndroidModel(
    addParentSubscriptionData
  ).save();
  return objSubscriptionTransactionDetail._id;
};

const getParentSubscriptionIos = async (
  transactionId,
  parentTransactionId = ""
) => {
  var objSubscriptionTransactionDetail = await SubscriptionTransactionDetailModel.aggregate(
    [
      {
        $project: {
          A: "$$ROOT"
        }
      },
      {
        $lookup: {
          localField: "_id",
          from: UserSubscriptionPackageModel.collection.name,
          foreignField: "subscription_transaction_parent_id",
          as: "B"
        }
      },
      {
        $match: {
          "A.device_type": SubscriptionPackageModel.IOS_DEVICE_TYPE + "",
          "A.parent_transaction_id": parentTransactionId
        }
      },
      {
        $unwind: { path: "$B", preserveNullAndEmptyArrays: true }
      },
      {
        $group: {
          _id: {
            user_id: "$A.user_id",
            _id: "$A._id",
            subscription_token: "$A.subscription_token",
            parent_transaction_id: "$A.parent_transaction_id",
            is_in_billing_retry: "$A.is_in_billing_retry",
            auto_renewing: "$A.auto_renewing"
          },
          UserSubscriptionPackages: { $push: "$B" }
        }
      },
      {
        $project: {
          user_Id: "$_id.user_id",
          parentSubscriptionId: "$_id._id",
          subscriptionToken: "$_id.subscription_token",
          parentTransactionId: "$_id.parent_transaction_id",
          // Here we need to get web order line item first and than transaction
          UserSubscriptionPackages: {
            $filter: {
              input: "$UserSubscriptionPackages",
              as: "item",
              cond: { $eq: ["$$item.transaction_id", transactionId] }
            }
          }
        }
      }
    ]
  );

  var result = [];
  if (objSubscriptionTransactionDetail.length) {
    for (var i in objSubscriptionTransactionDetail) {
      const row = objSubscriptionTransactionDetail[i];

      let UserSubscriptionPackage = row.UserSubscriptionPackagesWithWebOrderItem
        ? row.UserSubscriptionPackagesWithWebOrderItem[0]
        : row.UserSubscriptionPackages.length
        ? row.UserSubscriptionPackages[0]
        : {};

      const data = {
        userId: row.user_id ? row.user_id : "",
        userIdInSubscriptionTable: UserSubscriptionPackage.user_id
          ? UserSubscriptionPackage.user_id
          : "",
        parentSubscriptionId: row._id._id,
        subscriptionToken: row._id.subscription_token,
        parentTransactionId: row._id.parent_transaction_id,
        is_in_billing_retry: row._id.is_in_billing_retry,
        auto_renewing: row._id.auto_renewing,
        renewedSubscriptionId: UserSubscriptionPackage._id
          ? UserSubscriptionPackage._id
          : "",
        renewedSubscriptionTransactionId: UserSubscriptionPackage.transaction_id
          ? UserSubscriptionPackage.transaction_id
          : "",
        renewedSubscriptionWebOrderLineItemId: UserSubscriptionPackage.web_order_line_item_id
          ? UserSubscriptionPackage.web_order_line_item_id
          : "",
        calling_from: UserSubscriptionPackage.calling_from
          ? UserSubscriptionPackage.calling_from
          : ""
      };
      result.push(data);
    }
  }

  return result;
};

const getParentSubscriptionAndroid = async (
  endDate,
  parentTransactionId = ""
) => {
  var where = {
    device_type: SubscriptionTransactionDetailModel.ANDROID_DEVICE_TYPE + ""
  };

  where["UserSubscriptionPackages.end_date"] = endDate;

  if (parentTransactionId) {
    where.parent_transaction_id = parentTransactionId;
  }

  var objSubscriptionTransactionDetail = await SubscriptionTransactionDetailModel.aggregate(
    [
      {
        $lookup: {
          localField: "_id",
          from: UserSubscriptionPackageModel.collection.name,
          foreignField: "subscription_transaction_parent_id",
          as: "UserSubscriptionPackages"
        }
      },
      {
        $unwind: {
          path: "$UserSubscriptionPackages",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $match: where
      }
    ]
  );

  var result = {};
  if (objSubscriptionTransactionDetail.length) {
    for (var i in objSubscriptionTransactionDetail) {
      const row = objSubscriptionTransactionDetail[i];
      result = {
        userId: row.user_id,
        parentSubscriptionId: row._id,
        subscriptionToken: row.subscription_token,
        parentTransactionId: row.parent_transaction_id,
        renewedSubscriptionId:
          row.UserSubscriptionPackages && row.UserSubscriptionPackages._id
            ? row.UserSubscriptionPackages._id
            : ""
      };
    }
  }

  return result;
};

const getParentSubscriptionAndroidNew = async (
  transactionId,
  parentTransactionId
) => {
  var objSubscriptionTransactionDetail = await SubscriptionTransactionDetailAndroidModel.aggregate(
    [
      {
        $project: {
          A: "$$ROOT"
        }
      },
      {
        $lookup: {
          localField: "_id",
          from: UserSubscriptionPackageAndroidModel.collection.name,
          foreignField: "subscription_transaction_parent_id",
          as: "B"
        }
      },
      {
        $match: {
          "A.device_type": SubscriptionPackageModel.ANDROID_DEVICE_TYPE + "",
          "A.parent_transaction_id": parentTransactionId
        }
      },
      {
        $unwind: { path: "$B", preserveNullAndEmptyArrays: true }
      },
      {
        $group: {
          _id: {
            user_id: "$A.user_id",
            _id: "$A._id",
            subscription_token: "$A.subscription_token",
            parent_transaction_id: "$A.parent_transaction_id",
            is_in_billing_retry: "$A.is_in_billing_retry",
            auto_renewing: "$A.auto_renewing"
          },
          UserSubscriptionPackages: { $push: "$B" }
        }
      },
      {
        $project: {
          user_Id: "$_id.user_id",
          parentSubscriptionId: "$_id._id",
          subscriptionToken: "$_id.subscription_token",
          // "parentTransactionId": "$_id.parent_transaction_id",
          // Here we need to get web order line item first and than transaction
          UserSubscriptionPackages: {
            $filter: {
              input: "$UserSubscriptionPackages",
              as: "item",
              cond: { $eq: ["$$item.transaction_id", transactionId] }
            }
          }
        }
      }
    ]
  );

  var result = [];
  if (objSubscriptionTransactionDetail.length) {
    for (var i in objSubscriptionTransactionDetail) {
      const row = objSubscriptionTransactionDetail[i];

      let UserSubscriptionPackage = row.UserSubscriptionPackages.length
        ? row.UserSubscriptionPackages[0]
        : {};

      const data = {
        userId: row.user_id ? row.user_id : "",
        userIdInSubscriptionTable: UserSubscriptionPackage.user_id
          ? UserSubscriptionPackage.user_id
          : "",
        parentSubscriptionId: row._id._id,
        subscriptionToken: row._id.subscription_token,
        parentTransactionId: row._id.parent_transaction_id,
        is_in_billing_retry: row._id.is_in_billing_retry,
        auto_renewing: row._id.auto_renewing,
        renewedSubscriptionId: UserSubscriptionPackage._id
          ? UserSubscriptionPackage._id
          : "",
        renewedSubscriptionTransactionId: UserSubscriptionPackage.transaction_id
          ? UserSubscriptionPackage.transaction_id
          : "",
        calling_from: UserSubscriptionPackage.calling_from
          ? UserSubscriptionPackage.calling_from
          : ""
      };
      result.push(data);
    }
  }

  return result;
};

const addSubscriptionRenewalTransaction = async (
  userId,
  parentSubscriptionId,
  transactionId,
  endDate,
  packageId,
  description = "",
  isFreeTrial = 0,
  ...args
) => {
  [currency, price] = args;
  var addSubscription = {};
  addSubscription["subscription_transaction_parent_id"] = parentSubscriptionId;
  addSubscription["user_id"] = userId;
  if (transactionId) {
    addSubscription["transaction_id"] = transactionId;
  }
  addSubscription["end_date"] = endDate;
  addSubscription["description"] = description;
  addSubscription["subscription_package_id"] = packageId;
  addSubscription["is_free_trial"] = isFreeTrial;
  addSubscription["currency"] = currency;
  addSubscription["price"] = price;

  const objUserSubscriptionPackage = new UserSubscriptionPackageModel(
    addSubscription
  ).save();

  return objUserSubscriptionPackage._id;
};

const addSubscriptionRenewalTransactionNew = async args => {
  var addSubscription = {};
  let userId = args.user_id;
  //let parentSubscriptionId = args.parent_subscription_id;
  let transactionId = args.transaction_id;
  //let endDate = args.end_date;
  let packageId = args.package_id;
  let description = args.description ? args.description : "";
  //let isFreeTrial = args.is_free_trial ? args.is_free_trial : 0;
  //let currency = args.currency ? args.currency : "";
  let price = args.price ? args.price : 0;
 // let idfa_id = args.idfa_id ? args.idfa_id : "";
 // let idfv = args.idfv ? args.idfv : "";
 // let adid = args.adid ? args.adid : "";
 // let gpsAdid = args.gps_adid ? args.gps_adid : "";
  let purchaseDate = args.purchase_date ? args.purchase_date : "";

  //let isRenewal = args.is_renewal ? args.is_renewal : 0;
  //let isTrialConvertToPaid = args.is_trial_convert_to_paid
   // ? args.is_trial_convert_to_paid
    //: 0;
//   let isStartAsPaidTransaction = args.is_start_as_paid_transaction
//     ? args.is_start_as_paid_transaction
//     : 0;
//   let adjustEventName = args.adjust_event_name ? args.adjust_event_name : "";
  //let adjustEventId = args.adjust_event_id ? args.adjust_event_id : "";

  let callingFrom = args.calling_from;
  let subscriptionPackageName = args.subscription_package_name;

  //let isNewActivation = args.is_new_activation;

  //addSubscription["subscription_transaction_parent_id"] = parentSubscriptionId;
  addSubscription["user_id"] = userId;
  if (transactionId) {
    addSubscription["transaction_id"] = transactionId;
  }
  //addSubscription["end_date"] = endDate || new Date();
  addSubscription["description"] = description;
  addSubscription["subscription_package_id"] = packageId;
//  addSubscription["is_free_trial"] = isFreeTrial;
//   addSubscription["currency"] = currency;
//   addSubscription["price"] = price;
//   addSubscription["gps_adid"] = gpsAdid;
//   addSubscription["idfa_id"] = idfa_id;
//   addSubscription["adid"] = adid;
//   addSubscription["idfv"] = idfv;
  addSubscription["purchase_date"] = purchaseDate;

//   addSubscription["is_renewal"] = isRenewal;
//   addSubscription["adjust_event_id"] = adjustEventId;
//   addSubscription["adjust_event_name"] = adjustEventName;
//   addSubscription["is_trial_convert_to_paid"] = isTrialConvertToPaid;
//   addSubscription["is_start_as_paid_transaction"] = isStartAsPaidTransaction;

  addSubscription["calling_from"] = callingFrom;
  addSubscription["subscription_package_name"] = subscriptionPackageName;

//   addSubscription["is_new_activation"] = isNewActivation;

//   addSubscription["web_order_line_item_id"] = args.web_order_line_item_id
//     ? args.web_order_line_item_id
//     : "";

//   addSubscription["parent_transaction_id"] = args.parent_transaction_id
//     ? args.parent_transaction_id
//     : "";

//   addSubscription["is_billing_retry_renewal"] = args.is_billing_retry_renewal;

//   addSubscription["is_billing_retry_conversion"] =
//     args.is_billing_retry_conversion;

//   addSubscription["is_reactivation"] = args.is_reactivation;

  const objUserSubscriptionPackage = new UserSubscriptionPackageModel(
    addSubscription
  ).save();

  return objUserSubscriptionPackage;
};

const addSubscriptionRenewalTransactionNewHashtag = async args => {
  var addSubscription = {};
  let device_id = args.device_id;

  let transactionId = args.transaction_id;

  let packageId = args.package_id;
  let description = args.description ? args.description : "";
  let isFreeTrial = args.is_free_trial ? args.is_free_trial : 0;

  let price = args.price ? args.price : 0;

  let purchaseDate = args.purchase_date ? args.purchase_date : "";

  let callingFrom = args.calling_from;
  let subscriptionPackageName = args.subscription_package_name;

  addSubscription["device_id"] = device_id;
  if (transactionId) {
    addSubscription["transaction_id"] = transactionId;
  }

  addSubscription["description"] = description;
  addSubscription["subscription_package_id"] = packageId;

  addSubscription["price"] = price;

  addSubscription["purchase_date"] = purchaseDate;

  addSubscription["calling_from"] = callingFrom;
  addSubscription["subscription_package_name"] = subscriptionPackageName;

  const objUserSubscriptionPackage = new HashtagUserSubscriptionPackageModel(
    addSubscription
  ).save();



  return objUserSubscriptionPackage;
};

const addSubscriptionRenewalTransactionNewAndroid = async args => {
  var addSubscription = {};
  let userId = args.user_id;
  let parentSubscriptionId = args.parent_subscription_id;
  let transactionId = args.transaction_id;
  let endDate = args.end_date;
  let packageId = args.package_id;
  let description = args.description ? args.description : "";
  let isFreeTrial = args.is_free_trial ? args.is_free_trial : 0;
  let currency = args.currency ? args.currency : "";
  let price = args.price ? args.price : 0;
  let idfa_id = args.idfa_id ? args.idfa_id : "";
  let idfv = args.idfv ? args.idfv : "";
  let adid = args.adid ? args.adid : "";
  let gpsAdid = args.gps_adid ? args.gps_adid : "";
  let purchaseDate = args.purchase_date ? args.purchase_date : "";

  let isRenewal = args.is_renewal ? args.is_renewal : 0;
  let isTrialConvertToPaid = args.is_trial_convert_to_paid
    ? args.is_trial_convert_to_paid
    : 0;
  let isStartAsPaidTransaction = args.is_start_as_paid_transaction
    ? args.is_start_as_paid_transaction
    : 0;
  let adjustEventName = args.adjust_event_name ? args.adjust_event_name : "";
  let adjustEventId = args.adjust_event_id ? args.adjust_event_id : "";

  let callingFrom = args.calling_from;
  let subscriptionPackageName = args.subscription_package_name;

  let isNewActivation = args.is_new_activation;

  addSubscription["subscription_transaction_parent_id"] = parentSubscriptionId;
  addSubscription["user_id"] = userId;
  if (transactionId) {
    addSubscription["transaction_id"] = transactionId;
  }
  addSubscription["end_date"] = endDate;
  addSubscription["description"] = description;
  addSubscription["subscription_package_id"] = packageId;
  addSubscription["is_free_trial"] = isFreeTrial;
  addSubscription["currency"] = currency;
  addSubscription["price"] = price;
  addSubscription["gps_adid"] = gpsAdid;
  addSubscription["idfa_id"] = idfa_id;
  addSubscription["adid"] = adid;
  addSubscription["idfv"] = idfv;
  addSubscription["purchase_date"] = purchaseDate;

  addSubscription["is_renewal"] = isRenewal;
  addSubscription["adjust_event_id"] = adjustEventId;
  addSubscription["adjust_event_name"] = adjustEventName;
  addSubscription["is_trial_convert_to_paid"] = isTrialConvertToPaid;
  addSubscription["is_start_as_paid_transaction"] = isStartAsPaidTransaction;

  addSubscription["calling_from"] = callingFrom;
  addSubscription["subscription_package_name"] = subscriptionPackageName;

  addSubscription["is_new_activation"] = isNewActivation;

  addSubscription["web_order_line_item_id"] = args.web_order_line_item_id
    ? args.web_order_line_item_id
    : "";

  addSubscription["parent_transaction_id"] = args.parent_transaction_id
    ? args.parent_transaction_id
    : "";

  addSubscription["is_billing_retry_renewal"] = args.is_billing_retry_renewal;

  addSubscription["is_billing_retry_conversion"] =
    args.is_billing_retry_conversion;

  addSubscription["is_reactivation"] = args.is_reactivation;

  const objUserSubscriptionPackage = new UserSubscriptionPackageAndroidModel(
    addSubscription
  ).save();

  return objUserSubscriptionPackage._id;
};

const updateSubscriptionTransaction = async (
  subscriptionId,
  endDate,
  description = ""
) => {
  var addSubscription = {};
  addSubscription["end_date"] = endDate;
  addSubscription["description"] = description;

  await UserSubscriptionPackageModel.updateOne(
    { _id: subscriptionId },
    addSubscription
  );
  return addSubscription;
};

const updateSubscriptionTransactionNew = async args => {
  let addSubscription = {};

  addSubscription["price"] = args.price;
  addSubscription["currency"] = args.currency;
  addSubscription["idfa_id"] = args.idfa_id;
  addSubscription["adid"] = args.adid;
  addSubscription["calling_from"] = args.calling_from;
  addSubscription["subscription_package_name"] = args.subscription_package_name;

  addSubscription["web_order_line_item_id"] = args.web_order_line_item_id
    ? args.web_order_line_item_id
    : "";

  if (args.idfv) {
    addSubscription["idfv"] = args.idfv;
  }

  addSubscription["user_id"] = args.user_id;

  addSubscription["end_date"] = args.end_date;
  addSubscription["description"] = args.description;

  addSubscription["parent_transaction_id"] = args.parent_transaction_id
    ? args.parent_transaction_id
    : "";
  addSubscription["transaction_id"] = args.transaction_id
    ? args.transaction_id
    : "";

  // addSubscription['gps_adid'] = args.gps_adid ? args.gps_adid : '';

  await UserSubscriptionPackageModel.updateOne(
    { _id: args.renewedSubscriptionId },
    addSubscription
  );
  return addSubscription;
};

const updateSubscriptionTransactionNewAndroid = async args => {
  let addSubscription = {};

  addSubscription["price"] = args.price;
  addSubscription["currency"] = args.currency;
  addSubscription["idfa_id"] = args.idfa_id;
  addSubscription["adid"] = args.adid;
  addSubscription["calling_from"] = args.calling_from;
  addSubscription["subscription_package_name"] = args.subscription_package_name;

  addSubscription["web_order_line_item_id"] = args.web_order_line_item_id
    ? args.web_order_line_item_id
    : "";

  if (args.idfv) {
    addSubscription["idfv"] = args.idfv;
  }

  addSubscription["user_id"] = args.user_id;

  addSubscription["end_date"] = args.end_date;
  addSubscription["description"] = args.description;

  addSubscription["parent_transaction_id"] = args.parent_transaction_id
    ? args.parent_transaction_id
    : "";
  addSubscription["transaction_id"] = args.transaction_id
    ? args.transaction_id
    : "";

  addSubscription["gps_adid"] = args.gps_adid ? args.gps_adid : "";

  await UserSubscriptionPackageAndroidModel.updateOne(
    { _id: args.renewedSubscriptionId },
    addSubscription
  );
  return addSubscription;
};

const updateParentSubscriptionData = async (
  subscriptionTransactionDetailId,
  userId
) => {
  await SubscriptionTransactionDetailModel.updateOne(
    { _id: subscriptionTransactionDetailId },
    {
      user_id: userId
    }
  );
};

const updateParentSubscriptionDataAndroid = async (
  subscriptionTransactionDetailId,
  userId
) => {
  await SubscriptionTransactionDetailAndroidModel.updateOne(
    { _id: subscriptionTransactionDetailId },
    {
      user_id: userId
    }
  );
};

const updateSubscriptionTransactionsPendingRenewalInfoAndLatestReceipt = async (
  pendingRenewalInfo,
  latestReceipt
) => {
  if (pendingRenewalInfo && pendingRenewalInfo.length) {
    for (let i in pendingRenewalInfo) {
      let row = pendingRenewalInfo[i];
      let updateData = {
        auto_renewing: row.auto_renew_status === "1" ? 1 : 0,
        is_in_billing_retry: row.is_in_billing_retry_period === "1" ? 1 : 0,
        latest_receipt: latestReceipt
      };
      await SubscriptionTransactionDetailModel.updateOne(
        { parent_transaction_id: row["original_transaction_id"] },
        updateData
      );
    }
  }
};

const addParentSubscriptionFailedHashtag = async (
  userId,
  deviceType,
  subscriptionToken,
  message = "",
  responseStatus = ""
) => {
  var addParentSubscriptionData = {};
  addParentSubscriptionData["device_id"] = userId;
  addParentSubscriptionData["device_type"] = deviceType;
  addParentSubscriptionData["subscription_token"] = subscriptionToken;
  addParentSubscriptionData["error_message"] = message;
  addParentSubscriptionData["response_status"] = JSON.stringify(responseStatus);

  const objSubscriptionTransactionDetailFailed = await new HashtagSubscriptionTransactionDetailFailedModel(
    addParentSubscriptionData
  ).save();
  if (
    objSubscriptionTransactionDetailFailed &&
    objSubscriptionTransactionDetailFailed._id
  ) {
    return objSubscriptionTransactionDetailFailed._id;
  }
  return false;
};

const addParentSubscriptionFailed = async (
  userId,
  deviceType,
  subscriptionToken,
  message = "",
  responseStatus = ""
) => {
  var addParentSubscriptionData = {};
  addParentSubscriptionData["user_id"] = userId;
  addParentSubscriptionData["device_type"] = deviceType;
  addParentSubscriptionData["subscription_token"] = subscriptionToken;
  addParentSubscriptionData["error_message"] = message;
  addParentSubscriptionData["response_status"] = JSON.stringify(responseStatus);

  const objSubscriptionTransactionDetailFailed = await new SubscriptionTransactionDetailFailedModel(
    addParentSubscriptionData
  ).save();
  if (
    objSubscriptionTransactionDetailFailed &&
    objSubscriptionTransactionDetailFailed._id
  ) {
    return objSubscriptionTransactionDetailFailed._id;
  }
  return false;
};

// const validatePlayStoreReceipt = async (req) => {
//     try {
//         var options = {
//             email: process.env.ANDROID_CLIENT_EMAIL_ID,
//             key: process.env.ANDROID_PRIVATE_KEY,
//         };

//         var verifier = new Verifier(options);

//         let receipt = {
//             packageName: process.env.ANDROID_PACKAGE_NAME,
//             productId: req.body.subscription_id,
//             purchaseToken: req.body.purchase_token
//         };

//         let promiseData = await verifier.verifySub(receipt)

//         return {
//             status: "success",
//             message: "Google server validated your receipt",
//             receipt_response: promiseData
//         };

//     } catch (err) {
//         return {
//             status: "error",
//             message: "Google server is unable to validate your receipt",
//             receipt_response: err.message
//         };
//     }
// }

// const getAndroidPackageId = async (subscriptionId) => {
//     return await SubscriptionPackageModel.findOne({ product_id: subscriptionId });
// }

// const getPackageIdForAndroid = async (subscriptionToken) => {
//     var returnResponse = {};

//     var where = {
//         subscription_token: subscriptionToken
//     }

//     where["UserSubscriptionPackages.deleted_at"] = null;

//     var objSubscriptionTransactionDetails = await SubscriptionTransactionDetailModel.aggregate([
//         {
//             $lookup: {
//                 "localField": "_id",
//                 "from": UserSubscriptionPackageModel.collection.name,
//                 "foreignField": "subscription_transaction_parent_id",
//                 "as": "UserSubscriptionPackages"
//             }
//         },
//         {
//             $unwind: { path: "$UserSubscriptionPackages", preserveNullAndEmptyArrays: true }
//         },
//         {
//             $match: where
//         },
//     ]);

//     var subscriptionPackageId = '';
//     if (objSubscriptionTransactionDetails && objSubscriptionTransactionDetails.length == 1) {
//         if (objSubscriptionTransactionDetails[0].subscription_package_id) {
//             subscriptionPackageId = objSubscriptionTransactionDetails[0].subscription_package_id;
//         }
//     }

//     const objSubscriptionPackageModel = SubscriptionPackageModel.findById(subscriptionPackageId);

//     if (objSubscriptionPackageModel && objSubscriptionPackageModel.package_id) {
//         returnResponse = {
//             _id: row._id,
//             package_id: row.package_id,
//         };
//     }

//     return returnResponse;
// }

const validateIOSReceipt = async req => {
  try {
    iap1.config({
      applePassword: process.env.APPLE_PASSWORD,
      test: req.body.is_debug == 1 ? true : false,
      ignoreExpired: false
    });

    await iap1.setup();
    const receiptResponse = await iap1.validate(req.body.app_store_receipt);

    return receiptResponse;
  } catch (err) {
    err = JSON.parse(err);

    return {
      status: false,
      message: err.message
    };
  }
};

const userExpiredSubscription = async () => {
  var objSubscriptionTransactionDetail = await SubscriptionTransactionDetailModel.aggregate(
    [
      {
        $lookup: {
          localField: "_id",
          from: UserSubscriptionPackageModel.collection.name,
          foreignField: "subscription_transaction_parent_id",
          as: "UserSubscriptionPackages"
        }
      },
      {
        $unwind: {
          path: "$UserSubscriptionPackages",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $project: {
          user_id: 1,
          device_type: 1,
          parent_transaction_id: 1,
          subscription_token: 1,
          response_status: 1,
          error_message: 1,
          UserSubscriptionPackages: "$UserSubscriptionPackages",
          transaction_id: "$UserSubscriptionPackages.transaction_id",
          diff: {
            $divide: [
              {
                $subtract: [
                  { $max: "$UserSubscriptionPackages.end_date" },
                  new Date()
                ]
              },
              1000 * 60 * 60 * 24
            ]
          },
          subscriptionEndDate: {
            $max: "$UserSubscriptionPackages.end_date"
          }
        }
      }
    ]
  );

  var result = [];
  if (objSubscriptionTransactionDetail.length) {
    for (var i in objSubscriptionTransactionDetail) {
      var row = objSubscriptionTransactionDetail[i];
      var data = { ...row };
      delete data.UserSubscriptionPackages;

      if (row.diff && row.diff > -3 && row.diff < 4) {
        if (
          row.UserSubscriptionPackages &&
          row.UserSubscriptionPackages.subscription_package_id
        ) {
          var packageId = mongoose.Types.ObjectId(
            row.UserSubscriptionPackages.subscription_package_id
          );
          var objSubscriptionPackageModel = await SubscriptionPackageModel.findById(
            packageId
          );
        }
        if (objSubscriptionPackageModel && objSubscriptionPackageModel._id) {
          data.product_id = objSubscriptionPackageModel.product_id;
        } else {
          data.product_id = "";
        }
        result.push(data);
      }
    }
  }

  return result;
};

const userExpiredSubscriptionNew = async () => {
  const currentTime = MOMENT().toDate();
  const lastSevenDaysTime = MOMENT(currentTime)
    .subtract(7, "day")
    .toDate();

  let objUserSubscriptionPackages = await UserSubscriptionPackageModel.aggregate(
    [
      {
        $match: {
          end_date: {
            $gt: lastSevenDaysTime,
            $lt: currentTime
          }
        }
      },
      {
        $group: {
          _id: "$subscription_transaction_parent_id",
          rowData: {
            $push: {
              row_id: "$_id",
              subscription_package_id: "$subscription_package_id",
              transaction_id: "$transaction_id",
              subscription_transaction_parent_id:
                "$subscription_transaction_parent_id",
              price: "$price",
              currency: "$currency",
              idfa_id: "$idfa_id",
              adid: "$adid",
              idfv: "$idfv",
              end_date: "$end_date"
            }
          }
        }
      },
      {
        $unwind: "$rowData"
      },
      {
        $sort: {
          "rowData.end_date": -1
        }
      },
      {
        $group: {
          _id: "$_id",
          rowData: {
            $push: {
              row_id: "$rowData.row_id",
              subscription_package_id: "$rowData.subscription_package_id",
              transaction_id: "$rowData.transaction_id",
              subscription_transaction_parent_id:
                "$rowData.subscription_transaction_parent_id",
              price: "$rowData.price",
              currency: "$rowData.currency",
              idfa_id: "$rowData.idfa_id",
              adid: "$rowData.adid",
              idfv: "$rowData.idfv",
              end_date: "$rowData.end_date"
            }
          }
        }
      },
      {
        $project: {
          _id: 1,
          rowData: {
            $slice: ["$rowData", 0, 1]
          }
        }
      },
      {
        $project: {
          _id: { $arrayElemAt: ["$rowData.row_id", 0] },
          subscription_package_id: {
            $arrayElemAt: ["$rowData.subscription_package_id", 0]
          },
          transaction_id: { $arrayElemAt: ["$rowData.transaction_id", 0] },
          subscription_transaction_parent_id: {
            $arrayElemAt: ["$rowData.subscription_transaction_parent_id", 0]
          },
          price: { $arrayElemAt: ["$rowData.price", 0] },
          currency: { $arrayElemAt: ["$rowData.currency", 0] },
          idfa_id: { $arrayElemAt: ["$rowData.idfa_id", 0] },
          adid: { $arrayElemAt: ["$rowData.adid", 0] },
          idfv: { $arrayElemAt: ["$rowData.idfv", 0] },
          end_date: { $arrayElemAt: ["$rowData.end_date", 0] }
        }
      }
    ]
  );

  let subscriptionTransactionParentIds = [];
  let userSubscriptionPackagesData = {};
  if (objUserSubscriptionPackages.length) {
    for (let i in objUserSubscriptionPackages) {
      let row = objUserSubscriptionPackages[i];
      subscriptionTransactionParentIds.push(
        row.subscription_transaction_parent_id
      );

      userSubscriptionPackagesData[row.subscription_transaction_parent_id] = {
        price: row.price,
        currency: row.currency,
        subscription_package_id: row.subscription_package_id,
        idfa_id: row.idfa_id,
        adid: row.adid,
        idfv: row.idfv,
        transaction_id: row.transaction_id,
        end_date: row.end_date
      };
    }
  }

  var result = [];
  if (subscriptionTransactionParentIds.length) {
    let objSubscriptionTransactionDetail = await SubscriptionTransactionDetailModel.find(
      {
        _id: {
          $in: subscriptionTransactionParentIds
        }
      }
    );

    if (!objSubscriptionTransactionDetail.length) {
      return result;
    }

    for (var i in objSubscriptionTransactionDetail) {
      var row = objSubscriptionTransactionDetail[i].toObject();
      var data = { ...row };
      let subscriptionPackageId = userSubscriptionPackagesData[row._id];

      if (subscriptionPackageId) {
        data["price"] = subscriptionPackageId.price;
        data["currency"] = subscriptionPackageId.currency;
        data["idfa_id"] = subscriptionPackageId.idfa_id;
        data["adid"] = subscriptionPackageId.adid;
        data["idfv"] = subscriptionPackageId.idfv;
        data["transaction_id"] = subscriptionPackageId.transaction_id;
        data["end_date"] = subscriptionPackageId.end_date;

        var packageId = mongoose.Types.ObjectId(
          subscriptionPackageId.subscription_package_id
        );
        var objSubscriptionPackageModel = await SubscriptionPackageModel.findById(
          packageId
        );
        if (objSubscriptionPackageModel && objSubscriptionPackageModel._id) {
          data.product_id = objSubscriptionPackageModel.product_id;
        } else {
          data.product_id = "";
        }
      }
      result.push(data);
    }

    result = _.orderBy(result, ["end_date"], ["asc"]);
  }

  return result;
};

const userExpiredSubscriptionTemporaryToGetOldTransactions = async () => {
  const currentTime = MOMENT()
    .subtract(3, "day")
    .toDate();
  const lastThreeDaysTime = MOMENT(currentTime)
    .subtract(3, "month")
    .toDate();

  let objUserSubscriptionPackages = await UserSubscriptionPackageModel.aggregate(
    [
      {
        $match: {
          is_processed: 0,
          end_date: {
            $gt: lastThreeDaysTime,
            $lt: currentTime
          }
        }
      },
      {
        $group: {
          _id: "$subscription_transaction_parent_id",
          rowData: {
            $push: {
              row_id: "$_id",
              subscription_package_id: "$subscription_package_id",
              transaction_id: "$transaction_id",
              subscription_transaction_parent_id:
                "$subscription_transaction_parent_id",
              price: "$price",
              currency: "$currency",
              idfa_id: "$idfa_id",
              adid: "$adid",
              idfv: "$idfv",
              end_date: "$idfv"
            }
          }
        }
      },
      {
        $unwind: "$rowData"
      },
      {
        $sort: {
          "rowData.end_date": 1
        }
      },
      {
        $group: {
          _id: "$_id",
          rowData: {
            $push: {
              row_id: "$rowData.row_id",
              subscription_package_id: "$rowData.subscription_package_id",
              transaction_id: "$rowData.transaction_id",
              subscription_transaction_parent_id:
                "$rowData.subscription_transaction_parent_id",
              price: "$rowData.price",
              currency: "$rowData.currency",
              idfa_id: "$rowData.idfa_id",
              adid: "$rowData.adid",
              idfv: "$rowData.idfv"
            }
          }
        }
      },
      {
        $project: {
          _id: 1,
          rowData: {
            $slice: ["$rowData", 0, 1]
          }
        }
      },
      {
        $project: {
          _id: { $arrayElemAt: ["$rowData.row_id", 0] },
          subscription_package_id: {
            $arrayElemAt: ["$rowData.subscription_package_id", 0]
          },
          transaction_id: { $arrayElemAt: ["$rowData.transaction_id", 0] },
          subscription_transaction_parent_id: {
            $arrayElemAt: ["$rowData.subscription_transaction_parent_id", 0]
          },
          price: { $arrayElemAt: ["$rowData.price", 0] },
          currency: { $arrayElemAt: ["$rowData.currency", 0] },
          idfa_id: { $arrayElemAt: ["$rowData.idfa_id", 0] },
          adid: { $arrayElemAt: ["$rowData.adid", 0] },
          idfv: { $arrayElemAt: ["$rowData.idfv", 0] }
        }
      },
      {
        $limit: 50
      }
    ]
  );

  let subscriptionTransactionParentIds = [];
  let userSubscriptionPackagesData = {};
  if (objUserSubscriptionPackages.length) {
    for (let i in objUserSubscriptionPackages) {
      let row = objUserSubscriptionPackages[i];
      subscriptionTransactionParentIds.push(
        row.subscription_transaction_parent_id
      );

      userSubscriptionPackagesData[row.subscription_transaction_parent_id] = {
        price: row.price,
        currency: row.currency,
        subscription_package_id: row.subscription_package_id,
        idfa_id: row.idfa_id,
        adid: row.adid,
        idfv: row.idfv,
        transaction_id: row.transaction_id,
        user_subscription_id: row._id
      };
    }
  }

  var result = [];
  if (subscriptionTransactionParentIds.length) {
    let objSubscriptionTransactionDetail = await SubscriptionTransactionDetailModel.find(
      {
        _id: {
          $in: subscriptionTransactionParentIds
        }
      }
    );

    if (!objSubscriptionTransactionDetail.length) {
      return result;
    }

    for (var i in objSubscriptionTransactionDetail) {
      var row = objSubscriptionTransactionDetail[i].toObject();
      var data = { ...row };
      let subscriptionPackageId = userSubscriptionPackagesData[row._id];

      if (subscriptionPackageId) {
        data["_id"] = subscriptionPackageId.user_subscription_id;
        data["price"] = subscriptionPackageId.price;
        data["currency"] = subscriptionPackageId.currency;
        data["idfa_id"] = subscriptionPackageId.idfa_id;
        data["adid"] = subscriptionPackageId.adid;
        data["idfv"] = subscriptionPackageId.idfv;
        data["transaction_id"] = subscriptionPackageId.transaction_id;

        var packageId = mongoose.Types.ObjectId(
          subscriptionPackageId.subscription_package_id
        );
        var objSubscriptionPackageModel = await SubscriptionPackageModel.findById(
          packageId
        );
        if (objSubscriptionPackageModel && objSubscriptionPackageModel._id) {
          data.product_id = objSubscriptionPackageModel.product_id;
        } else {
          data.product_id = "";
        }
      }
      result.push(data);
    }
  }

  return result;
};

const purchasedSubscriptions = async userId => {
  let tblSubscriptionTransactionDetail =
    SubscriptionTransactionDetailModel.collection.name;
  let tblSubscriptionTransactionDetailDeviceType =
    tblSubscriptionTransactionDetail + ".device_type";
  const where = {
    user_id: userId,
    parent_transaction_id: {
      $not: /^.*added by.*$/i
    },
    tblSubscriptionTransactionDetailDeviceType:
      UserSubscriptionPackageModel.IOS_DEVICE_TYPE
  };
  var objSubscriptionTransactionDetail = await SubscriptionTransactionDetailModel.find(
    where
  ).sort({ _id: -1 });
  return objSubscriptionTransactionDetail;
};

const validatePlayStoreReceiptProcess = async (req, res, sendResponse = 1) => {
  var apiResponse = Utility.setResponse();

  const rules = {
    purchase_token: "required",
    // device_id: 'some',
    subscription_id: "required",
    order_id: "required",
    user_id: "required",
    price: "required",
    currency: "required"
  };

  const validator = Utility.validateInputs(req.body, rules);
  const matched = validator.check();
  if (!matched) {
    apiResponse.message = Utility.formatValidationMessages(validator.errors);
    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      return apiResponse;
    }
  }

  let subscriptionDetail = await validatePlayStoreReceipt(req);

  var subscriptionId = req.body.subscription_id;

  const userId = mongoose.Types.ObjectId(req.body.user_id);
  const orderId = req.body.order_id;
  const purchaseToken = req.body.purchase_token;

  const price = req.body.price;
  const currency = req.body.currency;

  try {
    const objUserDevice = await UserDevicesModel.findById(userId);
    if (!objUserDevice) {
      apiResponse.message = Utility.getApiMessages("invalid_request");
      if (sendResponse) {
        return Utility.sendResponse(req, res, apiResponse);
      } else {
        return apiResponse;
      }
    }

    var androidFreeTrial = "";
    var isBoostSubscription = 0;

    var packageId = await getAndroidPackageId(subscriptionId);
    if (subscriptionId) {
      if (!subscriptionId.match("free")) {
        androidFreeTrial = 0;
      } else {
        androidFreeTrial = 1;
      }

      if (subscriptionId.match("boost")) {
        isBoostSubscription = 1;
      }
    }

    // Subscription Id Require for Package Id Now we get the Package Id from Product id
    if (!subscriptionId && purchaseToken) {
      var subscriptionPackageId = await getPackageIdForAndroid(purchaseToken);

      subscriptionId = subscriptionPackageId.package_id;
      if (subscriptionId) {
        packageId = await getAndroidPackageId(subscriptionId);
      }
    }

    var expireDate = "";
    var isSubscriptionExist = "";
    var message = (status = "");

    var userDeviceUpdateData = {
      is_premium: SubscriptionPackageModel.NOT_IS_PREMIUM
    };

    const currentDate = Utility.getGMTDate();

    if (subscriptionDetail["status"] == "success") {
      subscriptionDetail = subscriptionDetail.receipt_response.payload;
      if (subscriptionDetail["expiryTimeMillis"]) {
        expireDate = Utility.formatUnixToGMTDate(
          subscriptionDetail["expiryTimeMillis"] / 1000,
          "YYYY-MM-DD"
        );

        //for check free trial status
        var isFreeTrial = UserSubscriptionPackageModel.NOT_FREE_TRIAL;
        const paymentState = subscriptionDetail["paymentState"];
        if (paymentState == 2) {
          isFreeTrial = UserSubscriptionPackageModel.IS_FREE_TRIAL;
        }

        var dbIsPremium = objUserDevice.is_premium;
        var dbExpiryDatePremium = objUserDevice.expires_date_ms;

        //check expiry date grater than current date
        if (expireDate >= currentDate) {
          if (isBoostSubscription) {
            userDeviceUpdateData.is_super_premium =
              SubscriptionPackageModel.IS_SUPER_PREMIUM;
            userDeviceUpdateData.expires_date_ms_super =
              subscriptionDetail["expiryTimeMillis"];

            //if user premium expiry date less then super premium expiry date
            //then we will update premium expiry date with it
            if (
              dbIsPremium &&
              dbExpiryDatePremium &&
              subscriptionDetail["expiryTimeMillis"] > dbExpiryDatePremium
            ) {
              userDeviceUpdateData.is_premium =
                SubscriptionPackageModel.IS_PREMIUM;
              userDeviceUpdateData.expires_date_ms =
                subscriptionDetail["expiryTimeMillis"];
            }
          } else {
            userDeviceUpdateData = {
              is_premium: SubscriptionPackageModel.IS_PREMIUM,
              expires_date_ms: subscriptionDetail["expiryTimeMillis"]
            };
          }
        }

        isSubscriptionExist = await getParentSubscriptionAndroid(
          expireDate,
          purchaseToken
        );

        if (
          isSubscriptionExist &&
          isSubscriptionExist.parentSubscriptionId &&
          isSubscriptionExist.userId != req.body.user_id
        ) {
          message = "This subscription is associated with some other user.";
          status = Utility.STATUS_ERROR;
        } else if (
          isSubscriptionExist &&
          isSubscriptionExist.parentSubscriptionId &&
          isSubscriptionExist.renewedSubscriptionId
        ) {
          message = "This subscription is already added for you";
          status = Utility.STATUS_ERROR;
        } else if (
          isSubscriptionExist &&
          isSubscriptionExist.parentSubscriptionId &&
          _.isEmpty(isSubscriptionExist.renewedSubscriptionId)
        ) {
          message = "Subscription added";
          status = Utility.STATUS_SUCCESS;
          await addSubscriptionRenewalTransaction(
            userId,
            isSubscriptionExist.parentSubscriptionId,
            orderId,
            expireDate,
            packageId._id,
            message,
            isFreeTrial,
            currency,
            price
          );
        } else if (_.isEmpty(isSubscriptionExist)) {
          message = "Subscription added";
          status = Utility.STATUS_SUCCESS;
          mailSent = 1;
          var parentSubscriptionId = await addParentSubscription(
            userId,
            SubscriptionPackageModel.ANDROID_DEVICE_TYPE,
            purchaseToken,
            purchaseToken,
            subscriptionDetail["autoRenewing"],
            subscriptionDetail
          );
          await addSubscriptionRenewalTransaction(
            userId,
            parentSubscriptionId,
            orderId,
            expireDate,
            packageId._id,
            message,
            isFreeTrial,
            currency,
            price
          );
        }
      } else {
        //add failed transaction
        message = "Subscription expired in android or invalid token";
        await addParentSubscriptionFailed(
          userId,
          SubscriptionPackageModel.ANDROID_DEVICE_TYPE,
          purchaseToken,
          message,
          subscriptionDetail["status"]
        );
        message = "Invalid Purchase Token";
        status = Utility.STATUS_ERROR;
        mailSent = 0;
      }
    } else {
      message = "Invalid Purchase Token";
      await addParentSubscriptionFailed(
        userId,
        SubscriptionPackageModel.ANDROID_DEVICE_TYPE,
        purchaseToken,
        message,
        subscriptionDetail
      );
      status = Utility.STATUS_ERROR;
      mailSent = 0;
    }
    apiResponse.status = status;
    apiResponse.message = message;

    if (objUserDevice.user_id && parseInt(objUserDevice.user_id)) {
      await updateUserSubscription(objUserDevice.user_id, userDeviceUpdateData);
    } else {
      await updateUserDeviceSubscription(objUserDevice, userDeviceUpdateData);
    }

    const deviceData = await UserDevicesModel.findById(userId);

    apiResponse.data = {
      is_premium: deviceData.is_premium,
      expires_date_ms: deviceData.expires_date_ms,

      is_super_premium: deviceData.is_super_premium,
      expires_date_ms_super: deviceData.expires_date_ms_super,

      is_premium_with_trial: deviceData.is_premium_with_trial
        ? deviceData.is_premium_with_trial
        : 0,
      free_trial_expire_date: deviceData.free_trial_expire_date
    };

    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      if (apiResponse.status == Utility.STATUS_SUCCESS) {
        apiResponse.receiptResponse = subscriptionDetail;
      }
      return apiResponse;
    }
  } catch (err) {
    var message = "Line: " + err.line + err.message;
    var receiptResponseStatus =
      "status" in subscriptionDetail ? subscriptionDetail["status"] : "Failed";
    await addParentSubscriptionFailed(
      userId,
      SubscriptionPackageModel.ANDROID_DEVICE_TYPE,
      purchaseToken,
      message,
      receiptResponseStatus
    );

    apiResponse.developer_messages = "Request not valid." + err.message;
    apiResponse.status = Utility.STATUS_ERROR;

    apiResponse.message = "Something went wrong. Please try again.";

    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      return apiResponse;
    }
  }
};

const validatePlayStoreReceiptProcessNew = async (
  req,
  res,
  sendResponse = 1
) => {
  var apiResponse = Utility.setResponse();

  const rules = {
    purchase_token: "required",
    // device_id: 'some',
    subscription_id: "required",
    order_id: "required"
  };

  const validator = Utility.validateInputs(req.body, rules);
  const matched = validator.check();
  if (!matched) {
    apiResponse.message = Utility.formatValidationMessages(validator.errors);
    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      return apiResponse;
    }
  }

  let subscriptionDetail = await validatePlayStoreReceipt(req);

  var subscriptionId = req.body.subscription_id;

  let userId =
    req.body.user_id && parseInt(req.body.user_id)
      ? new mongoose.mongo.ObjectId(req.body.user_id)
      : "";

  const isUserIdExists = userId ? 1 : 0;

  const orderId = req.body.order_id;
  const purchaseToken = req.body.purchase_token;

  const price = req.body.price;
  const currency = req.body.currency;

  const gpsAdid = req.body.gps_adid;
  const callingFrom = req.body.calling_from;

  let paymentState = req.body.paymentState;

  if (paymentState) {
    paymentState = subscriptionDetail["paymentState"];
  }

  try {
    let objUserDevice = "";

    if (isUserIdExists) {
      objUserDevice = await UserDevicesModel.findById(userId);
    } else {
      userId = new mongoose.mongo.ObjectId("000000000000000000000000");
    }

    objUserDevice = await UserDevicesModel.findById(userId);
    // if (!objUserDevice) {
    //     apiResponse.message = Utility.getApiMessages('invalid_request');
    //     if (sendResponse) {
    //         return Utility.sendResponse(req, res, apiResponse);
    //     } else {
    //         return apiResponse;
    //     }
    // }

    var androidFreeTrial = "";
    var isBoostSubscription = 0;

    var packageId = await getAndroidPackageId(subscriptionId);
    if (subscriptionId) {
      if (!subscriptionId.match("free")) {
        androidFreeTrial = 0;
      } else {
        androidFreeTrial = 1;
      }

      if (subscriptionId.match("boost")) {
        isBoostSubscription = 1;
      }
    }

    // Subscription Id Require for Package Id Now we get the Package Id from Product id
    if (!subscriptionId && purchaseToken) {
      var subscriptionPackageId = await getPackageIdForAndroid(purchaseToken);

      subscriptionId = subscriptionPackageId.package_id;
      if (subscriptionId) {
        packageId = await getAndroidPackageId(subscriptionId);
      }
    }

    var expireDate = "";
    var isSubscriptionExist = "";
    var message = (status = "");

    var userDeviceUpdateData = {
      is_premium: SubscriptionPackageModel.NOT_IS_PREMIUM
    };

    let parentTransactionId = orderId;
    if (orderId.length > 24) {
      parentTransactionId = parentTransactionId.slice(0, 24);
    }

    const currentDate = Utility.getGMTDate();

    if (subscriptionDetail["status"] == "success") {
      subscriptionDetail = subscriptionDetail.receipt_response.payload;

      expireDate = Utility.formatUnixToGMTDate(
        subscriptionDetail["expiryTimeMillis"] / 1000,
        "YYYY-MM-DD"
      );

      let purchaseDateMS = subscriptionDetail["startTimeMillis"];

      //for check free trial status
      var isFreeTrial = UserSubscriptionPackageAndroidModel.NOT_FREE_TRIAL;
      if (paymentState == 2) {
        isFreeTrial = UserSubscriptionPackageAndroidModel.IS_FREE_TRIAL;
      }

      // var dbIsPremium = objUserDevice.is_premium;
      // var dbExpiryDatePremium = objUserDevice.expires_date_ms;

      //check expiry date grater than current date
      // if (expireDate >= currentDate) {
      if (1) {
        var description = "Active Subscription";
        if (isBoostSubscription) {
          userDeviceUpdateData.is_super_premium =
            SubscriptionPackageModel.IS_SUPER_PREMIUM;
          userDeviceUpdateData.expires_date_ms_super =
            subscriptionDetail["expiryTimeMillis"];

          //if user premium expiry date less then super premium expiry date
          //then we will update premium expiry date with it
          // if (
          //     dbIsPremium &&
          //     dbExpiryDatePremium &&
          //     subscriptionDetail['expiryTimeMillis'] > dbExpiryDatePremium
          // ) {
          //     userDeviceUpdateData.is_premium = SubscriptionPackageModel.IS_PREMIUM;
          //     userDeviceUpdateData.expires_date_ms = subscriptionDetail['expiryTimeMillis'];
          // }
        } else {
          userDeviceUpdateData = {
            is_premium: SubscriptionPackageModel.IS_PREMIUM,
            expires_date_ms: subscriptionDetail["expiryTimeMillis"]
          };
        }

        var isNewActivation =
          UserSubscriptionPackageAndroidModel.NOT_NEW_ACTIVATION;
        var isFreeTrialConvertedToPaid =
          UserSubscriptionPackageAndroidModel.IS_NOT_TRIAL_CONVERT_TO_PAID;
        var isRenewalPurchase =
          UserSubscriptionPackageAndroidModel.IS_NOT_RENEWAL;
        var isStartAsPaidTransaction =
          UserSubscriptionPackageAndroidModel.IS_NOT_START_AS_PAID_TRANSACTION;
        var isRenewalFromBillingRetry =
          UserSubscriptionPackageAndroidModel.NOT_BILLING_RETRY_RENEWAL;
        var isConversionFromBillingRetry =
          UserSubscriptionPackageAndroidModel.NOT_BILLING_RETRY_CONVERSION;

        var isReactivation =
          UserSubscriptionPackageAndroidModel.NOT_REACTIVATION;

        var adjustEventIdToSent = "";
        var adjustEventNameToSent = "";

        if (isFreeTrial) {
          isNewActivation =
            UserSubscriptionPackageAndroidModel.IS_NEW_ACTIVATION;
          adjustEventIdToSent = process.env.ADJUST_EVENT_FREE_TRIAL_START;
          adjustEventNameToSent = "FREE TRIAL STARTED " + subscriptionId;
        } else {
          //not free
          //as fresh start
          //conversion
          //renewal
          isFreeTrial = UserSubscriptionPackageAndroidModel.NOT_FREE_TRIAL;
          if (parentTransactionId == orderId) {
            isNewActivation =
              UserSubscriptionPackageAndroidModel.IS_NEW_ACTIVATION;
            isStartAsPaidTransaction =
              UserSubscriptionPackageAndroidModel.IS_START_AS_PAID_TRANSACTION;

            if (subscriptionId == "fastvpnskutwelvemonth") {
              adjustEventIdToSent =
                process.env
                  .ANDROID_YEARLY_PAID_SUBSCRIPTION_START_ADJUST_EVENT_ID;
              adjustEventNameToSent = "START YEARLY SUBSCRIPTION";
            } else if (subscriptionId == "fastvpnskusixmonth") {
              adjustEventIdToSent =
                process.env.ANDROID_ADJUST_EVENT_6_MONTH_SUBSCRIPTION_START;
              adjustEventNameToSent = "START SIX MONTH SUBSCRIPTION";
            } else if (subscriptionId == "fastvpnskuthreemonth") {
              adjustEventIdToSent =
                process.env.ANDROID_ADJUST_EVENT_3_MONTH_SUBSCRIPTION_START;
              adjustEventNameToSent = "START THREE MONTH SUBSCRIPTION";
            }
            //monthly subscription event
            else if (subscriptionId == "fastvpnskuonemonth") {
              adjustEventIdToSent =
                process.env
                  .ANDROID_MONTHLY_PAID_SUBSCRIPTION_START_ADJUST_EVENT_ID;
              adjustEventNameToSent = "START MONTHLY SUBSCRIPTION";
            }

            //weekly subscription event
            else if (subscriptionId == "fastvpnskuoneweek") {
              adjustEventIdToSent =
                process.env.ANDROID_WEEK_SUBSCRIPTION_START_ADJUST_EVENT_ID;
              adjustEventNameToSent = "START WEEKLY SUBSCRIPTION";
            }

            //discount subscription event
            else if (subscriptionId.match(/discount/i)) {
              adjustEventIdToSent =
                process.env
                  .ANDROID_DISCOUNTED_SUBSCRIPTION_START_ADJUST_EVENT_ID;
              adjustEventNameToSent = "START DISCOUNT SUBSCRIPTION";
            }
            //premium subscription event
            else if (subscriptionId == "boost_speed_1month") {
              adjustEventIdToSent =
                process.env
                  .ANDROID_BOOST_SPEED_SUBSCRIPTION_START_ADJUST_EVENT_ID;
              adjustEventNameToSent = "START BOOST SERVER SUBSCRIPTION";
            } else {
              adjustEventIdToSent = "NA";
              adjustEventNameToSent = "No Event found";
            }
          } else {
            if (isFreeTrialConvertedToPaid) {
              isStartAsPaidTransaction =
                UserSubscriptionPackageAndroidModel.IS_START_AS_PAID_TRANSACTION;

              if (subscriptionId == "fastvpnskutwelvemonth") {
                adjustEventIdToSent =
                  process.env
                    .ADJUST_EVENT_CONVERT_YEARLY_SUBSCRIPTION_FROM_TRIAL;
                adjustEventNameToSent =
                  "CONVERT YEARLY SUBSCRIPTION FROM TRIAL";
              } else if (subscriptionId == "fastvpnskusixmonth") {
                adjustEventIdToSent =
                  process.env
                    .ADJUST_EVENT_CONVERT_6_MONTH_SUBSCRIPTION_FROM_TRIAL;
                adjustEventNameToSent =
                  "CONVERT SIX MONTH SUBSCRIPTION FROM TRIAL";
              } else if (subscriptionId == "fastvpnskuthreemonth") {
                adjustEventIdToSent =
                  process.env
                    .ADJUST_EVENT_CONVERT_3_MONTH_SUBSCRIPTION_FROM_TRIAL;
                adjustEventNameToSent =
                  "CONVERT THREE MONTH SUBSCRIPTION FROM TRIAL";
              }
              //monthly subscription event
              else if (subscriptionId == "fastvpnskuonemonth") {
                adjustEventIdToSent =
                  process.env
                    .ADJUST_EVENT_CONVERT_MONTHLY_SUBSCRIPTION_FROM_TRIAL;
                adjustEventNameToSent =
                  "CONVERT MONTHLY SUBSCRIPTION FROM TRIAL";
              }

              //weekly subscription event
              else if (subscriptionId == "fastvpnskuoneweek") {
                adjustEventIdToSent =
                  process.env
                    .ADJUST_EVENT_CONVERT_WEEKLY_SUBSCRIPTION_FROM_TRIAL;
                adjustEventNameToSent =
                  "CONVERT WEEKLY SUBSCRIPTION FROM TRIAL";
              }

              //discount subscription event
              else if (productId.match(/discount/i)) {
                adjustEventIdToSent =
                  process.env
                    .ADJUST_EVENT_CONVERT_DISCOUNTED_SUBSCRIPTION_FROM_TRIAL;
                adjustEventNameToSent =
                  "CONVERT DISCOUNT SUBSCRIPTION FROM TRIAL";
              }
              //premium subscription event
              else if (subscriptionId == "boost_speed_1month") {
                adjustEventIdToSent =
                  process.env
                    .ADJUST_EVENT_CONVERT_BOOST_SUBSCRIPTION_FROM_TRIAL;
                adjustEventNameToSent =
                  "CONVERT BOOST SERVER SUBSCRIPTION FROM TRIAL";
              } else {
                adjustEventIdToSent = "NA";
                adjustEventNameToSent = "No Event found";
              }
            } else {
              isRenewalPurchase =
                UserSubscriptionPackageAndroidModel.IS_RENEWAL;

              if (subscriptionId == "fastvpnskutwelvemonth") {
                adjustEventIdToSent =
                  process.env.ANDROID_ADJUST_EVENT_RENEWAL_YEARLY_SUBSCRIPTION;
                adjustEventNameToSent = "YEARLY RENEWAL SUBSCRIPTION";
              } else if (subscriptionId == "fastvpnskusixmonth") {
                adjustEventIdToSent =
                  process.env.ANDROID_ADJUST_EVENT_RENEWAL_6_MONTH_SUBSCRIPTION;
                adjustEventNameToSent = "SIX MONTH RENEWAL SUBSCRIPTION";
              } else if (subscriptionId == "fastvpnskuthreemonth") {
                adjustEventIdToSent =
                  process.env.ANDROID_ADJUST_EVENT_RENEWAL_3_MONTH_SUBSCRIPTION;
                adjustEventNameToSent = "THREE MONTH RENEWAL SUBSCRIPTION";
              }
              //monthly subscription event
              else if (subscriptionId == "fastvpnskuonemonth") {
                adjustEventIdToSent =
                  process.env.ANDROID_ADJUST_EVENT_RENEWAL_MONTHLY_SUBSCRIPTION;
                adjustEventNameToSent = "MONTHLY RENEWAL SUBSCRIPTION";
              }

              //weekly subscription event
              else if (subscriptionId == "fastvpnskuoneweek") {
                adjustEventIdToSent =
                  process.env.ANDROID_ADJUST_EVENT_RENEWAL_WEEKLY_SUBSCRIPTION;
                adjustEventNameToSent = "WEEKLY RENEWAL SUBSCRIPTION";
              }

              //discount subscription event
              else if (productId.match(/discount/i)) {
                adjustEventIdToSent =
                  process.env
                    .ANDROID_DISCOUNTED_SUBSCRIPTION_START_ADJUST_EVENT_ID;
                adjustEventNameToSent = "DISCOUNT RENEWAL SUBSCRIPTION";
              }
              //premium subscription event
              else if (subscriptionId == "boost_speed_1month") {
                adjustEventIdToSent =
                  process.env.ANDROID_ADJUST_EVENT_RENEWAL_BOOST_SUBSCRIPTION;
                adjustEventNameToSent = "BOOST RENEWAL SUBSCRIPTION";
              } else {
                adjustEventIdToSent = "NA";
                adjustEventNameToSent = "No Event found";
              }
            }
          }
        }

        let cancelDate = subscriptionDetail["userCancellationTimeMillis"];
        if (cancelDate) {
          description =
            "Subscription was active till " +
            payment["expires_date"] +
            " but user cancelled or modified on " +
            payment["cancellation_date"];
        }

        var isSubscriptionExist = null;
        var subscriptionWithParentTransactionId = null;
        // check if original transaction was in billing retry period
        let isOriginalTransactionIsInBillingPeriod = false;
        let isAutoRenewalActive = 1;

        let existingSubscriptionForThisTransaction = await getParentSubscriptionAndroidNew(
          orderId,
          parentTransactionId
        );
        for (var i in existingSubscriptionForThisTransaction) {
          var value = existingSubscriptionForThisTransaction[i];
          if (value.renewedSubscriptionTransactionId == orderId) {
            isSubscriptionExist = value;
          }
          subscriptionWithParentTransactionId = value;
          isOriginalTransactionIsInBillingPeriod = value.is_in_billing_retry;
          isAutoRenewalActive = value.auto_renewing;
        }

        if (isAutoRenewalActive == 0) {
          isReactivation = UserSubscriptionPackageAndroidModel.IS_REACTIVATION;
        }

        if (isOriginalTransactionIsInBillingPeriod && isRenewalPurchase) {
          isRenewalFromBillingRetry =
            UserSubscriptionPackageAndroidModel.IS_BILLING_RETRY_RENEWAL;
        }

        if (
          isOriginalTransactionIsInBillingPeriod &&
          isFreeTrialConvertedToPaid
        ) {
          isConversionFromBillingRetry =
            UserSubscriptionPackageAndroidModel.IS_BILLING_RETRY_CONVERSION;
        }

        if (
          isSubscriptionExist == null &&
          subscriptionWithParentTransactionId == null
        ) {
          // There is no subscription in the database so we can add to database
          description = description + " -- Transaction Added First Time";
          var parentSubscriptionId = await addParentSubscriptionAndroid(
            userId,
            SubscriptionPackageModel.ANDROID_DEVICE_TYPE,
            parentTransactionId,
            purchaseToken,
            subscriptionDetail["autoRenewing"],
            subscriptionDetail
          );

          let renewalData = {
            adid: "",
            price: price,
            user_id: userId,
            gps_adid: gpsAdid,
            currency: currency,
            package_id: packageId._id,
            end_date: expireDate,
            description: description,
            is_free_trial: isFreeTrial,
            transaction_id: orderId,
            parent_subscription_id: parentSubscriptionId,
            purchase_date: purchaseDateMS,

            is_renewal: isRenewalPurchase,
            is_trial_convert_to_paid: isFreeTrialConvertedToPaid,
            is_start_as_paid_transaction: isStartAsPaidTransaction,
            adjust_event_name: adjustEventNameToSent,
            adjust_event_id: adjustEventIdToSent,
            calling_from: callingFrom,
            subscription_package_name: subscriptionId,
            is_new_activation: isNewActivation,

            parent_transaction_id: parentTransactionId,

            is_billing_retry_renewal: isRenewalFromBillingRetry,
            is_billing_retry_conversion: isConversionFromBillingRetry,

            is_reactivation: isReactivation
          };
          if (cancelDate) {
            renewalData.cancellation_date = cancelDate;
          }

          await addSubscriptionRenewalTransactionNewAndroid(renewalData);
        } else if (subscriptionWithParentTransactionId) {
          // There is subscription in the database but may be associated to other userId.
          // So lets check if the purchase is new purchase then we can associate the purchase with current user id or if its running (old) purchase then we should not add same purchase to sent user id
          if (
            subscriptionWithParentTransactionId &&
            subscriptionWithParentTransactionId.parentSubscriptionId &&
            !subscriptionWithParentTransactionId.renewedSubscriptionId
          ) {
            description =
              description +
              " -- This Transaction does not exist in user subscription table so adding. Parent Transaction Id " +
              subscriptionWithParentTransactionId.parentSubscriptionId +
              " Transaction id - " +
              orderId +
              " -- New Entry Added";

            //update user id in parent transaction details table if in case it was purchased from different user
            //but updated or restored in different user device
            if (userId != subscriptionWithParentTransactionId.userId) {
              description =
                " -- The original purchase was associated to different user but this transaction is upgraded or downgraded with different user id. Current Transaction does not exist in user subscription table so adding. Parent Transaction Id " +
                subscriptionWithParentTransactionId.parentSubscriptionId +
                " Transaction id - " +
                orderId +
                " previous user_id = " +
                subscriptionWithParentTransactionId.userId +
                " -- Entry Updated";

              let parentSubscriptionId = mongoose.Types.ObjectId(
                subscriptionWithParentTransactionId.parentSubscriptionId
              );
              await updateParentSubscriptionDataAndroid(
                parentSubscriptionId,
                userId
              );
            }

            let renewalData = {
              adid: "",
              price: price,
              user_id: userId,
              gps_adid: gpsAdid,
              currency: currency,
              package_id: packageId._id,
              end_date: expireDate,
              description: description,
              is_free_trial: isFreeTrial,
              is_renewal: isRenewalPurchase,
              is_trial_convert_to_paid: isFreeTrialConvertedToPaid,
              is_start_as_paid_transaction: isStartAsPaidTransaction,
              transaction_id: orderId,
              parent_subscription_id:
                subscriptionWithParentTransactionId.parentSubscriptionId,
              purchase_date: purchaseDateMS,
              adjust_event_name: adjustEventNameToSent,
              adjust_event_id: adjustEventIdToSent,

              calling_from: callingFrom,
              subscription_package_name: subscriptionId,
              is_new_activation: isNewActivation,

              parent_transaction_id: parentTransactionId,

              is_billing_retry_renewal: isRenewalFromBillingRetry,
              is_billing_retry_conversion: isConversionFromBillingRetry,

              is_reactivation: isReactivation
            };

            if (cancelDate) {
              renewalData.cancellation_date = cancelDate;
            }
            await addSubscriptionRenewalTransactionNewAndroid(renewalData);
          } else if (
            subscriptionWithParentTransactionId.parentSubscriptionId &&
            subscriptionWithParentTransactionId.renewedSubscriptionId
          ) {
            description =
              description +
              " This Transaction exist in user subscription table so updating. Parent Transaction Id " +
              subscriptionWithParentTransactionId.parentSubscriptionId +
              " Transaction id - " +
              orderId +
              " -- Entry updated";

            //update user id in parent transaction details table if in case it was purchased from different user
            //but updated or restored in different user device
            if (userId != subscriptionWithParentTransactionId.userId) {
              description =
                description +
                " -- The original purchase was associated to different user but this transaction is upgraded or downgraded with different user id. Current Transaction does not exist in user subscription table so adding. Parent Transaction Id " +
                subscriptionWithParentTransactionId.parentSubscriptionId +
                " Transaction id - " +
                transactionId +
                " previous user_id = " +
                subscriptionWithParentTransactionId.userId +
                " -- Entry updated - else ";

              let parentSubscriptionId = mongoose.Types.ObjectId(
                subscriptionWithParentTransactionId.parentSubscriptionId
              );
              await updateParentSubscriptionDataAndroid(
                parentSubscriptionId,
                userId
              );
            }

            let updateSubscriptionTransactionData = {
              renewedSubscriptionId:
                subscriptionWithParentTransactionId.renewedSubscriptionId,
              description
            };

            let updatedCallingFrom = subscriptionWithParentTransactionId.calling_from
              ? subscriptionWithParentTransactionId.calling_from +
                " - " +
                callingFrom
              : callingFrom;

            updateSubscriptionTransactionData["price"] = price;
            updateSubscriptionTransactionData["user_id"] = userId;
            updateSubscriptionTransactionData["currency"] = currency;
            updateSubscriptionTransactionData["adid"] = adid;
            updateSubscriptionTransactionData["gps_adid"] = gpsAdid;
            updateSubscriptionTransactionData["end_date"] = expireDate;
            updateSubscriptionTransactionData[
              "calling_from"
            ] = updatedCallingFrom;

            updateSubscriptionTransactionData[
              "subscription_package_name"
            ] = subscriptionId;

            updateSubscriptionTransactionData[
              "parent_transaction_id"
            ] = parentTransactionId;

            updateSubscriptionTransactionData["transaction_id"] = orderId;

            await updateSubscriptionTransactionNewAndroid(
              updateSubscriptionTransactionData
            );
          }
        }
      }
    } else {
      message = "Invalid Purchase Token";

      await addParentSubscriptionFailed(
        userId,
        SubscriptionPackageModel.ANDROID_DEVICE_TYPE,
        purchaseToken,
        message,
        subscriptionDetail
      );
      status = Utility.STATUS_ERROR;
      mailSent = 0;
    }
    apiResponse.status = status;
    apiResponse.message = message;

    if (objUserDevice) {
      if (objUserDevice.user_id && parseInt(objUserDevice.user_id)) {
        await updateUserSubscription(
          objUserDevice.user_id,
          userDeviceUpdateData
        );
      } else {
        await updateUserDeviceSubscription(objUserDevice, userDeviceUpdateData);
      }
    }

    let deviceData = "";
    if (isUserIdExists) {
      deviceData = await UserDevicesModel.findById(userId);
    }

    apiResponse.data = {
      is_premium: deviceData ? deviceData.is_premium : 0,
      expires_date_ms: deviceData ? deviceData.expires_date_ms : "",

      is_super_premium: deviceData ? deviceData.is_super_premium : 0,
      expires_date_ms_super: deviceData ? deviceData.expires_date_ms_super : "",

      is_premium_with_trial: deviceData ? deviceData.is_premium_with_trial : 0,
      free_trial_expire_date: deviceData
        ? deviceData.free_trial_expire_date
        : ""
    };

    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      if (apiResponse.status == Utility.STATUS_SUCCESS) {
        apiResponse.receiptResponse = subscriptionDetail;
      }
      return apiResponse;
    }
  } catch (err) {
    var message = "Line: " + err.line + " => " + err.message;
    var receiptResponseStatus =
      "status" in subscriptionDetail ? subscriptionDetail["status"] : "Failed";
    await addParentSubscriptionFailed(
      userId,
      SubscriptionPackageModel.ANDROID_DEVICE_TYPE,
      purchaseToken,
      message,
      receiptResponseStatus
    );

    apiResponse.developer_messages = "Request not valid." + err.message;
    apiResponse.status = Utility.STATUS_ERROR;

    apiResponse.message = "Something went wrong. Please try again.";

    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      return apiResponse;
    }
  }
};

/**
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Number} sendResponse
 */
const validateIOSReceiptProcessNew = async (req, res, sendResponse = 1) => {
  let headers = req.headers;
  var apiResponse = Utility.setResponse();
 
  let userId =
    req.headers.auth_user_id && parseInt(req.headers.auth_user_id)
      ? new mongoose.mongo.ObjectId(req.headers.auth_user_id)
      : "";

  const isUserIdExists = userId ? 1 : 0;

  var subscriptionToken = req.body.app_store_receipt;

  const rules = {
    app_store_receipt: "required"
    //'is_debug': 'required',
    // 'user_id': 'required',
    // 'price': 'required',
    //'currency': 'required',
  };

  const validator = await Utility.validateInputs(req.body, rules);
  const matched = await validator.check();
  if (!matched) {
    apiResponse.message = Utility.formatValidationMessages(validator.errors);
    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      return apiResponse;
    }
  }

  var receiptResponse = await validateIOSReceipt(req);

  const refresh = req.body.refresh ? req.body.refresh : 0;
  const isRestore = req.body.is_restore ? req.body.is_restore : 0;

  const callingFrom = req.body.calling_from ? req.body.calling_from : "";

  apiResponse.refresh = refresh;
  apiResponse.is_restore = isRestore;

  try {
    let objUserDevice = "";

    if (isUserIdExists) {
      objUserDevice = await UserModel.findById(userId);
    } else {
      userId = new mongoose.mongo.ObjectId("000000000000000000000000");
    }

    const expireCurrentDateMs = +new Date();
    // const expireCurrentDateMs = 1583020800000;
    var userDeviceUpdateData = {
      // is_premium: SubscriptionPackageModel.NOT_IS_PREMIUM,
      // expires_date_ms: expireCurrentDateMs
    };

    var payments = [];
    var paymentCount = 0;
    // if (receiptResponse && receiptResponse.latest_receipt_info && receiptResponse.latest_receipt_info instanceof Array) {
    //     payments = receiptResponse.latest_receipt_info;
    // } else if (receiptResponse && receiptResponse.receipt && receiptResponse.receipt.in_app instanceof Array) {
    //     payments = receiptResponse.receipt.in_app;
    // }

    if (
      receiptResponse &&
      receiptResponse.receipt &&
      receiptResponse.receipt.in_app instanceof Array
    ) {
      payments = receiptResponse.receipt.in_app;
    

    if (payments.length) {
      payments = _.orderBy(payments, ["purchase_date_ms"], ["desc"]);
    }

    var productIdName = "";
    for (i in payments) {
      const payment = payments[i];

      let cancelDate = "";
      let purchaseDateMS = payment.purchase_date_ms;

      //get all active subscription from the receipt response
      //check expiry date grater than current date

      const transactionId = payment.transaction_id;
      const CheckSubscription = await UserSubscriptionPackageModel.findOne({
        transaction_id: transactionId
      });
      if (CheckSubscription) {
        apiResponse.status = Utility.STATUS_ERROR;
        apiResponse.message = "already purchase";

        apiResponse.show_message_to_user = Utility.NOT_SHOW_MESSAGE;
        return Utility.sendResponse(req, res, apiResponse);
      }
      //if (purchaseDateMS >= expireCurrentDateMs) {
      
      //  const parentTransactionId = payment.original_transaction_id;

        const productId = payment.product_id;
        productIdName = payment.product_id;
        var description = "Active Subscription";
        const packageId = await SubscriptionPackageModel.findOne({
          product_id: productId
        });

        // var dbIsPremium = objUserDevice.is_premium;
        // var dbExpiryDatePremium = objUserDevice.expires_date_ms;

        // var isSubscriptionExist = null;
        // var subscriptionWithParentTransactionId = null;
        // get existing subscription with transaction id and parent transactionId

        // var existingSubscriptionForThisTransaction = await getParentSubscriptionIos(
        //   transactionId,
        //   parentTransactionId
        // );

        // for (var i in existingSubscriptionForThisTransaction) {
        //   var value = existingSubscriptionForThisTransaction[i];
        //   if (value.renewedSubscriptionTransactionId == transactionId) {
        //     isSubscriptionExist = value;
        //   }
        // }
       // subscriptionWithParentTransactionId = value;
      

    //   if (
    //     isSubscriptionExist == null &&
    //     subscriptionWithParentTransactionId == null
    //   ) {
        // There is no subscription in the database so we can add to database
        description = description + " -- Transaction Added First Time";

        // var parentSubscriptionId = await addParentSubscription(
        //   userId,
        //   SubscriptionPackageModel.IOS_DEVICE_TYPE,
        //   parentTransactionId,
        //   subscriptionToken,
        //   1
        // );

        let renewalData = {
          user_id: userId,
          package_id: packageId._id,
          description: description,
          transaction_id: transactionId,
        //  parent_subscription_id: parentSubscriptionId,
          purchase_date: purchaseDateMS,

          calling_from: callingFrom,

          //parent_transaction_id: parentTransactionId
        };

		let saveUserPackag =  await addSubscriptionRenewalTransactionNew(renewalData);
		if(saveUserPackag._id){
		
			const getCoin = await SubscriptionPackageModel.findOne(
				{ _id:  mongoose.Types.ObjectId(saveUserPackag.subscription_package_id)}
			  );
			  if(getCoin){
				let  getCoinof = getCoin.toObject()
				  
				let upcon = getCoinof.credits;
				
				let updateStr = { $inc: { coin: +upcon } };
				 await UserModel.findOneAndUpdate({_id: mongoose.Types.ObjectId(userId)}, updateStr, {returnOriginal: true})
				 updatecoin  = await UserModel.findOne({_id:  mongoose.Types.ObjectId(userId)},{coin:true});
			  }
			  
	   
		}
        paymentCount++;

        
     // } 
	}
}
  

    if (
      receiptResponse &&
      receiptResponse.status &&
      receiptResponse.status != 0
    ) {
		console.log("invalit"); 
		updatecoin = '';
      apiResponse.developer_messages = apiResponse.message =
        "Invalid purchase token.";
      responseFailed = response;
      addParentSubscriptionFailed(
        userId,
        SubscriptionTransactionDetailModel.IOS_DEVICE_TYPE,
        subscriptionToken,
        message
      );
    } else if (paymentCount == 0) {
      apiResponse.developer_messages = apiResponse.message =
        "No Subscription added or updated";
    } else {
      apiResponse.developer_messages = apiResponse.message =
        paymentCount + " Subscription Added or updated";
    }

    apiResponse.status = Utility.STATUS_SUCCESS;

    let deviceData = "";
    if (isUserIdExists) {
      deviceData = await UserDevicesModel.findById(userId);
    }

    apiResponse.data = {
      coins:updatecoin,
      product_id: productIdName,
      current_gmt_timestamp: Utility.getGmtTime()
    };

    if (sendResponse) {
      //apiResponse.receiptResponse = receiptResponse;
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      if (callingFrom === "cron_temp_for_old_server") {
        apiResponse.response = receiptResponse;
        apiResponse.processedTransactions = processedTransactions;
        apiResponse.transaction_added = transactionAdded;
        apiResponse.transaction_updated = transactionUpdated;
      }

      return apiResponse;
    }
  } catch (err) {
    err = err instanceof Object ? err : JSON.parse(err);

    addParentSubscriptionFailed(
      userId,
      SubscriptionTransactionDetailModel.IOS_DEVICE_TYPE,
      subscriptionToken,
      err.message
    );
    apiResponse.status = Utility.STATUS_ERROR;
    apiResponse.message = "Something went wrong. Please try after sometimes.";
    apiResponse.developer_messages = err.message;
    apiResponse.show_message_to_user = Utility.NOT_SHOW_MESSAGE;

    //update parent transaction pending renewal info and receipt
    //  updateSubscriptionTransactionsPendingRenewalInfoAndLatestReceipt(pendingRenewalInfo, latestReceipt);
    //end

    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      return apiResponse;
    }
  }
};

const validateIOSReceiptProcessNewHashtag = async (
  req,
  res,
  sendResponse = 1
) => {
  let headers = req.headers;
  var apiResponse = Utility.setResponse();

  let deviceID = headers.device_id ? headers.device_id : "";

  const isUserIdExists = deviceID ? 1 : 0;

  var subscriptionToken = req.body.app_store_receipt;

  const rules = {
    app_store_receipt: "required"
    //'is_debug': 'required',

    // 'price': 'required',
    //'currency': 'required',
  };

  const validator = await Utility.validateInputs(req.body, rules);

  const matched = await validator.check();

  if (!matched) {
    apiResponse.message = Utility.formatValidationMessages(validator.errors);
    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      return apiResponse;
    }
  }

  var receiptResponse = await validateIOSReceipt(req);

  const refresh = req.body.refresh ? req.body.refresh : 0;
  const isRestore = req.body.is_restore ? req.body.is_restore : 0;

  const callingFrom = req.body.calling_from ? req.body.calling_from : "";

  apiResponse.refresh = refresh;
  apiResponse.is_restore = isRestore;

  try {
    let objUserDevice = "";

    if (isUserIdExists) {
      objUserDevice = headers.device_id;
    }
    const expireCurrentDateMs = +new Date();
    // const expireCurrentDateMs = 1583020800000;
    var userDeviceUpdateData = {
      // is_premium: SubscriptionPackageModel.NOT_IS_PREMIUM,
      // expires_date_ms: expireCurrentDateMs
    };

    var payments = [];
    var paymentCount = 0;
    // if (receiptResponse && receiptResponse.latest_receipt_info && receiptResponse.latest_receipt_info instanceof Array) {
    //     payments = receiptResponse.latest_receipt_info;
    // } else if (receiptResponse && receiptResponse.receipt && receiptResponse.receipt.in_app instanceof Array) {
    //     payments = receiptResponse.receipt.in_app;
    // }

    if (
      receiptResponse &&
      receiptResponse.receipt &&
      receiptResponse.receipt.in_app instanceof Array
    ) {
      payments = receiptResponse.receipt.in_app;
    }

    if (payments.length) {
      payments = _.orderBy(payments, ["purchase_date_ms"], ["desc"]);
    }

    var productIdName = "";
    for (i in payments) {
      const payment = payments[i];

      let cancelDate = "";
      let purchaseDateMS = payment.purchase_date_ms;

      //get all active subscription from the receipt response
      //check expiry date grater than current date

      const transactionId = payment.transaction_id;
      const CheckSubscription = await HashtagUserSubscriptionPackageModel.findOne(
        {
          transaction_id: transactionId
        }
      );
      if (CheckSubscription) {
        apiResponse.status = Utility.STATUS_ERROR;
        apiResponse.message = "already purchase";

        apiResponse.show_message_to_user = Utility.NOT_SHOW_MESSAGE;
        return Utility.sendResponse(req, res, apiResponse);
      }
      //if (purchaseDateMS >= expireCurrentDateMs) {

      const parentTransactionId = payment.original_transaction_id;

      const productId = payment.product_id;
      productIdName = payment.product_id;
      var description = "Active Subscription";
      const packageId = await SubscriptionPackageModel.findOne(
        { product_id: productId },
        null
      );

      // var dbIsPremium = objUserDevice.is_premium;
      // var dbExpiryDatePremium = objUserDevice.expires_date_ms;

      var isSubscriptionExist = null;
      var subscriptionWithParentTransactionId = null;
      // get existing subscription with transaction id and parent transactionId

      // var existingSubscriptionForThisTransaction = await getParentSubscriptionIos(
      //   transactionId,
      //   parentTransactionId
      // );

      // for (var i in existingSubscriptionForThisTransaction) {
      //   var value = existingSubscriptionForThisTransaction[i];
      //   if (value.renewedSubscriptionTransactionId == transactionId) {
      //     isSubscriptionExist = value;
      //   }
      // }
      // subscriptionWithParentTransactionId = value;

      // if (
      //   isSubscriptionExist == null &&
      //   subscriptionWithParentTransactionId == null
      // ) {
      // There is no subscription in the database so we can add to database
      description = description + " -- Transaction Added First Time";

      // var parentSubscriptionId = await addParentSubscriptionHashtag(
      //   deviceID,
      //   SubscriptionPackageModel.IOS_DEVICE_TYPE,
      //  // parentTransactionId,
      //   subscriptionToken,
      //   1
      // );

      let renewalData = {
        device_id: deviceID,
        package_id: packageId._id,
        description: description,
        transaction_id: transactionId,
        // parent_subscription_id: parentSubscriptionId,
        purchase_date: purchaseDateMS,

        calling_from: callingFrom

        //parent_transaction_id: parentTransactionId
      };

	 
	let saveUserPackag = await addSubscriptionRenewalTransactionNewHashtag(renewalData);

	if(saveUserPackag._id){
		
		const getCoin = await SubscriptionPackageModel.findOne(
			{ _id:  mongoose.Types.ObjectId(saveUserPackag.subscription_package_id)}
		  );
		  if(getCoin){
			let  getCoinof = getCoin.toObject()
			  
			let upcon = getCoinof.credits;
			
			let updateStr = { $inc: { coin: +upcon } };
			 await hashtagdeviceModel.findOneAndUpdate({device_id: deviceID}, updateStr, {returnOriginal: true})
			 updatecoin  = await hashtagdeviceModel.findOne({device_id: deviceID},{coin:true});
		  }
		  

	
   
	}
      paymentCount++;

      if (callingFrom === "cron_temp_for_old_server") {
        transactionAdded++;
      }
      // }
    }

    if (objUserDevice) {
      if (objUserDevice.device_id && parseInt(objUserDevice.device_id) !== 0) {
        await updateUserSubscription(
          objUserDevice.device_id,
          userDeviceUpdateData
        );
      } else {
        await updateUserDeviceSubscription(objUserDevice, userDeviceUpdateData);
      }
    }

    if (
      receiptResponse &&
      receiptResponse.status &&
      receiptResponse.status != 0
    ) {
      apiResponse.developer_messages = apiResponse.message =
        "Invalid purchase token.";
      responseFailed = response;
      addParentSubscriptionFailedHashtag(
        deviceID,
        SubscriptionTransactionDetailModel.IOS_DEVICE_TYPE,
        subscriptionToken,
        message
      );
    } else if (paymentCount == 0) {
      apiResponse.developer_messages = apiResponse.message =
        "No Subscription added or updated";
    } else {
      apiResponse.developer_messages = apiResponse.message =
        paymentCount + " Subscription Added or updated";
    }

    apiResponse.status = Utility.STATUS_SUCCESS;

    let deviceData = "";
    if (isUserIdExists) {
      // deviceData = await UserDevicesModel.findById(userId);
    }

    apiResponse.data = {
    
      product_id: productIdName,
	  current_gmt_timestamp: Utility.getGmtTime(),
	  coins:updatecoin
    };

    if (sendResponse) {
     // apiResponse.receiptResponse = receiptResponse;
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      if (callingFrom === "cron_temp_for_old_server") {
        apiResponse.response = receiptResponse;
        apiResponse.processedTransactions = processedTransactions;
        apiResponse.transaction_added = transactionAdded;
        apiResponse.transaction_updated = transactionUpdated;
      }

      return apiResponse;
    }
  } catch (err) {
    console.log("has", err);
    err = err instanceof Object ? err : JSON.parse(err);

    addParentSubscriptionFailedHashtag(
      deviceID,
      SubscriptionTransactionDetailModel.IOS_DEVICE_TYPE,
      subscriptionToken,
      err.message
    );
    apiResponse.status = Utility.STATUS_ERROR;
    apiResponse.message = "Something went wrong. Please try after sometimes.";
    apiResponse.developer_messages = err.message;
    apiResponse.show_message_to_user = Utility.NOT_SHOW_MESSAGE;

    //update parent transaction pending renewal info and receipt
    //  updateSubscriptionTransactionsPendingRenewalInfoAndLatestReceipt(pendingRenewalInfo, latestReceipt);
    //end

    if (sendResponse) {
      return Utility.sendResponse(req, res, apiResponse);
    } else {
      return apiResponse;
    }
  }
};

module.exports.validateIOSReceipt = validateIOSReceipt;

//module.exports.getAndroidPackageId = getAndroidPackageId;

module.exports.addParentSubscription = addParentSubscription;

module.exports.purchasedSubscriptions = purchasedSubscriptions;

module.exports.updateUserSubscription = updateUserSubscription;

//module.exports.getPackageIdForAndroid = getPackageIdForAndroid;

module.exports.userExpiredSubscription = userExpiredSubscription;

//module.exports.validatePlayStoreReceipt = validatePlayStoreReceipt;

module.exports.validatePlayStoreReceiptProcessNew = validatePlayStoreReceiptProcessNew;

module.exports.getParentSubscriptionIos = getParentSubscriptionIos;

module.exports.userExpiredSubscriptionNew = userExpiredSubscriptionNew;

module.exports.addParentSubscriptionFailed = addParentSubscriptionFailed;

module.exports.validateIOSReceiptProcessNew = validateIOSReceiptProcessNew;
module.exports.validateIOSReceiptProcessNewHashtag = validateIOSReceiptProcessNewHashtag;

module.exports.getParentSubscriptionAndroid = getParentSubscriptionAndroid;

module.exports.getParentSubscriptionAndroidNew = getParentSubscriptionAndroidNew;

module.exports.updateUserDeviceSubscription = updateUserDeviceSubscription;

module.exports.updateSubscriptionTransaction = updateSubscriptionTransaction;

module.exports.updateSubscriptionTransactionNew = updateSubscriptionTransactionNew;

module.exports.updateParentSubscriptionData = updateParentSubscriptionData;

module.exports.validatePlayStoreReceiptProcess = validatePlayStoreReceiptProcess;

module.exports.addSubscriptionRenewalTransaction = addSubscriptionRenewalTransaction;

module.exports.addSubscriptionRenewalTransactionNew = addSubscriptionRenewalTransactionNew;

module.exports.userExpiredSubscriptionTemporaryToGetOldTransactions = userExpiredSubscriptionTemporaryToGetOldTransactions;

module.exports.updateSubscriptionTransactionsPendingRenewalInfoAndLatestReceipt = updateSubscriptionTransactionsPendingRenewalInfoAndLatestReceipt;
